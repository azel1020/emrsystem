//
//  AssistantInfo.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 5/20/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class AssistantInfo: NSObject {
    //MARK: AssistantInfo Container Value
    var userId     :String!
    var username   :String!
    var position   :String!
    var location   :String!
    var status     :String!
    var department :String!
    var doctorId   :String!
    
    //MARK: AssistantInfo Custom Class Properties
    let a_userId     = "userId"
    let a_doctorId   = "doctorId"
    let a_username   = "username"
    let a_position   = "position"
    let a_location   = "location"
    let a_status     = "status"  //nakalimutan ko n kung para san //Great!
    let a_department = "department"
    
    let ASSISTANT_INFO  = "AssistantInfo"
    typealias SaveBlock = (Bool?, AnyObject?) -> Void
    
    func saveCurrentUserToAssistantInfo(withBlock: SaveBlock) {
        var doctorID = CatalyzeUser.currentUser().extraForKey(a_doctorId) as String
        var assistantEntry : CatalyzeEntry = CatalyzeEntry(className: ASSISTANT_INFO)
            assistantEntry.content.setObject(CatalyzeUser.currentUser().usersId, forKey: a_userId)
            assistantEntry.content.setObject(doctorID, forKey: a_doctorId)
            assistantEntry.content.setObject(CatalyzeUser.currentUser().username, forKey: a_username)
            assistantEntry.content.setObject(true, forKey: a_status)
        
// add when user can already fetch all assistant
        
//        assistantEntry.createInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
//            println("RESULT SUCCESS: \(result)")
//            withBlock(true,nil)
//            }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
//                println("RESULT: \(result)")
//                withBlock(false,result)
//        })
    }

    func fetchAllAssistantInfo() {
        var query : CatalyzeQuery = CatalyzeQuery(className: ASSISTANT_INFO)
        query.pageNumber = 1
        query.pageSize = 100 //may vary
        
        query.retrieveAllEntriesInBackgroundWithSuccess({ (result: [AnyObject]!) -> Void in
            println("Retrieve: \(result)")
            }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
            println("FAILED Retrieving")
        })
        
    }
    
    
    //TODO: Check all assistants of this doctor , test saving of user if assistant, to assistantInfo, check if it already exists
    
    
//    func fetch
}

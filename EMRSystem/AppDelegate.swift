//
//  AppDelegate.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/23/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let ApiKey = "ios DynamicObjx.EMRSystem 6da64e8d-3797-4782-88ea-a2eaa76e0a73"
    let AppID = "40e4411b-20ab-4993-975c-666fcd1b8c2e"

    let list: List = List()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        Catalyze.setApiKey(ApiKey, applicationId: AppID)
        self.setLocalNotification()
        list.awakeTypeAhead()
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        return true
    }
    
    func setLocalNotification() {
        if(UIApplication.instancesRespondToSelector(Selector("registerUserNotificationSettings:"))) {
            UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: .Alert | .Badge, categories: nil))
        }
        self.clearBadge()
    }
    
    func clearBadge() {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    }
    
    func setTableViewCellSelection() {
        let colorView = UIView()
        colorView.backgroundColor = UIColor(red:88/255.0, green: 29/255.0, blue: 26/255.0, alpha: 1.0)
        
        // use UITableViewCell.appearance() to configure
        // the default appearance of all UITableViewCells in your app
        UITableViewCell.appearance().selectedBackgroundView = colorView
    }

    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        self.clearBadge()
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


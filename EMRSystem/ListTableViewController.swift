//
//  ListTableViewController.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 5/21/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class ListTableViewController: UITableViewController, UIAlertViewDelegate {

    let base: BaseViewController = BaseViewController()
    let list: List = List()
    
    @IBOutlet weak var emptyStateView: UIView!
    var selectedIndex : NSIndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = List.sharedInstance.medicalList.count
        
        if count == 0 {
            return 1
        } else {
            return count
        }
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        var cell: MGSwipeTableCell!
        
        let count = List.sharedInstance.medicalList.count
        
        if count == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("EmptyStateCell") as MGSwipeTableCell
            
        } else {
            let title = List.sharedInstance.medicalList[indexPath.row] as String
            cell = tableView.dequeueReusableCellWithIdentifier("ListCell") as MGSwipeTableCell
            cell.textLabel!.text = title
            var deleteButton: MGSwipeButton = MGSwipeButton(title: "     ", backgroundColor: self.base.eRedColor()) { (sender:MGSwipeTableCell!) -> Bool in
                let alert = UIAlertView()
                alert.title = "EMR"
                alert.message = "Are you sure you want to delete \"\(title)\"?"
                alert.addButtonWithTitle("No")
                alert.addButtonWithTitle("Yes")
                alert.delegate = self
                alert.show()
                self.selectedIndex = indexPath
                return false
            }
            deleteButton.titleLabel?.font = UIFont(name: "fontawesome", size: 30)
            cell.rightButtons = [deleteButton]
            cell.rightSwipeSettings.transition = MGSwipeTransition.Border
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let count = List.sharedInstance.medicalList.count
        if count == 0 {
            return 115
        } else {
            return 50
        }
    }
    
    //MARK: AlertView
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            let title = List.sharedInstance.medicalList[self.selectedIndex.row] as String
            self.showHUD(true)
            self.base.HUD.labelText = "Cancelling Appointment..."
            self.list.deleteToObjectList(title, type: ListType.Medical)
            self.showHUD(false)
            self.tableView.reloadData()
        }
    }
    
    func showHUD(show: Bool) {
        if show == true {
            self.base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        } else {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
        }
    }

    
}

//
//  MainNavigationViewController.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 4/17/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class MainNavigationViewController: UINavigationController {

    var base: BaseViewController = BaseViewController()
    
    var viewcontrollerIdentifier = "" as NSString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setListener()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Notification
    func setListener() {
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "setViewController:", name: self.base.navigationListener, object: nil)
    }
    
    func setViewController(noti:NSNotification) {
        println("Listening...")
        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
            
            var identifier = noti.object as NSString
            if identifier.isEqualToString(self.viewcontrollerIdentifier) {
                self.popToRootViewControllerAnimated(true)
                return
            } else {
                self.popToRootViewControllerAnimated(false)
                self.viewcontrollerIdentifier = identifier
                var vc = self.storyboard?.instantiateViewControllerWithIdentifier(identifier) as UIViewController
                self.setViewControllers([vc], animated: false)
            }
            
        })
    }

}

//
//  UserProfileViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 4/17/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class UserProfileViewController: UITableViewController, UIPopoverControllerDelegate , UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var dobButton: UIButton!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var provinceTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var contactNoTextField: UITextField!
    @IBOutlet weak var dobCell: UITableViewCell!
    @IBOutlet weak var departmentLabel: UITextField!
    @IBOutlet weak var positionLabel: UITextField!
    
    var photoImage: NSMutableDictionary = NSMutableDictionary()
    var photoToBeDeleted: NSMutableDictionary = NSMutableDictionary()
    
    var base : BaseViewController  = BaseViewController()
    var login: LoginViewController = LoginViewController()
    var birthdateVC : BirthdateViewController = BirthdateViewController()
    var userAccnt   : UserAccount  = UserAccount()
    var userAccount : CatalyzeUser!
    
    var popoverController: UIPopoverController!
    var popOverView : UIPopoverController!
    var datePicker  : UIDatePicker!
    
    var selectedDate: NSDate!
    
    //Profile Photo
    var photoPicker : UIImagePickerController? = UIImagePickerController()
    
    //MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setListener()
        
        self.userAccount = CatalyzeUser.currentUser()
        self.birthdateVC = BirthdateViewController(nibName: "BirthdateViewController", bundle: nil)
        self.setUpAccountDetails()
        
        self.dobButton.layer.borderColor      = UIColor.lightGrayColor().CGColor
        self.dobButton.layer.borderWidth      = 1.0
        self.dobButton.layer.cornerRadius     = 5.0
        
        self.genderButton.layer.borderColor   = UIColor.lightGrayColor().CGColor
        self.genderButton.layer.borderWidth   = 1.0
        self.genderButton.layer.cornerRadius  = 5.0
        
        self.profilePhoto.layer.borderWidth   = 3.0
        self.profilePhoto.layer.borderColor   = base.eRedColor().CGColor
        self.profilePhoto.layer.cornerRadius  = self.profilePhoto.frame.height / 2
        self.profilePhoto.layer.masksToBounds = true
        
        var tap : UITapGestureRecognizer      = UITapGestureRecognizer(target: self, action: "showTakePhotoOptions")
        self.profilePhoto.addGestureRecognizer(tap)
    }
    
    func setListener() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setUserProfile", name: self.base.userProfileListener, object: nil)
    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "Edit Profile"
        var saveButton : UIBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Plain, target: self, action: "validateUserAccountForm")
        self.navigationItem.rightBarButtonItem = saveButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.whiteColor()
    }
    
    func setUpAccountDetails() {
        self.base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.base.HUD.labelText = "Retrieving User Photo..."
        
        self.usernameTextField.text = userAccount.username
        self.firstNameTextField.text = userAccount.name.firstName
        self.lastNameTextField.text = userAccount.name.lastName
        self.emailTextField.text = userAccount.email.primary
        
        if let gender = userAccount.gender {
            self.genderButton.setTitle(gender, forState: UIControlState.Normal)
        }
        if let age = userAccount.age {
            if age != 0 {
                self.ageTextField.text = "\(age)"            
            }
        }
        if let dob = userAccount.dob {
            self.selectedDate
                = dob
            self.dobButton.setTitle(self.base.formatDate(dob, withTime: false), forState: UIControlState.Normal)
        }
        if let status = userAccount.maritalStatus {
            self.statusTextField.text = status
        }
        
        if userAccount.addresses.count != 0{
            if let addresses = userAccount.addresses {
                var address : Address = addresses.firstObject as Address
                if let addressPrimary = address.addressLine1 {
                    self.addressTextField.text = addressPrimary
                }
                if let city = address.city {
                    self.cityTextField.text = city
                }
                if let state = address.state {
                    self.provinceTextField.text = state
                }
            }
        }
        
        if let contactNo = userAccount.phoneNumber.work {
            self.contactNoTextField.text = "\(contactNo)"
        }
        
        //extra fields
        if let department: NSString = userAccount.extraForKey(userAccnt.department) as? NSString {
            self.departmentLabel.text = department as String
        }
        
        if let position: NSString = userAccount.extraForKey(userAccnt.position) as? NSString {
            self.positionLabel.text = position as String
        }
        self.setUserProfile()

    }
    
    //MARK: Retrieve Profile Photo

    func setUserProfile() {
        if let photoFileId = userAccount.profilePhoto {
            if photoFileId != "" {
                if self.base.userAccount.hasCacheImageFor(photoFileId) {
                    println("has image cache")
                    var image = self.base.userAccount.getCacheImageFor(photoFileId)
                    self.setImageDictionary(image, hasImage: true, fileId: photoFileId, isNew: false)
                    
                    self.photoToBeDeleted.setObject(NSNumber(bool: false), forKey: self.base.keyToBeDeleted)
                    self.profilePhoto.image = image
                    self.base.HUD.hide(true)
                    self.setUpNavigationBar()
                }
            } else {
                self.setUpNavigationBar()
                self.setImageDictionary(UIImage(), hasImage: false, fileId: "", isNew: false)
                self.base.HUD.hide(true)
            }
        } else {
            self.setUpNavigationBar()
            self.setImageDictionary(UIImage(), hasImage: false, fileId: "", isNew: false)
            self.base.HUD.hide(true)
        }
    }
    
    func setImageDictionary(image:UIImage, hasImage: Bool, fileId: NSString, isNew: Bool) {
        if hasImage {
            self.photoImage.setObject(image, forKey: self.base.keyImage)
            self.photoImage.setObject(NSNumber(bool: true), forKey: self.base.keyHasImage)
            self.photoImage.setObject(fileId, forKey: self.base.keyFileId)
        } else {
            self.photoImage.setObject("", forKey: self.base.keyFileId)
            self.photoImage.setObject(NSNumber(bool: false), forKey: self.base.keyHasImage)
        }
        
        self.photoImage.setObject(NSNumber(bool: isNew), forKey: self.base.keyIsNew)
    }
    
    //MARK: Gender, Date and Age  Selector
    @IBAction func setGender(sender: AnyObject) {
        var actionSheet : UIActionSheet = UIActionSheet(title: "Select Gender", delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil)
            actionSheet.addButtonWithTitle("Female")
            actionSheet.addButtonWithTitle("Male")
            actionSheet.showInView(self.tableView)
    }
    
    @IBAction func selectDateOfBirth(sender: AnyObject) {
        var dobRect = tableView.rectForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 2))

        self.popOverView  = UIPopoverController(contentViewController: birthdateVC)
        self.popOverView.presentPopoverFromRect(CGRectMake(-200, dobRect.origin.y, dobRect.width, dobRect.height), inView: self.tableView, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        self.popOverView.passthroughViews = nil
        self.popOverView.delegate = self
        self.popOverView.setPopoverContentSize(CGSizeMake(400, 260), animated: true)
    
        self.datePicker = birthdateVC.datePicker
        self.datePicker.setDate(selectedDate, animated: true)
        self.birthdateVC.cancelButton.addTarget(self, action: "dismissPopOver", forControlEvents: UIControlEvents.TouchUpInside)
        self.birthdateVC.doneButton.addTarget(self, action: "setDateAndAge", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    func setDateAndAge() {
        if self.base.getYear(self.datePicker.date) <=  self.base.getYear(NSDate()) {
            self.selectedDate = self.datePicker.date
            self.dobButton.setTitle(self.base.formatDate(self.datePicker.date, withTime: false), forState: UIControlState.Normal)
            self.dismissPopOver()
            self.ageTextField.text = self.base.getAge(self.datePicker.date)
        } else {
            self.base.showAlert("Invalid Date of Birth")
        }
    }
    
    func dismissPopOver() {
        self.popOverView.dismissPopoverAnimated(true)
    }

    //MARK: ActionSheetDelegate
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if actionSheet.tag == 1{
            self.showCamera(buttonIndex)
        } else {
            if buttonIndex == 0 {
                self.genderButton.setTitle("F", forState: UIControlState.Normal)
            } else if buttonIndex == 1 {
                self.genderButton.setTitle("M", forState: UIControlState.Normal)
            }
        }
    }
    
    //MARK: Edit Account Details
    func validateUserAccountForm() {
        self.view.endEditing(true)
        self.saveButton(isEnable: false)
        if base.isConnectedToNetwork() {
            if isFormValid() {
                var hasImage = self.photoImage.objectForKey(self.base.keyHasImage)?.boolValue
                var hasDeletePhoto = self.photoToBeDeleted.objectForKey(self.base.keyToBeDeleted)?.boolValue
                var isNewImage  = self.photoImage.objectForKey(self.base.keyIsNew)?.boolValue
                var fileId = self.photoImage.objectForKey(self.base.keyFileId) as NSString
                
                self.base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                base.HUD.labelText = "Saving Photo. Please Wait..."
                
                if self.isFormHasChanges() && isNewImage == false {// Changes in UserDetails
                    println("------ Changes in userdetails only ------")
                    self.saveUserDetails(fileId)
                } else if (isNewImage == true && hasDeletePhoto == true) || (hasDeletePhoto == true) {// Replace Image
                    println("------ Replaces Existing Image ------")
                    self.deletePhoto(hasNewImage:true,fileId: fileId)
                } else if (isNewImage == true && hasDeletePhoto == false) || (isNewImage == true) {// New Image
                    println("------ Changes in default image ------")
                    self.savePhoto()
                } else {
                    self.saveButton(isEnable: true)
                    self.base.HUD.hide(true)
                    self.base.showAlert("Changes successfully saved.")
                }
                
            } else {
            self.saveButton(isEnable: true)

                self.base.showAlert("Please fill all fields")
            }
        } else {
            self.saveButton(isEnable: true)
            self.base.showAlert(base.noNetworkConnectionMessage)
        }
    }
    
    func saveButton(#isEnable: Bool) {
        self.navigationItem.rightBarButtonItem?.enabled = isEnable
    }
    
    func deletePhoto(#hasNewImage: Bool, fileId: NSString) {
        self.base.appointments.deletePhoto(self.photoToBeDeleted.objectForKey(self.base.keyFileId) as NSString, inBlock: { (succeeded:Bool?, object: AnyObject?) -> Void in
            if succeeded == true {
                if hasNewImage == true { //If User Changed Existing image
                    println("with saving another image")
                    self.savePhoto()
                } else {
                    if self.isFormHasChanges() { //If user change details and removed photo
                        println("with changes in user detail")
                        self.saveUserDetails(fileId)
                    } else { //If user clear only its image
                        println("without changes in user detail")
                        self.updateProfilePhotoOnly(fileId)
                    }
                }
            } else {
                self.saveButton(isEnable: true)
                self.base.showAlert("Oops. Something wrong. Please try again.")
                self.base.HUD.hide(true)
            }
        })
    }
    
    func savePhoto() {
        
        let data = UIImageJPEGRepresentation(profilePhoto.image, 0.5) as NSData
        
        self.base.userAccount.saveProfilePhoto(data, withBlock: { (succeeded:Bool?, result:AnyObject?) -> Void in
            if succeeded == true {
                let fileDictionary = result as NSDictionary
                let fileId = fileDictionary.objectForKey(self.base.appointments.filesId)! as NSString
                self.setImageDictionary(self.photoImage.objectForKey(self.base.keyImage)! as UIImage, hasImage: true, fileId: fileId, isNew: false)
                self.photoToBeDeleted.removeAllObjects()
            
                if self.isFormHasChanges() { //If user save new image and have changes
                    println("with changes in user detail")
                    self.saveUserDetails(fileId)
                } else {
                    println("without changes in user detail")
                    self.updateProfilePhotoOnly(fileId) //If user update only its image
                }
            } else {
                self.saveButton(isEnable: true)

                self.base.showAlert("Oops. Something wrong. Please try again.")
                self.base.HUD.hide(true)
            }
        })
    }
    
    func updateProfilePhotoOnly(photoFileId: String) {
        self.photoImage.setObject(photoFileId, forKey: self.base.keyFileId)
        self.base.userAccount.editUserAccountPhotoOnly(photoFileId, withBlock: { (isSuccess: Bool?, error: AnyObject?) -> Void in
            self.base.HUD.hide(true)
            if isSuccess != false {
                NSNotificationCenter.defaultCenter().postNotificationName(self.base.menuUserListener, object: nil)
                self.saveButton(isEnable: true)
                self.base.HUD.hide(true)
               self.base.showAlert("Successfully updated your photo")
            } else {
                self.saveButton(isEnable: true)
                self.base.HUD.hide(true)
               self.base.showAlertError(error!)
            }
        })
    }
    
    func saveUserDetails(photoFileId: String) {
        
        var emailAdd : Email = Email()
        emailAdd.primary = emailTextField.text
        
        var fullName : Name = Name()
        fullName.firstName = firstNameTextField.text
        fullName.lastName  = lastNameTextField.text
        
        var phoneNo: PhoneNumber = PhoneNumber()
        phoneNo.work = contactNoTextField.text
        
        var address: Address = Address()
        address.addressLine1 = addressTextField.text
        address.city         = cityTextField.text
        address.state        = provinceTextField.text
        address.country      = "Philippines" //required fields from address and
        address.type         = "Home"        // maybe added on the future
        var addressArray : NSMutableArray = [address]
        
        var genderMF  = genderButton.titleLabel?.text
        
        userAccnt.editUserAccount(usernameTextField.text, email: emailAdd, name: fullName, dob: self.selectedDate, age: ageTextField.text.toInt()!, gender: genderMF!, status: statusTextField.text, address: addressArray, contactNo: phoneNo, dept: departmentLabel.text , pos: positionLabel.text, profilePhoto: photoFileId) { (result: Bool?, error: AnyObject?) -> Void in
            self.saveButton(isEnable: true)
            self.base.HUD.hide(true)
            if result == true {
                NSNotificationCenter.defaultCenter().postNotificationName(self.base.menuUserListener, object: nil)
                self.base.HUD.hide(true)
                self.base.showAlert("Successfully updated your account")
            } else {
                self.base.HUD.hide(true)
                self.base.showAlertError(error)
            }
        }
    }
    
    //MARK: Form Validation
    func isFormValid() -> Bool {
        var dob    = dobButton.titleLabel?.text
        var gender = genderButton.titleLabel?.text
        
        if departmentLabel.text != "" && positionLabel.text != "" && usernameTextField.text != "" &&
            emailTextField.text != "" && firstNameTextField.text != "" && lastNameTextField.text != "" {
                if dob! != "" && gender! != "" && statusTextField.text != "" && addressTextField.text != "" &&
                    cityTextField.text != "" && provinceTextField.text != "" && contactNoTextField.text != "" {
                        return true
                }
        }
        
        return false
    }
    
    func isFormHasChanges() -> Bool {
        var dob    = dobButton.titleLabel?.text
        var gender = genderButton.titleLabel?.text
        var address : Address = userAccount.addresses.firstObject as Address
        
        if departmentLabel.text == userAccount.extraForKey(userAccnt.department) as NSString! &&
            positionLabel.text == userAccount.extraForKey(userAccnt.position) as NSString! &&
            usernameTextField.text == userAccount.username && emailTextField.text == userAccount.email.primary &&
            firstNameTextField.text == userAccount.name.firstName && lastNameTextField.text == userAccount.name.lastName &&
            selectedDate == userAccount.dob && gender == userAccount.gender && statusTextField.text == userAccount.maritalStatus &&
            contactNoTextField.text == userAccount.phoneNumber.work {
            if addressTextField.text == address.addressLine1 && cityTextField.text == address.city
                && provinceTextField.text == address.state {
                return false
            }
        }
        
        return true
    }
    
    func isProfilePhotoHasChanged() -> Bool {
        return profilePhoto.image != UIImage(named: "profile") ? true : false
    }
    
    //MARK: TableView
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1 : return "Account Settings"
        case 2 : return "Profile"
        default: return nil
        }
    }
 
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 30
        }
    }
    
    //MARK: Camera
    func showTakePhotoOptions() {
        var actionSheet = UIActionSheet()
        if profilePhoto.image != UIImage(named: "profile") {
            actionSheet = UIActionSheet(title: "Choose", delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery", "Remove Photo")
        } else {
            actionSheet = UIActionSheet(title: "Choose", delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        }
            actionSheet.tag = 1
            actionSheet.showInView(self.view)
    }
    
    func showCamera(camera : Int) {
        self.photoPicker = UIImagePickerController()
        self.photoPicker?.delegate      = self
        self.photoPicker?.allowsEditing = true

        switch camera {
        case 0:
            if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                self.photoPicker?.sourceType = .Camera
            } else {
                self.photoPicker?.sourceType = .SavedPhotosAlbum
            }
            self.showOperatedImagePicker()
        case 1:
            self.photoPicker?.sourceType = .SavedPhotosAlbum
            self.showOperatedImagePicker()
        case 2:
            var hasImage = self.photoImage.objectForKey(self.base.keyHasImage)?.boolValue
            var isNew = self.photoImage.objectForKey(self.base.keyIsNew)?.boolValue
            if hasImage == true && isNew == false {
                self.photoToBeDeleted.setObject(self.photoImage.objectForKey(self.base.keyImage)!, forKey: self.base.keyImage)
                self.photoToBeDeleted.setObject(self.photoImage.objectForKey(self.base.keyFileId)!, forKey: self.base.keyFileId)
                self.photoToBeDeleted.setObject(NSNumber(bool: true), forKey: self.base.keyToBeDeleted)
            }
            self.setImageDictionary(UIImage(), hasImage: false, fileId: "", isNew: true)
            self.profilePhoto.image = UIImage(named: "profile") //Remove profile photo
        default:
            break
        }
    }
    
    func showOperatedImagePicker() {
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            self.showPicker()
        }
    }

    func showPicker() {
        if photoPicker?.sourceType == UIImagePickerControllerSourceType.SavedPhotosAlbum {
            photoPicker?.modalPresentationStyle = .FormSheet
            self.base.setLandscapeLeftOrientation()
        } else {
            self.base.setCameraOrientation()
        }
        
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            self.photoPicker?.allowsEditing = true
            self.presentViewController(self.photoPicker!, animated: true, completion: { () -> Void in })
        }
    }
    
    //MARK: ImagePicker 
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        var chosenImage = info[UIImagePickerControllerEditedImage] as UIImage
        var hasImage = self.photoImage.objectForKey(self.base.keyHasImage)?.boolValue
        var isNew = self.photoImage.objectForKey(self.base.keyIsNew)?.boolValue
        if hasImage == true && isNew == false {
            self.photoToBeDeleted.setObject(self.photoImage.objectForKey(self.base.keyImage)!, forKey: self.base.keyImage)
            self.photoToBeDeleted.setObject(self.photoImage.objectForKey(self.base.keyFileId)!, forKey: self.base.keyFileId)
            self.photoToBeDeleted.setObject(NSNumber(bool: true), forKey: self.base.keyToBeDeleted)
        }
        
        self.profilePhoto.image = chosenImage
        self.setImageDictionary(chosenImage, hasImage: true, fileId: "", isNew: true)
        
        self.dismissPicker()
    }
    
    func changeImage() {
        
    }
    
    func dismissPicker() {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.base.setLandscapeLeftOrientation()
        })
    }
    
    //MARK: Others
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

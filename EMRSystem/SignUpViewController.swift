//
//  SignUpViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/25/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {
    @IBOutlet weak var fnameTextField: UITextField!
    @IBOutlet weak var lnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet var textFields: [UITextField]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = false
        
        for textfield in textFields {
            textfield.layer.sublayerTransform = CATransform3DMakeTranslation(8, 0, 0)
        }
    }
    
    @IBAction func signUpUser(sender: AnyObject) {
        if isConnectedToNetwork() {
            var email : Email = Email()
                email.primary = emailTextField.text
            
            var name : Name = Name()
                name.firstName = fnameTextField.text
                name.lastName  = lnameTextField.text
            
            self.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            
            authService.signUpUser(usernameTextField.text, email: email, name: name, password: passwordTextField.text, doctorID: nil, isAdminStatus: true, withBlock: { (isSuccess: Bool?, error: AnyObject?) -> Void in
                self.HUD.hide(true)
                if isSuccess != false {
                    self.showAlert("Request Sent! Please activate your account on your email")
                    self.navigationController?.popViewControllerAnimated(true)
                } else {
                    self.showAlertError(error!)
                }
            })
        } else {
            self.showAlert(noNetworkConnectionMessage)
        }
    }
}

//
//  DashboardViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 4/29/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class DashboardViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIPopoverControllerDelegate , UIPickerViewDataSource , UIPickerViewDelegate, UIAlertViewDelegate {

    @IBOutlet weak var todaysTableView: UITableView!
    @IBOutlet weak var patientsCollectionView: UICollectionView!
    @IBOutlet weak var greetingsLabel: UILabel!
    @IBOutlet weak var dateNowLabel: UILabel!
    @IBOutlet weak var timeNowLabel: UILabel!
    @IBOutlet weak var todayPatientLabel: UILabel!
    @IBOutlet weak var yesterdayPatientLabel: UILabel!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var bannerView: UIView!
    
    var selectedIndex: NSIndexPath!
    
    var appointmentYesterday : [Appointment] = []
    var appointmentTodayArray: [Appointment] = []
    var missedArray          : [Appointment] = []
    var finishedArray        : [Appointment] = []
    var pendingArray         : [Appointment] = []
    var recentPatient        : [PatientInfo] = []
    var timer: dispatch_source_t!
    
    var popoverController: UIPopoverController!
    var popOverView  : UIPopoverController!
    var filterPicker : UIPickerView!
    var filterOptions = []
    
    var ALLTODAY = "All"
    var PENDING  = "Pending"
    var MISSED   = "Missed"
    var FINISHED = "Finished"
    var SELECTED = "All"
    var lastSelected = 1
    
    //Icons
    var allTodayIcon = "appointment"
    var missedIcon   = "missedAppointment"
    var finishedIcon = "finishedAppointments"
    var pendingIcon  = "pendingAppointment"
    
    //MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showHUD(true)
        self.HUD.labelText = "Retrieving Appointment..."
        self.setUpNavigationBar()
        self.setUpBanner()
        self.getAppointmentToday()
        self.todaysTableView.tableFooterView = UIView(frame: CGRectZero)
        self.filterOptions = ["All","Pending","Missed","Finished"]
        self.SELECTED      = PENDING
        self.setAppointmentIcon(from: SELECTED)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getAppointmentToday", name: updateAppointmentList, object: nil)
    }
    
    func setUpBanner() {
        self.greetingsLabel.text = "\(self.setGreetings()) \(CatalyzeUser.currentUser().name.firstName) !"
        self.dateNowLabel.text = "\(self.getWeekDay(NSDate(), isComplete: true))  \(self.getMonth(NSDate())) \(self.getDayDatabaseFormat(NSDate()))"
    }
    
    func setUpNavigationBar() {
        self.navigationController?.navigationBarHidden = false
        self.navigationItem.title = "Dashboard"
    }
    
    @IBAction func showFilterOptions(sender: AnyObject) {
        var optionVC = OptionsViewController(nibName: "OptionsViewController", bundle: nil)
        self.popOverView = UIPopoverController(contentViewController: optionVC)
        var rect = CGRectMake(filterButton.frame.origin.x + 10, filterButton.frame.origin.y , filterButton.frame.width, filterButton.frame.height)
        self.popOverView.presentPopoverFromRect(rect, inView: self.bannerView, permittedArrowDirections: UIPopoverArrowDirection.Up, animated: true)
        self.popOverView.delegate  = self
        self.popOverView.setPopoverContentSize(CGSizeMake(320, 200), animated: true)
        
        self.filterPicker = optionVC.filterPicker as UIPickerView
        self.filterPicker.delegate   = self
        self.filterPicker.dataSource = self
        self.setAppointmentIcon(from: SELECTED)        
        self.filterPicker.selectRow(lastSelected, inComponent: 0, animated: true)
    }
    
    //MARK: UIPickerView Delegate and DataSource
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.filterOptions.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return self.filterOptions[row] as String
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
            case 1  : SELECTED = PENDING
            case 2  : SELECTED = MISSED
            case 3  : SELECTED = FINISHED
            default : SELECTED = ALLTODAY
        }
        
        self.lastSelected = row
        self.todaysTableView.reloadData()
        self.setAppointmentIcon(from: SELECTED)
        self.popOverView.dismissPopoverAnimated(true)
    }
    
    //MARK: Appointments
    func checkAppointments() { //getting the next appointment
        if self.appointmentTodayArray.count != 0 {
            for appointment in self.appointmentTodayArray {
                if appointment.status != true && isDate(NSDate(), isGreaterThan: self.convertDateFromString(appointment.date, withTime: true)!) {
                    self.timeNowLabel.text = self.getTime(appointment.date)
                    return
                } else {
                    self.timeNowLabel.text = "--:-- --"
                }
            }
        }
    }
    
    func getAppointmentToday() {
        self.appointmentTodayArray = []
        self.appointmentYesterday  = []
        self.recentPatient = []
        self.finishedArray = []
        
        var idArray: NSMutableArray = []
         
        if let appointments = AuthService.sharedInstance.allAppointmentArray {
            for appointment in appointments {
                var convertedDate = self.convertDateFromString(appointment.date, withTime: true) as NSDate!
                if self.isDateToday(convertedDate) {
                    self.appointmentTodayArray.append(appointment)
                } else if self.isDateYesterday(convertedDate) {
                    self.appointmentYesterday.append(appointment)
                }
                
                if !idArray.containsObject(appointment.patientId) {
                    idArray.addObject(appointment.patientId)
                    if let patients = AuthService.sharedInstance.allPatientsArray {
                        var encodedObject = self.getCacheFor(appointment.patientId) as NSDictionary
                        recentPatient.append(self.patientInfo.getDictionary(encodedObject))
                    }
                }
            }
            
            self.setPatientCount(yesterdayArray: removeDuplicatePatient(appointmentYesterday), todayArray: removeDuplicatePatient(appointmentTodayArray))
            self.appointmentTodayArray = self.sortAppointmenByDate(self.appointmentTodayArray, isAscending: false)
            self.separateAppointments()
            self.patientsCollectionView.reloadData()
            self.todaysTableView.reloadData()
            self.checkAppointments()
            self.startTimer()
            self.todaysTableView.hidden = false
            self.showHUD(false)
        } else {
            self.todaysTableView.hidden = true
        }
    }
    
    func separateAppointments() { //separate appointments from missed and finished
        self.missedArray   = []
        self.finishedArray = []
        self.pendingArray  = []
        
        if self.appointmentTodayArray.count != 0 {
            for appointment in self.appointmentTodayArray {
                if isAppointmentMissed(appointment.status!, date: appointment.date) {
                    self.missedArray.append(appointment) //MISSED
                } else if isAppointmentFinished(appointment.status!) {
                    self.finishedArray.append(appointment) //FINISHED
                } else {
                    self.pendingArray.append(appointment) //PENDING
                }
            }
        }
    }
    
    func appointmentDetails(#from : String) -> [Appointment] {
        switch from {
            case PENDING  : return self.pendingArray
            case MISSED   : return self.missedArray
            case FINISHED : return self.finishedArray
            default : return self.appointmentTodayArray
        }
    }
    
    func setAppointmentIcon(#from: String) {
        switch from {
        case PENDING  : filterButton.setImage(UIImage(named: pendingIcon), forState: UIControlState.Normal)
        case MISSED   : filterButton.setImage(UIImage(named: missedIcon), forState: UIControlState.Normal)
        case FINISHED : filterButton.setImage(UIImage(named: finishedIcon), forState: UIControlState.Normal)
        default : filterButton.setImage(UIImage(named: allTodayIcon), forState: UIControlState.Normal)
        }
    }
    
    //MARK: Timer
    func startTimer() {
        println("Timer started")
        let queue = dispatch_queue_create("com.domain.app.timer", nil)
            timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue)
        dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC, 1 * NSEC_PER_SEC) // every 10 seconds, with leeway of 1 second
        dispatch_source_set_event_handler(timer) {
            self.setUpBanner()
            self.checkAppointments()
            self.separateAppointments()
        }
//        dispatch_resume(timer)0
    }
    
    func stopTimer() {
        dispatch_source_cancel(timer)
        timer = nil
    }
    
    //MARK: Patient Count Today / Yesterday
    func setPatientCount(#yesterdayArray: NSMutableArray , todayArray: NSMutableArray) {
        var todayPatientWord     = todayArray.count <= 1 ? "Patient" : "Patients"
        var yesterdayPatientWord = yesterdayArray.count <= 1 ? "Patient" : "Patients"
        
        self.todayPatientLabel.text     = "\(todayArray.count) \(todayPatientWord)"
        self.yesterdayPatientLabel.text = "\(yesterdayArray.count) \(yesterdayPatientWord)"
    }
    
    func removeDuplicatePatient(appointmentArray:[Appointment]) -> NSMutableArray {
        var removedDuplicate : [Appointment] = []
        var removedDuplicates = NSMutableArray()
        
        for appointment in appointmentArray {
            if !removedDuplicates.containsObject(appointment.patientId) {
                removedDuplicates.addObject(appointment.patientId)
            }
        }
        
        return removedDuplicates
    }

    //MARK:UITableView
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : MGSwipeTableCell!
        var appointmentArray = appointmentDetails(from: SELECTED)
        
        if appointmentArray.count == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("EmptyStateCell", forIndexPath: indexPath) as MGSwipeTableCell
            var headerLabel  = cell.viewWithTag(2) as UILabel
            var messageLabel = cell.viewWithTag(3) as UILabel
            
            switch SELECTED {
                case PENDING  : headerLabel.text  = "No Pending Appointment."
                                messageLabel.text = "Appointments are clear today"
                case MISSED   : headerLabel.text  = "No Missed Appointment."
                                messageLabel.text = "Missed Appointment(s) can be found here"
                case FINISHED : headerLabel.text  = "No Finished Appointment."
                                messageLabel.text = "Cleared Appointment(s) can be found here"
                default:        headerLabel.text  = "No Appointment."
                                messageLabel.text = "Appointment(s) can be found here"
            }
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("todayCell", forIndexPath: indexPath) as MGSwipeTableCell
            var initialsImageView = cell.viewWithTag(tagPhotoImage) as UIImageView
            var dayLabel          = cell.viewWithTag(tagDay)        as UILabel
            var timeLabel         = cell.viewWithTag(tagTime)       as UILabel
            var timeAheadLabel    = cell.viewWithTag(tagAheadTime)  as UILabel
            var fullNameLabel     = cell.viewWithTag(tagName)       as UILabel
            var statusImage       = cell.viewWithTag(20)            as UILabel
            var complaintLabel    = cell.viewWithTag(tagComplaint)  as SummaryTextView
            
            var details             = appointmentArray[indexPath.row] as Appointment
                fullNameLabel.text  = "\(details.lastName) \(details.firstName), \(details.middleName)"
                complaintLabel.text = details.complaint
                timeLabel.text      = self.getTime(details.date)
                dayLabel.text       = self.getWeekDay(self.convertDateFromString(details.date, withTime:true)!, isComplete: false)
                timeAheadLabel.text = self.timeAhead(self.convertDateFromString(details.date, withTime:true)!)
            
            initialsImageView.setImageWithString("\(details.firstName) \(getInitialLetter(details.lastName))", color: eRedColor(), circular: true)
            initialsImageView.layer.cornerRadius = initialsImageView.frame.height / 2
            initialsImageView.clipsToBounds = true
            
            if self.hasCacheFor(details.patientId) == true {
                let patientDetail = self.patientInfo.getDictionary(self.getCacheFor(details.patientId) as NSDictionary)
                
                if let photoFileId = patientDetail.photo {
                    if photoFileId != "" {
                        self.userAccount.getImageWithBlock(photoFileId, withBlock: { (succeeded:Bool?, object:AnyObject?) -> Void in
                            
                            if succeeded == true {
                                let image = object as UIImage
                                initialsImageView.image = image
                            } else {
                                initialsImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.eRedColor(), circular: true)
                            }
                        })
                    } else {
                        initialsImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.eRedColor(), circular: true)
                    }
                } else {
                    initialsImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.eRedColor(), circular: true)
                }
                
            }
            
            var buttonArray:NSMutableArray = []
            
            if isAppointmentFinished(details.status!) {
                statusImage.text       = ""
                statusImage.hidden     = false
                cell.contentView.alpha = 0.8
            } else if isAppointmentMissed(details.status!, date: details.date) {
                statusImage.text       = ""
                statusImage.hidden     = false
                cell.contentView.alpha = 0.8
                buttonArray.addObject(self.cellDeleteButton(indexPath, appointment: details) as MGSwipeButton)
                buttonArray.addObject(self.cellResceduleButton(indexPath, appointment: details) as MGSwipeButton)
            } else {
                statusImage.hidden     = true
                cell.contentView.alpha = 1.0
                buttonArray.addObject(self.cellDeleteButton(indexPath, appointment: details) as MGSwipeButton)
            }
            
            cell.rightButtons = buttonArray
            cell.rightSwipeSettings.transition = MGSwipeTransition.Border
        }
        
        return cell
    }
    
    func cellDeleteButton(indexPath: NSIndexPath, appointment: Appointment) -> MGSwipeButton {
        var button: MGSwipeButton = MGSwipeButton(title: "     ", backgroundColor: self.eRedColor()) { (sender:MGSwipeTableCell!) -> Bool in
            let alert = UIAlertView()
            alert.title = "EMR"
            alert.message = "Are you sure you want to cancel appointment with \(appointment.firstName) \(appointment.lastName)?"
            alert.addButtonWithTitle("No")
            alert.addButtonWithTitle("Yes")
            alert.delegate = self
            alert.show()
            self.selectedIndex = indexPath
            return false
        }
        button.titleLabel?.font = UIFont(name: "fontawesome", size: 30)
        return button
    }
    
    func cellResceduleButton(indexPath: NSIndexPath, appointment: Appointment) -> MGSwipeButton {
        var button: MGSwipeButton = MGSwipeButton(title: "       ", backgroundColor: self.eLightRedColor()) { (sender:MGSwipeTableCell!) -> Bool in
            
            var aavc : AddAppointmentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddAppointmentViewController") as AddAppointmentViewController
            var dict = self.getCacheFor(appointment.patientId) as NSDictionary
            aavc.info = self.patientInfo.getDictionary(dict) as PatientInfo
            aavc.appointmentDetails = appointment
            aavc.isViewAppointment  = true
            aavc.userCanEdit        = true
            self.navigationController?.pushViewController(aavc, animated: true)
            return true
        }
        button.titleLabel?.font = UIFont(name: "fontawesome", size: 30)
        return button
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if SELECTED != MISSED {
            var appointmentArray = appointmentDetails(from: SELECTED)
            var madvc = self.storyboard?.instantiateViewControllerWithIdentifier("MoreAppointmentDetailsViewController") as MoreAppointmentDetailsViewController
                madvc.isViewMoreDetails  = false
                madvc.appointmentDetails = appointmentArray[indexPath.row] as Appointment
            self.navigationController?.pushViewController(madvc, animated: true)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointmentDetails(from: SELECTED).count == 0 ? 1 : appointmentDetails(from: SELECTED).count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch SELECTED {
            case PENDING  : return "Pending Schedules (\(self.pendingArray.count))"
            case MISSED   : return "Missed Schedules (\(self.missedArray.count))"
            case FINISHED : return "Schedules Done (\(self.finishedArray.count))"
            default : return "Schedules for today (\(self.appointmentTodayArray.count))"
        }
    }
    
    //MARK: UICollectionView Cell
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("patientCell", forIndexPath: indexPath) as UICollectionViewCell
        var photoImageView = cell.viewWithTag(100) as UIImageView
        var name = cell.viewWithTag(101) as UILabel
        
        photoImageView.layer.cornerRadius = photoImageView.frame.height / 2
        photoImageView.clipsToBounds = true
        var patientDetail = self.recentPatient[indexPath.row] as PatientInfo
        if let photoFileId = patientDetail.photo {
            if photoFileId != "" {
                self.userAccount.getImageWithBlock(photoFileId, withBlock: { (succeeded:Bool?, object:AnyObject?) -> Void in
                    
                    if succeeded == true {
                        let image = object as UIImage
                        photoImageView.image = image
                    } else {
                        photoImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.eRedColor(), circular: true)
                    }
                })
            } else {
                photoImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.eRedColor(), circular: true)
            }
        } else {
            photoImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.eRedColor(), circular: true)
        }
        
        name.text = "\(patientDetail.lastName), \(patientDetail.firstName)"
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.recentPatient.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var info = self.recentPatient[indexPath.row] as PatientInfo
        var ppvc = self.storyboard?.instantiateViewControllerWithIdentifier("PatientProfileViewController") as PatientProfileViewController
            ppvc.patientDetails = info
        self.navigationController?.pushViewController(ppvc, animated: true)
    }

    //MARK: Others
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: AlertView
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            self.showHUD(true)
            self.HUD.labelText = "Cancelling Appointment..."
            let appointmentArray = appointmentDetails(from: SELECTED)
            let appointment = appointmentArray[selectedIndex.row] as Appointment
            self.appointments.deleteAppointmentWithEntryId(appointment.entryId!, inBlock: { (succeeded:Bool?, error:AnyObject?) -> Void in
                if succeeded == true {
                    self.showHUD(false)
                    self.showHUD(true)
                    self.HUD.labelText = "Retrieving Appointments..."
                    self.showAlert("Successfully Cancelled appointment!")
                    NSNotificationCenter.defaultCenter().postNotificationName(self.fetchAppointmentListener, object: nil)
                } else {
                    self.showHUD(false)
                    self.showAlert("Oops! something went wrong. Please try again.")
                }
            })
        }
    }

}

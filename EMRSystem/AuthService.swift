//
//  AuthService.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 4/1/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class AuthService: NSObject {
    var userCredentials     : CatalyzeUser!
    var allAppointmentArray : [Appointment]!
    var allPatientsArray    : [PatientInfo]!
    var isUserAdmin         : Bool!
    let isAdmin             = "isAdmin"
    let doctorId            = "doctorId"
    
    var assistantInfo      : AssistantInfo = AssistantInfo()
    typealias SuccessBlock = (Bool?, AnyObject?) -> Void
    typealias SaveBlock    = (Bool?, AnyObject?) -> Void
    typealias ResultBlock  = ([AnyObject]?, AnyObject?) -> Void
    
    class var sharedInstance : AuthService {
        struct Singleton {
            static let instance = AuthService()
        }
        
        return Singleton.instance
    }
    
    func loginuser(username: String, password: String, withBlock: SuccessBlock) {
        CatalyzeUser.logInWithUsernameInBackground(username, password: password, success: { (user: CatalyzeUser!) -> Void in
            AuthService.sharedInstance.userCredentials = user
            println("USERID: \(user.usersId)")
            self.assistantInfo.fetchAllAssistantInfo()
            

            
//            self.assistantInfo.saveCurrentUserToAssistantInfo({ (isSuccess: Bool?, error: AnyObject?) -> Void in
//                if isSuccess == true {
//                    println("SAVING SUCCESS")
//                } else {
//                    println("SAVING FAILED")
//                }
//            })
//            AuthService.sharedInstance.isUserAdmin     = user.extraForKey(self.isAdmin) as Bool
//            var doctor = user.extraForKey(self.doctorId) as String
            println(AuthService.sharedInstance.isUserAdmin)
//            println("DR: \(doctor)")
            withBlock(true, nil)
    
            }) { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
            withBlock(false, result)
        }
    }
    
    func signUpUser(username: String, email: Email, name: Name, password: String, doctorID: String?, isAdminStatus: Bool, withBlock: SuccessBlock) {
        var extraDict : NSDictionary = NSDictionary()
        extraDict = isAdminStatus != true ? [isAdmin : isAdminStatus , doctorId: doctorID!] : [isAdmin : isAdminStatus]
        
        println("Dict: \(extraDict)")
        CatalyzeUser.signUpWithUsernameInBackground(username, email: email, name: name, password: password, inviteCode: "12345", extras: extraDict, success: { (user: CatalyzeUser!) -> Void in
            println("user: \(user)")
            withBlock(true, nil)
            }) { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
            withBlock(false, result)
        }
    }
    
    /*NOTE: All appointments were always being
            saved (when created or updated)
            on sharedInstance but not the appointments 
            that were fetched with a distinct patientId 
            (using this method fetchAllAppointmentsWithPatientId)
            */
}

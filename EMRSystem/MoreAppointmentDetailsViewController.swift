//
//  MoreAppointmentDetailsViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 4/8/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class MoreAppointmentDetailsViewController: UITableViewController, KSTokenViewDelegate {
    
    var isViewMoreDetails : Bool = false
    var info : PatientInfo!
    var appointmentDetails: Appointment!
    var base = BaseViewController()
    
    var patientEntryId: String!
    
    //Token Arrays
    var medicalValues: Array<String> = List.sharedInstance.medicalList
    var allergiesTokenArray  : [String] = []
    var observationTokenArray: [String] = []
    var assessmentTokenArray : [String] = []
    var planTokenArray       : [String] = []
    
    @IBOutlet weak var additionalButtonsCell: UITableViewCell!
    @IBOutlet weak var complaintCell        : UITableViewCell!
    @IBOutlet weak var headerCell           : UITableViewCell!
    @IBOutlet weak var timeLabel            : UILabel!
    @IBOutlet weak var nameLabel            : UILabel!
    @IBOutlet var textViewArray             : [UITextView]!
    @IBOutlet weak var complaintTextView    : UITextView!
    
    @IBOutlet weak var vitalsTextView       : UITextView!
    @IBOutlet weak var notesTextView        : UITextView!
    @IBOutlet weak var medicationTextView   : UITextView!
    @IBOutlet weak var allergyTokenView     : KSTokenView!
    @IBOutlet weak var observationTokenView : KSTokenView!
    @IBOutlet weak var assessmentTokenView  : KSTokenView!
    @IBOutlet weak var planTokenView        : KSTokenView!
    
    @IBOutlet var tokenViewCollection: [KSTokenView]!
    
    //MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.setUpTokenViews()
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        
        for field in self.textViewArray {
            var textView = field as UITextView
            textView.layer.borderColor  = UIColor.lightGrayColor().CGColor
            textView.layer.borderWidth  = 1.0
            textView.layer.cornerRadius = 5.0
        }
        
        if isViewMoreDetails {
            self.headerCell.hidden            = true
            self.headerCell.frame             = CGRectZero
            self.complaintCell.hidden         = true
            self.complaintCell.frame          = CGRectZero
            self.additionalButtonsCell.hidden = true
            self.tableView.reloadData()
        } else {
            self.headerCell.hidden = false
            self.headerCell.frame  = CGRectZero
            nameLabel.text         = "\(appointmentDetails.lastName) \(appointmentDetails.firstName),  \(appointmentDetails.middleName)"
            timeLabel.text         = base.getTime(appointmentDetails?.date)
            complaintTextView.text = appointmentDetails?.complaint
        }
        
        if appointmentDetails.status == true {
            self.setUpAppointmentDetails()
        }
    }
    
    func setUpTokenViews () {
        for view in tokenViewCollection {
            view.delegate    = self
            view.promptText  = ""
            view.style       = .Squared
            view.font        = UIFont(name: "STHeitiTC-Medium", size: 14)!
            
            view.layer.borderColor  = UIColor.lightGrayColor().CGColor
            view.layer.borderWidth  = 1.0
            view.layer.cornerRadius = 6.0
            view.contentMode        = UIViewContentMode.TopLeft
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        if appointmentDetails.status != true {
            self.base.HUD.hide(true)
        }
    }
    
    func setUpNavigationBar() {
        if isViewMoreDetails {
            self.navigationItem.title = info.lastName + "," + info.firstName + " " + info.middleName
        } else {
            if appointmentDetails.status != true {
                var saveButton : UIBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Plain, target: self, action: "saveAppointmentDetails")
                self.navigationItem.rightBarButtonItem = saveButton
                self.navigationItem.title = "Today"
            } else {
                self.navigationItem.title = "View Appointment Details"
            }
        }
    }
    
    //MARK: KSTokenView
    func tokenView(token: KSTokenView, performSearchWithString string: String, completion: ((results: Array<AnyObject>) -> Void)?) {
        var data: Array<String> = []
        for value: String in medicalValues {
            if value.lowercaseString.rangeOfString(string.lowercaseString) != nil {
                data.append(value)
            }
        }
        completion!(results: data)
    }
    
    func tokenView(tokenView: KSTokenView, didChangeFrame frame: CGRect) {
        tokenView.frame = CGRectMake(42, 43, 635, 72)
    }
    
    func tokenView(tokenView: KSTokenView, didAddToken token: KSToken) {
        switch tokenView.tag {
        case 1 : allergiesTokenArray.append("\(token.title)")
        case 3 : observationTokenArray.append("\(token.title)")
        case 4 : assessmentTokenArray.append("\(token.title)")
        case 5 : planTokenArray.append("\(token.title)")
        default: println("ADD TokenView with Tag 2")
        }
    }
    
    func tokenView(tokenView: KSTokenView, didDeleteToken token: KSToken) {
        switch tokenView.tag {
        case 1 : allergiesTokenArray   = base.deleteTokenFromArray(token.title, tokenArray: allergiesTokenArray)
        case 3 : observationTokenArray = base.deleteTokenFromArray(token.title, tokenArray: observationTokenArray)
        case 4 : assessmentTokenArray  = base.deleteTokenFromArray(token.title, tokenArray: assessmentTokenArray)
        case 5 : planTokenArray        = base.deleteTokenFromArray(token.title, tokenArray: planTokenArray)
        default: println("DEL TokenView with Tag 2")
        }
    }
    
    func tokenView(token: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        return object as String
    }
    
    func tokenView(tokenView: KSTokenView, shouldAddToken token: KSToken) -> Bool {
        let list: List = List()
        list.addToObjectList(token.title, type: ListType.Location)
        self.medicalValues = List.sharedInstance.medicalList
        return true
    }

    //MARK: Set Appointment Details (View / Edit)
    func setUpAppointmentDetails() {
        self.complaintTextView.text   = appointmentDetails.complaint
        self.vitalsTextView.text      = appointmentDetails.vitals
        self.notesTextView.text       = appointmentDetails.notes
        self.medicationTextView.text  = appointmentDetails.medication
        self.allergyTokenView.placeholder     = base.convertToStringSet(appointmentDetails.allergy!)
        self.observationTokenView.placeholder = base.convertToStringSet(appointmentDetails.observation!)
        self.assessmentTokenView.placeholder  = base.convertToStringSet(appointmentDetails.assessment!)
        self.planTokenView.placeholder        = base.convertToStringSet(appointmentDetails.plan!)
        
        //not yet settled - if details can be edited after it has been saved 
        self.complaintTextView.userInteractionEnabled    = false
        self.vitalsTextView.userInteractionEnabled       = false
        self.notesTextView.userInteractionEnabled        = false
        self.medicationTextView.userInteractionEnabled   = false
        self.allergyTokenView.userInteractionEnabled     = false
        self.observationTokenView.userInteractionEnabled = false
        self.assessmentTokenView.userInteractionEnabled  = false
        self.planTokenView.userInteractionEnabled        = false
    }
    
    //MARK: Save Appointment Details
    func saveAppointmentDetails() {
        if base.isConnectedToNetwork() {
            self.base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            var status = isViewMoreDetails ? false : true
            base.appointments.saveStartedAppointmentDetails(appointmentDetails.entryId!, vitals: vitalsTextView.text, notes: notesTextView.text, medication: medicationTextView.text, allergy: allergiesTokenArray, observation: observationTokenArray, assessment: assessmentTokenArray, plan: planTokenArray , isAppointmentDone: status) { (result: Bool?, error: AnyObject?) -> Void in
                self.base.HUD.hide(true)
                if result != false {
                    self.base.showAlert("Appointment Done \n Details Saved")
                    NSNotificationCenter.defaultCenter().postNotificationName(self.base.fetchAppointmentListener, object: nil)
                    self.navigationController?.popViewControllerAnimated(true)
                } else {
                    self.base.showAlertError(error!)
                }
            }
        } else {
            self.base.isConnectedToNetwork()
        }
    }
    
    //MARK: UITableView Delegate
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return isViewMoreDetails ? 0 : 58
        case 1:
            return isViewMoreDetails ? 0 : 128
        default:
            return 128
        }
    }

}

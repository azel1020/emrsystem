//
//  MasterSplitViewController.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 4/17/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class MasterSplitViewController: UISplitViewController {

    var base: BaseViewController = BaseViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredDisplayMode = .PrimaryHidden
        self.setListener()
       // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setListener() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "logInUser:", name: self.base.splitviewListener, object: nil)
    }
    
    func logInUser(noti:NSNotification) {
        var isLogin = noti.object as Bool
        self.hideOrShowRootViewController(isLogin)
        if isLogin {
            NSNotificationCenter.defaultCenter().postNotificationName(self.base.menuUserListener, object:nil)
            NSNotificationCenter.defaultCenter().postNotificationName(self.base.navigationListener, object:"DashboardViewController")
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName(self.base.navigationListener, object:"LoginViewController")
        }
    }
    
    func hideOrShowRootViewController(isShow: Bool) {
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            if isShow {
                self.preferredDisplayMode = .AllVisible
            } else {
                self.preferredDisplayMode = .PrimaryHidden
            }
        }, completion: nil)
    }
}

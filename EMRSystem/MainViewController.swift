//
//  ViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/23/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController, UISearchControllerDelegate , UISearchBarDelegate , UITableViewDelegate, UITableViewDataSource , UIAlertViewDelegate {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var appointmentTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var appointmentButton: UIButton!

    @IBOutlet weak var yesterdayPatientLabel: UILabel!
    @IBOutlet weak var todayPatientLabel: UILabel!
    
    @IBOutlet weak var EmptyStateView: UIView!
    
    var filteredPatientInfo : [PatientInfo] = []
    var todayScheduleArray  : [Appointment] = []
    var patientInfoArray    : [PatientInfo]!
    var grayBackgroudView   : UIView!
    var searchTableView     : UITableView!
    
    let searchCellIdentifier = "searchCell"
    var selectedIndex        = 0

    //MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Dashboard"

//        self.activityIndicator.startAnimating()
//        self.setUpNavigationBar()
//        self.setUpSearchTableView()
//        self.fetchAllPatients()
//        self.appointmentTableView.tableFooterView?.hidden = true
//        self.fetchAppointmentsToday()
//
//        self.appointmentButton.enabled  = false
//        self.todayPatientLabel.text     = ""
//        self.yesterdayPatientLabel.text = ""
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "fetchAppointmentsToday", name: updateAppointmentList, object: nil)
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "fetchAllPatients", name: updatePatientList, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
//        self.showTableView(false)
    }
    
    func setUpSearchTableView() {
        var tapDismiss : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissSearchTableView")
        
        self.grayBackgroudView = UIView(frame: CGRectMake(0, 40, self.view.frame.width, self.view.frame.height))
        self.grayBackgroudView.backgroundColor = UIColor.blackColor()
        self.grayBackgroudView.hidden = true
        self.grayBackgroudView.alpha  = 0
        
        self.searchTableView = UITableView(frame: CGRectMake(50, 50, self.grayBackgroudView.frame.width - 100, self.grayBackgroudView.frame.height - 150), style: UITableViewStyle.Grouped)
        self.searchTableView.registerNib(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: searchCellIdentifier)
        
        self.searchTableView.backgroundColor    = UIColor.clearColor()
        self.searchTableView.tableFooterView    = UIView(frame: CGRectZero)
        self.searchTableView.layer.cornerRadius = 5.0
        self.searchTableView.clipsToBounds      = true
        self.searchTableView.alpha              = 0
        self.searchTableView.hidden             = true
        self.searchTableView.delegate           = self
        self.searchTableView.dataSource         = self
        
        self.grayBackgroudView.addGestureRecognizer(tapDismiss)
        self.view.addSubview(self.grayBackgroudView)
        self.view.addSubview(self.searchTableView)
        self.view.bringSubviewToFront(self.searchTableView)
    }
    
    func setUpNavigationBar() {
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBarHidden = false
        var logoutButton : UIBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Bordered, target: self, action: "logout")
        self.navigationItem.rightBarButtonItem = logoutButton
    }
    
    func logout() {
        CatalyzeUser.currentUser().logout()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    //MARK: Fetch Patient Info and Appointments
    func fetchAllPatients() {
        HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        patientInfo.fetchAllPatient { (result: [AnyObject]?, error: AnyObject?) -> Void in
            self.HUD.hide(true)
            if error == nil {
                self.patientInfoArray = result as [PatientInfo]
                self.searchBar.userInteractionEnabled = true
            } else {
                self.showAlertError(error!)
            }
        }
    }
    
    func fetchAppointmentsToday() {
        self.todayScheduleArray = []
        appointments.fetchAllAppointments { (result: [AnyObject]?, error: AnyObject?) -> Void in
            if error == nil {
                var appointmentsArray = result as [Appointment]
                var yesterdayArray = NSMutableArray()
                var todayArray = NSMutableArray()
                for appointment in appointmentsArray {
                    var convertedDate = self.convertDateFromString(appointment.date, withTime: true) as NSDate!
                    if self.isDateToday(convertedDate) {
                        self.todayScheduleArray.append(appointment)
                        todayArray.addObject(appointment.patientId)
                    } else if self.isDateYesterday(convertedDate) {
                        yesterdayArray.addObject(appointment.patientId)
                    }
                }
                
                self.todayScheduleArray = self.sortAppointmenByDate(self.todayScheduleArray, isAscending: false)
                AuthService.sharedInstance.allAppointmentArray = appointmentsArray
                self.appointmentButton.enabled = true
                
                self.setPatientCount(self.removeDuplicatePatient(yesterdayArray), todayArray: self.removeDuplicatePatient(todayArray))
                
                
                self.setEmptyState()
                
                self.appointmentTableView.delegate   = self
                self.appointmentTableView.dataSource = self
                self.appointmentTableView.reloadData()
            
                
                self.activityIndicator.stopAnimating()
            } else {
                self.showAlertError(error!)
            }
        }
    }
    
    //MARK: Empty State
    func setEmptyState() {
        self.appointmentTableView.tableFooterView?.hidden = false
        if self.todayScheduleArray.count == 0 {
            self.EmptyStateView.hidden = false
        } else {
            self.EmptyStateView.hidden = true
        }
    }
    
    //MARK: Patient Count Today / Yesterday
    func setPatientCount(yesterdayArray:NSMutableArray, todayArray:NSMutableArray) {
        var patientWord = "Patients"
        if todayArray.count <= 1 {
            patientWord = "Patient"
        }
        self.todayPatientLabel.text = "\(todayArray.count) \(patientWord)"
        if yesterdayArray.count <= 1 {
            patientWord = "Patient"
        } else {
            patientWord = "Patients"
        }
        self.yesterdayPatientLabel.text = "\(yesterdayArray.count) \(patientWord)"
    }
    
    func removeDuplicatePatient(appointmentArray:NSMutableArray) -> NSMutableArray {
        var removedDuplicate = NSMutableArray()
        for id:AnyObject in appointmentArray {
            if let patientId = id as? NSString {
                if !removedDuplicate.containsObject(patientId) {
                    removedDuplicate.addObject(patientId)
                }
            }
        }
        return removedDuplicate
    }
    
    //MARK: UISearchBar Delegate
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        var keywordSearch = searchBar.text
        filteredPatientInfo = []
        
        for info in patientInfoArray {
            if containsString(info.firstName, keyword: keywordSearch)  ||
               containsString(info.middleName, keyword: keywordSearch) ||
               containsString(info.lastName, keyword: keywordSearch)   {
                filteredPatientInfo.append(info)
            }
        }
        
        if filteredPatientInfo.count == 0 {
            self.showAlert("No Result found for \(keywordSearch)")
        } else {
            self.showTableView(true)
        }
    }
    
    func dismissSearchTableView() {
        self.showTableView(false)
    }
    
    func showTableView(isShow: Bool) {
        self.view.endEditing(true)        
        if isShow {
            self.searchTableView.reloadData()
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                self.searchBar.frame = CGRectMake(0, 0, self.view.frame.width, self.searchBar.frame.height)
                self.grayBackgroudView.alpha  = 0.5
                self.grayBackgroudView.hidden = false
                self.searchTableView.alpha    = 0.9
                self.searchTableView.hidden   = false
            })
        } else {
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                self.searchBar.frame = CGRectMake(354, 0, 670, self.searchBar.frame.height)
                self.grayBackgroudView.alpha  = 0
                self.grayBackgroudView.hidden = true
                self.searchTableView.alpha    = 0
                self.searchTableView.hidden   = true
            })
        }
    }
    
    //MARK: UITableView Delegate & DataSource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell!
        if tableView == self.searchTableView {
            cell = tableView.dequeueReusableCellWithIdentifier(searchCellIdentifier, forIndexPath: indexPath) as UITableViewCell
            cell.clipsToBounds      = true
            cell.layer.cornerRadius = 5.0
            cell.layer.borderWidth  = 1.0
            cell.layer.borderColor  = UIColor(red: (209/255.0), green: (210/255.0), blue: (206/255.0), alpha: 1.0).CGColor
            
            var nameLabel           = cell.viewWithTag(100) as UILabel
            var ageGenderLabel      = cell.viewWithTag(101) as UILabel
            var photoView           = cell.viewWithTag(102) as UIImageView
            
            let info = filteredPatientInfo[indexPath.row] as PatientInfo
            nameLabel.text          = info.lastName + ", " + info.firstName + " " + info.middleName
            ageGenderLabel.text     = String(info.age) + " " + info.gender
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
            var timeLabel           = cell.viewWithTag(100) as UILabel
            var nameLabel           = cell.viewWithTag(101) as UILabel
            var ageGenderLabel      = cell.viewWithTag(102) as UILabel
            var complaintLabel      = cell.viewWithTag(103) as SummaryTextView
            var cellView            = cell.viewWithTag(105) as UIView!
            var markLabel           = cell.viewWithTag(106) as UILabel
            
            if todayScheduleArray.count != 0 {
                let appointmentInfo      = todayScheduleArray[indexPath.row] as Appointment
                nameLabel.text           = "\(appointmentInfo.lastName) \(appointmentInfo.firstName), \(appointmentInfo.middleName)"
                timeLabel.text           = getTime(appointmentInfo.date)
                ageGenderLabel.text      = appointmentInfo.patientAge + " " + appointmentInfo.patientGender
                complaintLabel.text      = appointmentInfo.complaint
                complaintLabel.textColor = UIColor.lightGrayColor()
                cellView.hidden          = false
                
                if appointmentInfo.status == true {
                    markLabel.textColor  = eDoneStatusColor()
                } else {
                    markLabel.textColor  = ePendingStatusColor()
                }
            } else {
                cellView.hidden          = true
                cell.backgroundColor     = eBackgroundColor()
            }
        }

         return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if tableView == self.searchTableView {
            var apvc = self.storyboard?.instantiateViewControllerWithIdentifier("AddPatientViewController") as AddPatientViewController
                apvc.info = filteredPatientInfo[indexPath.row] as PatientInfo
                apvc.isViewPatientInfo = true
            self.navigationController?.pushViewController(apvc, animated: true)
        } else {
            if todayScheduleArray.count != 0 {
                selectedIndex = indexPath.row
                var todayAppointment = todayScheduleArray[indexPath.row] as Appointment
                if todayAppointment.status == false {
                    self.checkStatusForAppointment(convertDateFromString(todayAppointment.date, withTime: true)!)
                } else {
                    self.startAppointment()
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchTableView {
            return filteredPatientInfo.count
        } else {
            return todayScheduleArray.count
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            return 100
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == self.searchTableView {
            return "Search Results"
        }
        return "Schedules Today"
    }
    
    //MARK: Appointment Section
    func checkStatusForAppointment(date: NSDate) {
        if NSDate().compare(date) == NSComparisonResult.OrderedAscending {
            var alertView : UIAlertView = UIAlertView(title: "EMR", message: "Already start appointment?", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "No" , "Yes")
            alertView.show()
        } else {
            self.startAppointment()
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            self.startAppointment()
        }
    }
    
    func startAppointment() {
        var madvc = self.storyboard?.instantiateViewControllerWithIdentifier("MoreAppointmentDetailsViewController") as MoreAppointmentDetailsViewController
        madvc.isViewMoreDetails = false
        madvc.appointmentDetails = todayScheduleArray[selectedIndex] as Appointment
        self.navigationController?.pushViewController(madvc, animated: true)
    }
}


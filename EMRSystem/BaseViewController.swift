//
//  BaseViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/23/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

enum CameraSettings:Int {
    case ipadCamera = 0
    case ipadCameraRoll
}

enum ListType:Int {
    case Medical = 0
    case Location
}

class BaseViewController: UIViewController {
    
    //GCD
    let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
    
    let authService : AuthService = AuthService()
    let patientInfo : PatientInfo = PatientInfo()
    let appointments: Appointment = Appointment()
    let userAccount: UserAccount  = UserAccount()
    
    var HUD : MBProgressHUD = MBProgressHUD()
    let noNetworkConnectionMessage = "Please check your network connection"

    //Notification center
    let updatePatientProfile     = "updatePatientProfile"  //for single viewing of patient profiles
    let updateAppointmentList    = "updateAppointmentList" //for appointment list table
    let patientListener          = "updatePatientTable"
    let fetchAppointmentListener = "fetchAppointments"    //fetching all appointments
    let fetchPatientListener     = "fetchPatientList"     //fetching all patients
    let userProfileListener      = "setUserProfile"
    let patientNoteListener      = "getPatientsNote"
    let viewPatientNoteListener      = "viewPatientsNote"
    let drawingListener          = "setDrawing"
    
    let navigationListener       = "setViewcontrollers"
    let splitviewListener        = "hideOrShowRootViewController"
    let menuUserListener         = "setUserDetail"
    let patientHUDListener       = "showPatientHUD"

    //Appointment Keys
    let keyTitle            = "title"
    let keyAppointment      = "appointment"
    let keyEmptyTitle       = "emptyStateTitle"
    let keyEmptySubtitle    = "emptyStateSubtitle"
    let keyIsEmpty          = "isEmpty"
    
    //Image Keys
    let keyImage        = "image"
    let keyFileId       = "fileId"
    let keyHasImage     = "hasImage"
    let keyToBeDeleted  = "toBeDeleted"
    let keyIsNew        = "isNew"
    
    //Appointment Cell tags
    let tagPhotoImage   = 1
    let tagDay          = 2
    let tagTime         = 3
    let tagFullDate     = 4
    let tagAheadTime    = 5
    let tagAgoTime      = 6
    let tagName         = 7
    let tagComplaint    = 8
    
    //Date Dictionary Keys
    let keyDateDictionaryDay        = "day"
    let keyDateDictionaryMonth      = "month"
    let keyDateDictionaryYear       = "year"
    let keyDateDictionaryDate       = "dateCode"
    
    //Recent Patient
    let recentPatientCount = 5
    
    //APPointment Status
    let TODAY    = "Today"
    let PAST     = "Past"
    let UPCOMING = "Upcoming"
    
    //MARK: Format Date
    func getYear(date: NSDate) -> Int {
        let flags: NSCalendarUnit = .DayCalendarUnit | .MonthCalendarUnit | .YearCalendarUnit
        let components = NSCalendar.currentCalendar().components(flags, fromDate:date)
        
        return components.year
    }
    
    func getTime(date: String?) -> String {
        let formatterDate = NSDateFormatter()
            formatterDate.dateFormat = "hh:mm a"
        
        return "\(formatterDate.stringFromDate(convertDateFromString(date!, withTime: true)!))"
    }
    
    func getDateOnly(date: String?) -> String {
        let formatterDate = NSDateFormatter()
            formatterDate.dateFormat = "MMM dd, yyyy"
        
        return "\(formatterDate.stringFromDate(convertDateFromString(date!, withTime: true)!))"
    }
    
    func getWeekDay(date:NSDate , isComplete: Bool) -> String {
        let formatterDate = NSDateFormatter()
        if isComplete {
            formatterDate.dateFormat = "EEEE"
        } else {
            formatterDate.dateFormat = "EE"
        }
        
        var weekday = formatterDate.stringFromDate(date)
        return weekday.uppercaseString
    }
    
    func getMonth(date: NSDate) -> String {
        let formatterDate = NSDateFormatter()
        formatterDate.dateFormat = "MMMM"
        var month = formatterDate.stringFromDate(date)
        return month.uppercaseString
    }


    func formatDate(date: NSDate , withTime: Bool) -> String {
        let formatterDate = NSDateFormatter()
        if withTime {
            formatterDate.dateFormat = "MMM dd, yyyy hh:mm a"
        } else {
            formatterDate.dateFormat = "MMM dd, yyyy"
        }
        
        return formatterDate.stringFromDate(date)
    }
    
    func getDateDictionary(date : NSDate) -> NSDictionary {
        var dict = NSMutableDictionary()
        dict.setObject(self.getYearDatabaseFormat(date), forKey: keyDateDictionaryYear)
        dict.setObject(self.getDayDatabaseFormat(date), forKey: keyDateDictionaryDay)
        dict.setObject(self.getMonthDatabaseFormat(date), forKey: keyDateDictionaryMonth)
        dict.setObject(self.getDateDatabaseFormat(date), forKey: keyDateDictionaryDate)
        return NSDictionary(dictionary: dict)
    }
    
    func convertDateFromString(date: String , withTime: Bool) -> NSDate? {
        let formatterDate = NSDateFormatter()
            formatterDate.timeZone = NSCalendar.currentCalendar().timeZone        
        if withTime {
            formatterDate.dateFormat = "MMM dd, yyyy hh:mm a"
        } else {
            formatterDate.dateFormat = "MMM dd, yyyy"
        }
            
        return formatterDate.dateFromString(date)!
    }
    
    func dateComponents(date :NSDate) -> NSDateComponents {
        let flags: NSCalendarUnit = .DayCalendarUnit | .MonthCalendarUnit | .YearCalendarUnit | .HourCalendarUnit | .MinuteCalendarUnit | .SecondCalendarUnit
        let components = NSCalendar.currentCalendar().components(flags, fromDate:date)
        return components
    }
    
    func timeAhead(date:NSDate) -> String {
        let future = self.dateComponents(date) as NSDateComponents
        let now = self.dateComponents(NSDate()) as NSDateComponents
        var diff = future.hour - now.hour
        
        if diff == 0 {
            var minuteDiff = future.minute - now.minute
            if future.minute > now.minute {
                return "\(minuteDiff) minute to go"
            }
            return "Just now"
            
        } else if diff < 0 {
            return date.timeAgo()
        } else {
            if diff == 1 {
                return "\(diff) hour to go"
            } else {
                return "\(diff) hours to go"
            }
        }
    }
    
    func setGreetings() -> String {
        var time = self.dateComponents(NSDate()) as NSDateComponents
        var timeNow = time.hour
        
        if timeNow > 18 {
            return  "Good Evening"
        } else if timeNow > 12 {
            return "Good Afternoon"
        } else if timeNow > 6 {
            return "Good Morning"
        } else {
            return "Good Day"
        }

    }
    
    func dateEnd(date: NSDate) -> NSDate {
        var calendar = NSCalendar(identifier: NSGregorianCalendar)
        let components = self.dateComponents(date) as NSDateComponents
        components.hour = 23
        components.second = 59
        components.second = 59
        return calendar!.dateFromComponents(components)!
    }
    
    func dateStart(date: NSDate) -> NSDate {
        var calendar = NSCalendar(identifier: NSGregorianCalendar)
        let components = self.dateComponents(date) as NSDateComponents
        components.hour = 0
        components.second = 0
        components.second = 0
        return calendar!.dateFromComponents(components)!
    }
    
    func todayEnd() -> NSDate {
        return self.dateEnd(NSDate())
    }
    
    func todayStart() -> NSDate {
        return self.dateStart(NSDate())
    }
    
    func isDateUpcomming(date:NSDate) -> Bool {
        var end = date.compare(self.todayEnd() as NSDate)
        if end == NSComparisonResult.OrderedDescending {
            return true
        }
        return false
    }
    
    func isDateToday(date: NSDate) -> Bool {
        var start = date.compare(self.todayStart() as NSDate)
        var end = date.compare(self.todayEnd() as NSDate)
        if (start == NSComparisonResult.OrderedDescending && end == NSComparisonResult.OrderedAscending) || start == NSComparisonResult.OrderedSame || end == NSComparisonResult.OrderedSame {
            return true
        }
        return false
    }
    
    func isDatePassed(date: NSDate) -> Bool {
        var start = date.compare(self.todayEnd() as NSDate)
        if start == NSComparisonResult.OrderedAscending {
            return true
        }
        return false
    }
    
    func isDateYesterday(date: NSDate) -> Bool {
        var start = date.compare(self.yesterdayDate(true) as NSDate)
        var end = date.compare(self.yesterdayDate(false) as NSDate)
        if (start == NSComparisonResult.OrderedDescending && end == NSComparisonResult.OrderedAscending) || start == NSComparisonResult.OrderedSame || end == NSComparisonResult.OrderedSame {
            return true
        }
        return false
    }
    
    func yesterdayDate(isStart: Bool) -> NSDate {
        var dateComponent = NSDateComponents()
        dateComponent.day = -1
        let calendar = NSCalendar(identifier: NSGregorianCalendar) as NSCalendar!
        if isStart == true {
            return calendar.dateByAddingComponents(dateComponent, toDate: self.todayStart() as NSDate, options:NSCalendarOptions(0))!
        } else {
            return calendar.dateByAddingComponents(dateComponent, toDate: self.todayEnd() as NSDate, options:NSCalendarOptions(0))!
        }
    }
    
    func isDate(date:NSDate, isEqualTo comparedDate: NSDate) -> Bool {
        var dateCompare = date.compare(comparedDate)
        if dateCompare == NSComparisonResult.OrderedSame {
            return true
        }
        return false
    }
    
    func isDate(date:NSDate, isGreaterThan comparedDate: NSDate) -> Bool {
        var dateCompare = date.compare(comparedDate)
        if dateCompare == NSComparisonResult.OrderedAscending {
            return true
        }
        return false
    }
    
    func isDate(date:NSDate, isLesserThan comparedDate: NSDate) -> Bool {
        var dateCompare = date.compare(comparedDate)
        if dateCompare == NSComparisonResult.OrderedDescending {
            return true
        }
        return false
    }

    func sortAppointmenByDate(appointments:[Appointment] , isAscending: Bool) -> [Appointment] {
        var sortedAppointments = [Appointment]()
        for (var i=0; i < appointments.count; i++) {
            var appointment = appointments[i] as Appointment
            if i == 0 {
                sortedAppointments.append(appointment)
            } else {
                for (var k=0; k < sortedAppointments.count; k++) {
                    var sortedAppointment = sortedAppointments[k] as Appointment
                    
                    if isDate(self.convertDateFromString(sortedAppointment.date, withTime: true)!, isGreaterThan: self.convertDateFromString(appointment.date, withTime: true)!) ||
                        isDate(self.convertDateFromString(sortedAppointment.date, withTime: true)!, isEqualTo: self.convertDateFromString(appointment.date, withTime: true)!) {
                        sortedAppointments.insert(appointment,atIndex:k)
                        break
                    } else if k == (sortedAppointments.count-1) {
                        sortedAppointments.insert(appointment, atIndex: 0)
                        break
                    }
                }
            }
        }
        
        return isAscending ? sortedAppointments : sortedAppointments.reverse()
    }
    
    
    func getDateDatabaseFormat(date:NSDate) -> Int {
        var components = self.dateComponents(date) as NSDateComponents
        var month = self.getMonthDatabaseFormat(date)
        var day = self.getDayDatabaseFormat(date)
        var dateCodeString = "\(components.year)\(month)\(day)" as NSString
        return dateCodeString.integerValue
    }
    
    func getMonthDatabaseFormat(date:NSDate) -> NSString {
        var components = self.dateComponents(date) as NSDateComponents
        var month = toString(components.month) as NSString
        if month.length == 1 {
            month = "0\(month)"
        }
        return month
    }
    
    func getDayDatabaseFormat(date:NSDate) -> NSString {
        var components = self.dateComponents(date) as NSDateComponents
        var day = toString(components.day) as NSString
        if day.length == 1 {
            day = "0\(day)"
        }
        return day
    }
    
    func getYearDatabaseFormat(date: NSDate) -> NSString {
        var components = self.dateComponents(date) as NSDateComponents
        return "\(components.year)"
    }
    
    //MARK: Age
    func getAge(birthdate: NSDate) -> String {
        return "\(getYear(NSDate()) - getYear(birthdate))"
    }
    
    //MARK: Email & Search Validation
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        if let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx) {
            return emailTest.evaluateWithObject(email)
        }
        return false
    }
    
    func containsString(value: String, keyword: String) -> Bool {
        if NSString(string: value.lowercaseString).containsString(keyword.lowercaseString) {
            return true
        }
        return false
    }
    
    //MARK: AlertViews
    func showAlert(message : String) {
        var alert : UIAlertView = UIAlertView(title:"EMR" , message: message, delegate: self, cancelButtonTitle: "OK")
        alert.show()
    }
    
    func showAlertError(responseObject: AnyObject!) {
        var error = responseObject["errors"] as [Dictionary<String, AnyObject>]
        var errorMsg = error[0]["message"] as String

        var alertView : UIAlertView = UIAlertView(title:"Error", message: errorMsg, delegate: self, cancelButtonTitle: "Ok")
            alertView.show()
    
    }
    
    //MARK: Orientation
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    func setCameraOrientation () {
        self.setDeviceOrientation(UIDeviceOrientation.Portrait.rawValue)
    }
    
    func setLandscapeLeftOrientation () {
        self.setDeviceOrientation(UIDeviceOrientation.LandscapeLeft.rawValue)
    }
    
    func setDeviceOrientation(orientation:Int) {
        UIDevice.currentDevice().setValue(orientation, forKey: "orientation")
    }
    
    //MARK: Colors
    func eRedColor() -> UIColor {
        return UIColor(red: 130/255.0, green: 42/255.0, blue: 38/255.0, alpha: 1.0)
    }
    
    func eLightRedColor() -> UIColor {
        return UIColor(red: 182/255.0, green: 86/255.0, blue: 77/255.0, alpha: 1.0)
    }
    
    func eDarkRedColor() -> UIColor {
        return UIColor(red: 91/255.0, green: 30/255.0, blue: 27/255.0, alpha: 1.0)
    }
    
    func eLightBlueColor() -> UIColor {
        return UIColor(red: 223/255.0, green: 249/255.0, blue: 255/255.0, alpha: 1.0)
    }
    
    func eLightGreenColor() -> UIColor {
        return UIColor(red: 94/255.0, green: 136/255.0, blue: 158/255.0, alpha: 1.0)
    }
    
    func eBackgroundColor() -> UIColor {
        return UIColor(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1.0)
    }
    
    func eDoneStatusColor() -> UIColor {
        return UIColor(red: 203/255.0, green: 212/255.0, blue: 222/255.0, alpha: 1.0)
    }
    
    func ePendingStatusColor() -> UIColor {
        return UIColor(red: 30/255.0, green: 66/255.0, blue: 112/255.0, alpha: 1.0)        
    }
    
    //MARK: Check Network Reachability
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0)).takeRetainedValue()
        }
        
        var flags: SCNetworkReachabilityFlags = 0
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == 0 {
            return false
        }
        
        let isReachable = (flags & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection) ? true : false
    }
    
    //MARK: Appointments
    func isAppointmentMissed(status: Bool, date: String) -> Bool {
        if status != true && isDate(NSDate(), isLesserThan: self.convertDateFromString(date, withTime: true)!) {
            return true
        }
        return false
    }
    
    func isAppointmentFinished(status: Bool) -> Bool {
        return status == true ? true : false
    }
    
    func convertToDisplayGroupings(appointmentArray:[Appointment]) -> NSMutableArray {
        var mutableArray     :NSMutableArray = []
        var upcommingArray   :[Appointment] = []
        var todayArray       :[Appointment] = []
        var historyArray     :[Appointment] = []
        
        for appointment in appointmentArray {
            var convertedDate = self.convertDateFromString(appointment.date, withTime: true) as NSDate!
            if self.isDateToday(convertedDate) {
                todayArray.append(appointment as Appointment)
            } else if self.isDatePassed(convertedDate) {
                historyArray.append(appointment as Appointment)
            } else {
                upcommingArray.append(appointment as Appointment)
            }
        }
        
        if todayArray.count > 0 {
            var dict = NSMutableDictionary()
            dict.setObject(TODAY, forKey: self.keyTitle)
            dict.setObject(self.sortAppointmenByDate(todayArray, isAscending:false), forKey: self.keyAppointment)
            dict.setObject(NSNumber(bool: false), forKey: self.keyIsEmpty)
            dict.setObject("", forKey: self.keyEmptyTitle)
            dict.setObject("", forKey: self.keyEmptySubtitle)
            mutableArray.addObject(dict)
        } else {
            var dict = NSMutableDictionary()
            dict.setObject(TODAY, forKey: self.keyTitle)
            dict.setObject(todayArray, forKey: self.keyAppointment)
            dict.setObject(NSNumber(bool: true), forKey: self.keyIsEmpty)
            dict.setObject("No Appointment Today.", forKey: self.keyEmptyTitle)
            dict.setObject("Appointment(s) for today can be found here.", forKey: self.keyEmptySubtitle)
            mutableArray.addObject(dict)
        }
        
        if upcommingArray.count != 0 {
            var dict = NSMutableDictionary()
            dict.setObject(UPCOMING, forKey: self.keyTitle)
            dict.setObject(self.sortAppointmenByDate(upcommingArray, isAscending: true), forKey: self.keyAppointment)
            dict.setObject(NSNumber(bool: false), forKey: self.keyIsEmpty)
            mutableArray.addObject(dict)
        }
        
        if historyArray.count > 0 {
            var dict = NSMutableDictionary()
            dict.setObject(PAST, forKey: self.keyTitle)
            dict.setObject(self.sortAppointmenByDate(historyArray, isAscending: true), forKey: self.keyAppointment)
            dict.setObject(NSNumber(bool: false), forKey: self.keyIsEmpty)
            dict.setObject("", forKey: self.keyEmptyTitle)
            dict.setObject("", forKey: self.keyEmptySubtitle)

            mutableArray.addObject(dict)
        } else {
            var dict = NSMutableDictionary()
            dict.setObject(PAST, forKey: self.keyTitle)
            dict.setObject(historyArray, forKey: self.keyAppointment)
            dict.setObject(NSNumber(bool: true), forKey: self.keyIsEmpty)
            dict.setObject("No Appointments.", forKey: self.keyEmptyTitle)
            dict.setObject("Past Appointment(s) can be found here.", forKey: self.keyEmptySubtitle)
            mutableArray.addObject(dict)
        }
        return mutableArray
    }
    
    //MARK: Objects
    func customizeTextField(textField: UITextField) -> UITextField {
        var border = CALayer()
        var width = CGFloat(2.0)
        border.borderColor = UIColor.lightGrayColor().CGColor
        border.frame = CGRectMake(0, textField.frame.height - width, textField.frame.width, textField.frame.height)
        
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        
        return textField
    }
    
    func getWidthFrameCenter(parentFrame: CGRect, childFrame: CGRect) -> CGRect {
        var frame = childFrame as CGRect
        var parentHalfWidth = parentFrame.size.width / 2
        var childHalfWidth = childFrame.size.width / 2
        frame.origin.x = parentHalfWidth - childHalfWidth
        return frame
    }
    
    //MARK: KSTokenView
    func deleteTokenFromArray(token: String, tokenArray: [String]) -> [String] {
        var newTokenArray: [String] = tokenArray
        
        for var i = 0 ; i < newTokenArray.count; i++ {
            if token == newTokenArray[i] {
                newTokenArray.removeAtIndex(i)
            }
        }
        
        return newTokenArray
    }
    
    func convertToStringSet(details: [String]) -> String {
        var string = "None"
        if details.count != 0 {
            string = details[0] as String
            for var i = 1 ; i < details.count; i++ {
                string = string + ", " + details[i]
            }
            return string
        }
        
        return string
    }
    
    //MARK: Helper
    func getInitialLetter(name: String!) -> String {
        if name != "" || name != nil {
            return (name as NSString).substringWithRange(NSMakeRange(0, 1))
        }
        return name
    }
    
    //Cache
    func setCache(object: NSDictionary, keyId: NSString) {
        EGOCache.globalCache().setObject(object, forKey: "class\(keyId)", withTimeoutInterval: NSTimeInterval(self.userAccount.cacheDay))
    }
    
    func removeCacheFor(keyId: NSString) {
        EGOCache.globalCache().removeCacheForKey("class\(keyId)")
    }
    
    func getCacheFor(keyId: NSString) -> NSDictionary {
        return EGOCache.globalCache().objectForKey("class\(keyId)") as NSDictionary
    }
    
    func hasCacheFor(keyId: NSString) -> Bool {
        return EGOCache.globalCache().hasCacheForKey("class\(keyId)")
    }
    
    func showHUD(show: Bool) {
        if show == true {
            self.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        } else {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
        }
    }
    
}

//
//  DrawViewController.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 5/13/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class DrawViewController: UIViewController, UIPopoverControllerDelegate, PIColorPickerControllerDelegate {


    var selectedColor  : UIColor!
    var popOver        : UIPopoverController!
    var imageToBeEdit  : UIImage!

    var base: BaseViewController = BaseViewController()
    
    @IBOutlet weak var piDrawerView: PIDrawerView!
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var colorPicker: UIButton!
    @IBOutlet weak var eraseButton: UIButton!
    @IBOutlet weak var pencilButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.photoView.image = imageToBeEdit
        self.setpIDrawer()
        self.setColorPicker()
        // Do any additional setup after loading the view.
    }
    
    func setColorPicker() {
        self.colorPicker.setTitleColor(self.selectedColor, forState: UIControlState.Normal)
        self.colorPicker.layer.borderWidth = 1
        self.colorPicker.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    func setpIDrawer() {
        self.piDrawerView.drawingMode = DrawingMode.Paint
        
        if EGOCache.globalCache().hasCacheForKey("drawColor") {
            self.selectedColor = EGOCache.globalCache().objectForKey("drawColor") as UIColor
            self.piDrawerView.selectedColor = self.selectedColor
        } else {
            self.selectedColor = UIColor.redColor()
            self.piDrawerView.selectedColor = self.selectedColor
        }
        
        if EGOCache.globalCache().hasCacheForKey("pencilPoint") {
            self.piDrawerView.pencilPoint = EGOCache.globalCache().objectForKey("pencilPoint") as Float
        } else {
            self.piDrawerView.pencilPoint = 2.0
        
            EGOCache.globalCache().setObject(2.0, forKey: "pencilPoint")
        }
        
    }
    
    @IBAction func showColorPickerView(sender: AnyObject) {
        let button = sender as UIButton
        var colorPickerVC :PIColorPickerController = PIColorPickerController(nibName: "PIColorPickerController", bundle: nil)
        colorPickerVC.delegate = self
        colorPickerVC.currentSelectedColor = self.selectedColor
        
        self.popOver = UIPopoverController(contentViewController: colorPickerVC)
        self.popOver.delegate = self
        self.popOver.setPopoverContentSize(CGSizeMake(320, 568), animated: true)
             self.popOver.presentPopoverFromRect(button.frame, inView: button.superview!, permittedArrowDirections: UIPopoverArrowDirection.Down, animated: true)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func clearDrawing(sender: AnyObject) {
        self.piDrawerView.viewImage = nil
        self.piDrawerView.setNeedsDisplay()
    }
    
    //MARK: PIColorPickerController
    func colorSelected(selectedColor: UIColor!) {
        self.popOver.dismissPopoverAnimated(true)
        self.selectedColor               = selectedColor
        self.colorPicker.setTitleColor(self.selectedColor, forState: UIControlState.Normal)
        self.piDrawerView.selectedColor  = selectedColor
        EGOCache.globalCache().setObject(selectedColor, forKey: "drawColor")
    }
    
    //MARK: Buttons
    
    @IBAction func close(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func pencilMode(sender: AnyObject) {
        self.isEraseSelected(false)
        self.piDrawerView.drawingMode = DrawingMode.Paint
    }
    
    @IBAction func eraseMode(sender: AnyObject) {
        self.isEraseSelected(true)
        self.piDrawerView.drawingMode = DrawingMode.Erase
    }
    
    
    func isEraseSelected(isSelected: Bool) {
        if isSelected == true {
            self.pencilButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
            self.eraseButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        } else {
            self.pencilButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            self.eraseButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
            
        }
    }
    @IBAction func saveImage(sender: AnyObject) {
        UIGraphicsBeginImageContext(self.view.frame.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext())
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        NSNotificationCenter.defaultCenter().postNotificationName(self.base.drawingListener
            , object:image)
        self.close(UIButton())
        
    }

    @IBAction func showPoint(sender: AnyObject) {
        let button = sender as UIButton
        var view = NSBundle.mainBundle().loadNibNamed("PointSlider", owner: self, options: nil)[0] as UIView
        
        var slider : UISlider = view.viewWithTag(1) as UISlider
        
        slider.maximumValue = 10
        slider.minimumValue = 1
        var label = view.viewWithTag(2) as UILabel
        slider.value = EGOCache.globalCache().objectForKey("pencilPoint") as Float
        label.text = String(format: "%.1f", slider.value)
        slider.addTarget(self, action: "slider:", forControlEvents: UIControlEvents.TouchDragInside)
        slider.addTarget(self, action: "slider:", forControlEvents: UIControlEvents.TouchDragOutside)
        slider.addTarget(self, action: "slider:", forControlEvents: UIControlEvents.TouchUpInside)
        slider.addTarget(self, action: "slider:", forControlEvents: UIControlEvents.TouchUpOutside)
        
        var viewCon = UIViewController()
        viewCon.view = view
        self.popOver = UIPopoverController(contentViewController: viewCon)
        self.popOver.delegate = self
        self.popOver.setPopoverContentSize(view.frame.size, animated: true)
        self.popOver.presentPopoverFromRect(button.frame, inView: button.superview!, permittedArrowDirections: UIPopoverArrowDirection.Down, animated: true)
    }
   
    
    //MARK: Slider
    func slider(slider: UISlider) {
        var view = slider.superview as UIView!
        var label = view.viewWithTag(2) as UILabel
        self.piDrawerView.pencilPoint = slider.value
        EGOCache.globalCache().setObject(slider.value, forKey: "pencilPoint")
        label.text = String(format: "%.1f", slider.value)
    }
    
}

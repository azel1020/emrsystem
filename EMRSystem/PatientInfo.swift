//
//  PatientInfo.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/23/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit
//TODO: Add saving of new fields allergies etc
class PatientInfo: NSObject {
    //MARK: PatientInfo Container Value
    var entryId             : String!
    var id                  : String!
    var dateofFirstVisit    : String!
    var firstName           : String!
    var middleName          : String!
    var lastName            : String!
    var dateOfBirth         : String!
    var age                 : Int!
    var gender              : String!
    var address             : String!
    var city                : String!
    var state               : String!
    var postalCode          : String!
    var contactNo           : String!
    var email               : String!
    var referringPhysician  : String!
    var attendingPhysician  : String!
    var allergies           : [String]!
    var familyHistory       : [String]!
    var others              : String!
    var photo               : String!
    
    //MARK: PatientInfo Custom Class Properties
    let p_dateofFirstVisit: String   = "dateOfFirstVisit"
    let p_firstName: String          = "firstName"
    let p_middleName: String         = "middleName"
    let p_lastName: String           = "lastName"
    let p_dateOfBirth: String        = "dateOfBirth"
    let p_age: String                = "age"
    let p_gender: String             = "gender"
    let p_address: String            = "address"
    let p_city: String               = "city"
    let p_state: String              = "state"
    let p_postalCode: String         = "postalCode"
    let p_contactNo: String          = "contactNo"
    let p_email: String              = "email"
    let p_referringPhysician: String = "referringPhysician"
    let p_attendingPhysician: String = "attendingPhysician"
    let p_allergies: String          = "allergies"
    let p_familyHistory: String      = "familyHistory"
    let p_others: String             = "others"
    let p_photo: String              = "photo"

    let PATIENT_INFO_CLASS           = "PatientInfo"
    let ENTRY_ID                     = "entryId"
    
    typealias SaveBlock = (Bool?, AnyObject?) -> Void
    typealias ResultBlock = ([AnyObject]?, AnyObject?) -> Void
    typealias RetrieveBlock = (AnyObject?, AnyObject?) -> Void
    
    //MARK: Fetching Patient Info
    func fetchPatientInfoWithEntryId(id: String, withBlock: RetrieveBlock) {
        var patientEntry : CatalyzeEntry = CatalyzeEntry(className: PATIENT_INFO_CLASS)
            patientEntry.entryId = id
        
        patientEntry.retrieveInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
            withBlock(self.convertPatientInfos(result as CatalyzeEntry), nil)
            }, failure: { (result : [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
            withBlock(nil, result)
        })
    }
    
    func fetchAllPatient(withBlock: ResultBlock) {
        var query : CatalyzeQuery = CatalyzeQuery(className: PATIENT_INFO_CLASS)
        query.pageNumber = 1
        query.pageSize = 100 //may vary

        query.retrieveAllEntriesInBackgroundWithSuccess({ (result: [AnyObject]!) -> Void in
            var patientArray : [PatientInfo] = self.convertPatientInfoArray(result as [CatalyzeEntry])
            AuthService.sharedInstance.allPatientsArray = patientArray
            withBlock(patientArray, nil)
            }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                println(status)
                println(error)
                withBlock(nil, result)
        })
    }
    
    func convertPatientInfoArray(result: [CatalyzeEntry]) -> [PatientInfo] {
        var patientInfos : Array = [PatientInfo]()
        
        for info in result {
            patientInfos.append(self.convertPatientInfos(info)) 
//            self.deletePatientWithEntryId(info.entryId)
        }
        
        return patientInfos
    }
    
    func convertPatientInfos(info: CatalyzeEntry) -> PatientInfo {
        var patientInfo: PatientInfo = PatientInfo()
            patientInfo.entryId            = info.entryId as String
            patientInfo.dateofFirstVisit   = info.content.objectForKey(p_dateofFirstVisit) as String
            patientInfo.firstName          = info.content.objectForKey(p_firstName) as String
            patientInfo.lastName           = info.content.objectForKey(p_lastName) as String
            patientInfo.middleName         = info.content.objectForKey(p_middleName) as String
            patientInfo.dateOfBirth        = info.content.objectForKey(p_dateOfBirth) as String
            patientInfo.age                = info.content.objectForKey(p_age) as Int
            patientInfo.gender             = info.content.objectForKey(p_gender) as String
            patientInfo.address            = info.content.objectForKey(p_address) as String
            patientInfo.city               = info.content.objectForKey(p_city) as String
            patientInfo.state              = info.content.objectForKey(p_state) as String
            patientInfo.postalCode         = info.content.objectForKey(p_postalCode) as String
            patientInfo.email              = info.content.objectForKey(p_email) as String
            patientInfo.contactNo          = info.content.objectForKey(p_contactNo) as String
            patientInfo.allergies          = info.content.objectForKey(p_allergies) as [String]
            patientInfo.familyHistory      = info.content.objectForKey(p_familyHistory) as [String]
            patientInfo.others             = info.content.objectForKey(p_others) as String
            patientInfo.photo              = info.content.objectForKey(p_photo) as String
        
            patientInfo.referringPhysician = info.content.objectForKey(p_referringPhysician) as String
            patientInfo.attendingPhysician = info.content.objectForKey(p_attendingPhysician) as String
        
        var base : BaseViewController = BaseViewController()
        
        base.setCache(self.setDictionary(patientInfo), keyId: patientInfo.entryId)
        
        return patientInfo
    }
    
    //MARK: Save Info
    func savePatientInfo(firstVisit: String!, fName: String!, mName: String!, lName: String!, dob: String!, age: Int!, gender: String!, address: String!, city: String, state: String, postalCode: String, contactno: String!, email: String!, refPhysician: String!, attPhysician: String!, allergies: [String], familyHistory: [String], others: String ,entryId: String? ,photo: String!,isFromEditView: Bool!,  withBlock: SaveBlock)-> Void {

        var patientInfoEntry : CatalyzeEntry = CatalyzeEntry(className: PATIENT_INFO_CLASS)
            patientInfoEntry.content.setObject(firstVisit, forKey: p_dateofFirstVisit)
            patientInfoEntry.content.setObject(fName, forKey: p_firstName)
            patientInfoEntry.content.setObject(mName, forKey: p_middleName)
            patientInfoEntry.content.setObject(lName, forKey: p_lastName)
            patientInfoEntry.content.setObject(dob, forKey: p_dateOfBirth)
            patientInfoEntry.content.setObject(age, forKey: p_age)
            patientInfoEntry.content.setObject(gender, forKey: p_gender)
            patientInfoEntry.content.setObject(address, forKey: p_address)
            patientInfoEntry.content.setObject(city, forKey: p_city)
            patientInfoEntry.content.setObject(state, forKey: p_state)
            patientInfoEntry.content.setObject(postalCode, forKey: p_postalCode)
            patientInfoEntry.content.setObject(contactno, forKey: p_contactNo)
            patientInfoEntry.content.setObject(email, forKey: p_email)
            patientInfoEntry.content.setObject(allergies, forKey: p_allergies)
            patientInfoEntry.content.setObject(familyHistory, forKey: p_familyHistory)
            patientInfoEntry.content.setObject(others, forKey: p_others)
        
            patientInfoEntry.content.setObject(refPhysician, forKey: p_referringPhysician)
            patientInfoEntry.content.setObject(attPhysician, forKey: p_attendingPhysician)
        
            patientInfoEntry.content.setObject(photo, forKey: p_photo)
        
        if isFromEditView != true {
            patientInfoEntry.createInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
                withBlock(true, nil)
                }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                    withBlock(false, result)
            })
        } else {
            patientInfoEntry.entryId = entryId
            patientInfoEntry.saveInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
                    withBlock(true, self.convertPatientInfos(result as CatalyzeEntry))
                }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                    withBlock(false, result)
            })
        }
    }
    
    //MARK: Delete Entry
    func deletePatientWithEntryId(entryID : String) {
        var patientEntry: CatalyzeEntry = CatalyzeEntry(className: PATIENT_INFO_CLASS)
        patientEntry.entryId = entryID
        
        patientEntry.deleteInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
            println("Patient Deleted")
            }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                println(error)
        })
    }
    
    // MARK: NSCoding
    
    func setDictionary(patientInfo: PatientInfo) -> NSDictionary {
        var dict = NSMutableDictionary()
        
        dict.setObject(patientInfo.entryId, forKey: "entryId")
        
        dict.setObject(patientInfo.dateofFirstVisit, forKey: p_dateofFirstVisit)
        dict.setObject(patientInfo.firstName, forKey: p_firstName)
        dict.setObject(patientInfo.middleName, forKey: p_middleName)
        dict.setObject(patientInfo.lastName, forKey: p_lastName)
        dict.setObject(patientInfo.dateOfBirth, forKey: p_dateOfBirth)
        dict.setObject(patientInfo.age, forKey: p_age)
        dict.setObject(patientInfo.gender, forKey: p_gender)
        dict.setObject(patientInfo.address, forKey: p_address)
        dict.setObject(patientInfo.city, forKey: p_city)
        dict.setObject(patientInfo.state, forKey: p_state)
        dict.setObject(patientInfo.postalCode, forKey: p_postalCode)
        dict.setObject(patientInfo.contactNo, forKey: p_contactNo)
        dict.setObject(patientInfo.email, forKey: p_email)
        dict.setObject(patientInfo.allergies, forKey: p_allergies)
        dict.setObject(patientInfo.familyHistory, forKey: p_familyHistory)
        dict.setObject(patientInfo.others, forKey: p_others)
        
        dict.setObject(patientInfo.referringPhysician, forKey: p_referringPhysician)
        dict.setObject(patientInfo.attendingPhysician, forKey: p_attendingPhysician)
        dict.setObject(patientInfo.photo, forKey: p_photo)
        
        return NSDictionary(dictionary: dict)
    }
    
    func getDictionary(dict:NSDictionary) -> PatientInfo {
        var patientInfo = PatientInfo()
    
        patientInfo.entryId = dict.objectForKey("entryId") as String
        
        patientInfo.dateofFirstVisit    = dict.objectForKey(p_dateofFirstVisit) as String
        patientInfo.firstName           = dict.objectForKey(p_firstName) as String
        patientInfo.middleName          = dict.objectForKey(p_middleName) as String
        patientInfo.lastName            = dict.objectForKey(p_lastName) as String
        patientInfo.dateOfBirth         = dict.objectForKey(p_dateOfBirth) as String
        patientInfo.age                 = dict.objectForKey(p_age) as Int
        patientInfo.gender              = dict.objectForKey(p_gender) as String
        patientInfo.address             = dict.objectForKey(p_address) as String
        patientInfo.city                = dict.objectForKey(p_city) as String
        patientInfo.state               = dict.objectForKey(p_state) as String
        patientInfo.postalCode          = dict.objectForKey(p_postalCode) as String
        patientInfo.contactNo           = dict.objectForKey(p_contactNo) as String
        patientInfo.email               = dict.objectForKey(p_email) as String
        patientInfo.allergies           = dict.objectForKey(p_allergies) as [String]
        patientInfo.familyHistory       = dict.objectForKey(p_familyHistory) as [String]
        patientInfo.others              = dict.objectForKey(p_others) as String
        
        patientInfo.referringPhysician  = dict.objectForKey(p_referringPhysician) as String
        patientInfo.attendingPhysician  = dict.objectForKey(p_attendingPhysician) as String
        patientInfo.photo               = dict.objectForKey(p_photo) as String
        
        return patientInfo
    }
    
    
}

//
//  NotesViewController.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 5/12/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class NotesViewController: UIViewController, UITextViewDelegate , UIAlertViewDelegate {

    var isAdd = true
    var patientInfo : PatientInfo!
    
    let notePlaceHolder = "Enter Notes here..."
    
    @IBOutlet weak var noteTextView: UITextView!
    
    var patientNote: PatientsNote = PatientsNote()
    var base: BaseViewController = BaseViewController()
    
    var selectedIndex: NSIndexPath!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noteTextView.delegate = self
        
        self.setNavigation()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setNavigation() {
        if isAdd == true {
            self.navigationItem.title = "Add note to \(patientInfo.firstName)"
            var saveButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: "save")
            self.navigationItem.rightBarButtonItem = saveButton
            self.setPlaceholder(true)
        } else {
            self.setPlaceholder(false)
            self.noteTextView.text = self.patientNote.note
            self.noteTextView.editable = false
            self.navigationItem.title = "Dr. \(self.patientNote.lastName) note to \(patientInfo.firstName)"
            var deleteButton = UIBarButtonItem(title: "Delete", style: UIBarButtonItemStyle.Plain, target: self, action: "delete")
            self.navigationItem.rightBarButtonItem = deleteButton
                   }
    }
    
    func delete() {
        let alert = UIAlertView()
        alert.title = "EMR"
        alert.message = "Are you sure you want to delete Dr. \(self.patientNote.lastName) note to \(patientInfo.firstName)?"
        alert.addButtonWithTitle("No")
        alert.addButtonWithTitle("Yes")
        alert.delegate = self
        alert.show()

    }
    
    func setHUD() {
        self.base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.base.HUD.labelText = "Saving. Please Wait..."
    }

    func save() {
        self.view.endEditing(true)
        if self.isFormIsValid() == false {
            return
        }
        self.base.HUD.show(true)
        self.setHUD()
        self.patientNote.save(note: self.noteTextView.text, patientID: self.patientInfo.entryId, doctorID: CatalyzeUser.currentUser().usersId,fName: CatalyzeUser.currentUser().name.firstName, lName:CatalyzeUser.currentUser().name.lastName) { (result:AnyObject?, error:AnyObject?) -> Void in
            if error == nil {
                self.base.showAlert("Note successfully added!")
                NSNotificationCenter.defaultCenter().postNotificationName(self.base.patientNoteListener, object: nil)
                self.navigationController?.popViewControllerAnimated(true)
            } else {
                self.base.HUD.hide(true)
                self.base.showAlert("Oops! Something went wrong. Please try again.")
            }
        }

    }
    
    func isFormIsValid() -> Bool {
        if self.noteTextView.text == "" || self.isPlaceHolder() == true {
             self.base.showAlert("Please put a note for patient!")
            return false
        }
        return true
    }
    
    //MARK: Textview
    func setPlaceholder(isSet: Bool) {
        if isSet == true {
            self.noteTextView.text = self.notePlaceHolder
            self.noteTextView.textColor = UIColor.lightGrayColor()
        } else {
            self.noteTextView.text = ""
            self.noteTextView.textColor = UIColor.blackColor()
        }
    }
    
    func isPlaceHolder() -> Bool {
        if self.noteTextView.text == self.notePlaceHolder && self.noteTextView.textColor == UIColor.lightGrayColor() {
            return true
        }
        return false
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if isPlaceHolder() == true {
            self.setPlaceholder(false)
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if self.noteTextView.text == "" {
            self.setPlaceholder(true)
        }
    }
    
    //MARK: AlertView
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            self.patientNote.delete(entryID: self.patientNote.entryId, inBlock: { (object:AnyObject?, error:AnyObject?) -> Void in
                if error == nil {
                    self.base.showAlert("Note Successfully deleted!")
                    NSNotificationCenter.defaultCenter().postNotificationName(self.base.patientNoteListener, object: nil)
                    self.navigationController?.popViewControllerAnimated(true)
                } else {
                    self.base.showAlert("Oops! Something went wrong. Please try again.")
                }
            })
            self
        }
    }

}

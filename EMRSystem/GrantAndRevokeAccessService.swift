//
//  GrantAndRevokeAccessService.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 5/21/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class GrantAndRevokeAccessService: NSObject {
    
    enum appointmentQuery {
        case match
        case past
        case future
        case pastAndMatch
        case futureAndMatch
        var description : String {
            switch self {
                // Use Internationalization, as appropriate.
                case match: return "="
                case past: return "<"
                case future: return ">"
                case pastAndMatch: return "<="
                case futureAndMatch: return ">="
            }
        }
    }
    
    let api           = "https://api.catalyze.io"
    
    //KEYS
    let CONTENTTYPE   = "Content-Type"
    let ACCEPT        = "Accept"
    let AUTHORIZATION = "Authorization"
    let XAPIKEY       = "X-Api-Key"
    let AUTH_KEY      = "_catalyze_authorization"
    
    //VALUES
    let APPLICATION   = "application/json"
    let BEARER        = "Bearer"
    let BROWSER       = "browser DynamicObjx.EMRSystem 6da64e8d-3797-4782-88ea-a2eaa76e0a73"
    
    let appointment: Appointment = Appointment()
    
    typealias SuccessBlock = (Bool?, AnyObject?) -> Void
    typealias ResultBlock = (AnyObject?, AnyObject?) -> Void
    
    let activityIndicator : NetworkActivityIndicator = NetworkActivityIndicator()
    
    func httpClient() -> AFHTTPRequestOperationManager {
        var authKey = NSUserDefaults.standardUserDefaults().valueForKey(AUTH_KEY) as String
        
        var baseUrl : NSURL = NSURL(string: api)!
        var httpClient : AFHTTPRequestOperationManager = AFHTTPRequestOperationManager(baseURL: baseUrl)
        let clientSerializer: AFJSONRequestSerializer  = AFJSONRequestSerializer()
        
        httpClient.requestSerializer = clientSerializer
        httpClient.requestSerializer.setValue(APPLICATION, forHTTPHeaderField: CONTENTTYPE)
        httpClient.requestSerializer.setValue(APPLICATION, forHTTPHeaderField: ACCEPT)
        httpClient.requestSerializer.setValue(BEARER + " \(authKey)", forHTTPHeaderField: AUTHORIZATION)
        httpClient.requestSerializer.setValue(BROWSER, forHTTPHeaderField: XAPIKEY)
        
        let responseSerializer: AFJSONResponseSerializer = AFJSONResponseSerializer()
        httpClient.responseSerializer = responseSerializer
        return httpClient
    }
    
    func getAppointmentOf(#dateCode:Int, query:appointmentQuery, withBlock block: ResultBlock) {
    
        self.activityIndicator.show(true)
        var httpClient : AFHTTPRequestOperationManager = self.httpClient()
        var url = "/v2/classes/Appointment/query"
        
        var dict = ["\(query.description)" : ["dateCode", dateCode]]
        
        httpClient.POST(url, parameters: dict, success: { (operation: AFHTTPRequestOperation!, id: AnyObject!) -> Void in
            self.activityIndicator.show(false)
            var response: AnyObject? = NSJSONSerialization.JSONObjectWithData(operation.responseData, options:nil, error: nil)
            var dictArray = response as [NSDictionary]
            block(self.appointment.convertDictionaryToAppointmentArray(dictArray), nil)
//            block(self.appointment.convertDictionaryToAppointments(dictArray), nil)
            }) { (operation: AFHTTPRequestOperation!, error: NSError!) -> Void in
                self.activityIndicator.show(false)

                println("error: \(error)")
                block(nil, error)
        }
    }
    
    func postGrantUser(#userId: String, customClass: String, withBlock: SuccessBlock) {
        var authKey = NSUserDefaults.standardUserDefaults().valueForKey(AUTH_KEY) as String
        
        println("AUTHKEY !!!: \(authKey)")
        
        var baseUrl : NSURL = NSURL(string: api)!
        var httpClient : AFHTTPRequestOperationManager = AFHTTPRequestOperationManager(baseURL: baseUrl)
        let clientSerializer: AFJSONRequestSerializer  = AFJSONRequestSerializer()
            httpClient.requestSerializer = clientSerializer
        
            httpClient.requestSerializer.setValue(APPLICATION, forHTTPHeaderField: CONTENTTYPE)
            httpClient.requestSerializer.setValue(APPLICATION, forHTTPHeaderField: ACCEPT)
            httpClient.requestSerializer.setValue(BEARER + " \(authKey)", forHTTPHeaderField: AUTHORIZATION)
            httpClient.requestSerializer.setValue(BROWSER, forHTTPHeaderField: XAPIKEY)
        
        println(BEARER + " \(authKey)")
        let responseSerializer: AFJSONResponseSerializer = AFJSONResponseSerializer()
            httpClient.responseSerializer = responseSerializer
        
        var url = "/v2/acl/custom/\(customClass)/\(userId)"
        var body = ["retrieve", "update@"]
        
        
//        var body = NSDictionary()
//        body.valueForKey("retrieve")
//        body.valueForKey("update")
//        body.set
        
        println(JSONStringify(body, prettyPrinted: true))
        
        println("HTTPCLIENT: \(httpClient)")
        
        httpClient.POST(url, parameters: body, success: { (operation: AFHTTPRequestOperation!, id: AnyObject!) -> Void in
            println("Access granted !!!")
            withBlock(true, nil)
            }) { (operation: AFHTTPRequestOperation!, error: NSError!) -> Void in
            withBlock(false, error)
            println("err: \(error)")
            println("status: \(operation.response.statusCode)")
        }
    }
    
    func JSONStringify(value: AnyObject, prettyPrinted: Bool = false) -> String {
        var options = prettyPrinted ? NSJSONWritingOptions.PrettyPrinted : nil
        if NSJSONSerialization.isValidJSONObject(value) {
            if let data = NSJSONSerialization.dataWithJSONObject(value, options: options, error: nil) {
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding) {
                    return string
                }
            }
        }
        return ""
    }
    
//    NSURL *baseUrl = [NSURL URLWithString:@"https://api.catalyze.io"];
//    AFHTTPRequestOperationManager *httpClient = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseUrl];
//    httpClient.requestSerializer = [AFJSONRequestSerializer serializer];
//    
//    [httpClient.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [httpClient.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [httpClient.requestSerializer setValue:@"Bearer 0c7f26c8-5b4a-4a32-b35a-2e249448bbf2" forHTTPHeaderField:@"Authorization"];
//    [httpClient.requestSerializer setValue:@"browser api.catalyze.io 525ad5d6993247cccb083e5a" forHTTPHeaderField:@"X-Api-Key"];
//    
//    httpClient.responseSerializer = [AFHTTPResponseSerializer serializer];
//    
//    NSString *url = [NSString stringWithFormat:@"/v2%@",@"/acl/custom/{customClass}/{entityId}"];
//    
//    NSDictionary *body = @[@"retrieve", @"update@"]
//    ;
//    
//    [httpClient POST:url parameters:body success:^(AFHTTPRequestOperation *operation, id responseObject) {
//    NSLog(@"Success Response: %@", [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:nil]);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//    NSLog(@"Error: %@", error);
//    NSLog(@"Status: %ld", [[operation response] statusCode]);
//    NSLog(@"Response: %@", [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:nil]);
//    }];
   
}

//
//  NetworkActivityIndicator.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 4/6/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class NetworkActivityIndicator: NSObject {
    func show(isShowing:Bool) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = isShowing
    }
}

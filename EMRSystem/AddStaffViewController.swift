//
//  AddStaffViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 5/20/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class AddStaffViewController: UITableViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField:  UITextField!
    @IBOutlet weak var emailTextField:     UITextField!
    @IBOutlet weak var usernameTextField:  UITextField!
    @IBOutlet weak var passwordTextField:  UITextField!
    
    var base : BaseViewController = BaseViewController()
    var isPasswordValid = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "Add Staff"
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done,
                                                                              target: self,
                                                                              action: "submitAssistantForm")
    }
    
    func submitAssistantForm() {
        self.tableView.endEditing(true)
        if base.isConnectedToNetwork() {
            if isFormValid() {
                if base.isValidEmail(emailTextField.text) {
                    var email : Email  = Email()
                        email.primary  = emailTextField.text
                    
                    var name : Name    = Name()
                        name.firstName = firstNameTextField.text
                        name.lastName  = lastNameTextField.text
                    
                    self.base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                    base.authService.signUpUser(usernameTextField.text, email: email, name: name, password: passwordTextField.text, doctorID: CatalyzeUser.currentUser().usersId, isAdminStatus: false, withBlock: { (isSuccess: Bool?, error: AnyObject?) -> Void in
                        self.base.HUD.hide(true)
                        if isSuccess != false {
                            self.base.showAlert("Request Sent! Please activate your account on your email")
                            self.navigationController?.popViewControllerAnimated(true)
                        } else {
                            self.base.showAlertError(error!)
                        }
                    })
                } else {
                    self.base.showAlert("Invalid Email")
                }
            } else {
                self.base.showAlert("Please fill missing field/s")
            }
        } else {
            self.base.showAlert(base.noNetworkConnectionMessage)
        }
    }
    
    func isFormValid() -> Bool {
        if firstNameTextField.text == "" || lastNameTextField.text == "" ||
               emailTextField.text == "" || usernameTextField.text == "" ||
               passwordTextField.text == "" {
            return false
        }
    
        return true
    }

    //MARK: Others
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//
//  PhotoCloseButton.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 4/7/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit
import QuartzCore

class PhotoCloseButton: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    var index: NSIndexPath!
    
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.height/2
    }

}

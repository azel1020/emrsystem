//
//  PatientsNote.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 5/11/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class PatientsNote: NSObject {

    var entryId   : String!
    var patientId : String!
    var doctorsId : String!
    var note      : String!
    var firstName : String!
    var lastName  : String!
    
    let PATIENTSNOTE = "PatientsNote"

    let a_patientId = "patientId"
    let a_doctorsId = "doctorsId"
    let a_firstName = "firstName"
    let a_lastName = "lastName"
    let a_note = "note"
    
    let networkActivity: NetworkActivityIndicator = NetworkActivityIndicator()

    typealias RetrieveBlock = (AnyObject?, AnyObject?) -> Void

    func getNotesOf(#id: NSString, inBlock block:RetrieveBlock) {
        var query: CatalyzeQuery = CatalyzeQuery(className: PATIENTSNOTE)
        query.queryField = a_patientId
        query.queryValue = id
        query.pageNumber = 1
        query.pageSize = 100
        query.retrieveAllEntriesInBackgroundWithSuccess({ (result:[AnyObject]!) -> Void in
                block(self.convertPatientsNoteArray(result as [CatalyzeEntry]), nil)
            }, failure: { (result:[NSObject : AnyObject]!, status:Int32, error:NSError!) -> Void in
                block(nil, error)
        })
    }
    
    func convertPatientsNoteArray(results: [CatalyzeEntry]) -> [PatientsNote] {
        var patientsNotes = [PatientsNote]()
        for patientNote in results {
//            self.delete(entryID: patientNote.entryId, inBlock: { (object:AnyObject?, error:AnyObject?) -> Void in
//            })
            patientsNotes.append(self.convertPatientsNote(patientNote))
        }
        return patientsNotes
    }
    
    func convertPatientsNote(info:CatalyzeEntry) -> PatientsNote {
        var patientNote: PatientsNote = PatientsNote()
            patientNote.entryId       = info.entryId as String
            patientNote.doctorsId     = info.content.objectForKey(a_doctorsId) as String
            patientNote.patientId     = info.content.objectForKey(a_patientId) as String
            patientNote.note          = info.content.objectForKey(a_note) as String
            patientNote.firstName     = info.content.objectForKey(a_firstName) as String
            patientNote.lastName      = info.content.objectForKey(a_lastName) as String
        return patientNote
    }
    
    func delete(#entryID: NSString, inBlock block:RetrieveBlock) {
        var appointmentEntry: CatalyzeEntry = CatalyzeEntry(className: PATIENTSNOTE)
        appointmentEntry.entryId = entryID
        appointmentEntry.deleteInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
            println("Appointment Deleted")
            block(true, nil)
            }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                println(error)
                block(false, error)
        })
        
    }
    
    func save(#note: String, patientID: String, doctorID: String, fName: String, lName: String, inBlock: RetrieveBlock) {
        self.networkActivity.show(true)
        var appointmentEntry : CatalyzeEntry = CatalyzeEntry(className: PATIENTSNOTE)
            appointmentEntry.content.setObject(note, forKey: a_note)
            appointmentEntry.content.setObject(patientID, forKey: a_patientId)
            appointmentEntry.content.setObject(doctorID, forKey: a_doctorsId)
            appointmentEntry.content.setObject(fName, forKey: a_firstName)
            appointmentEntry.content.setObject(lName, forKey: a_lastName)
        appointmentEntry.createInBackgroundWithSuccess({ (object:AnyObject!) -> Void in
                self.networkActivity.show(false)
                inBlock(object, nil)
            }, failure: { (result:[NSObject : AnyObject]!, status:Int32, error:NSError!) -> Void in
                self.networkActivity.show(false)
                inBlock(nil, error)
        })
        
        
    }
}

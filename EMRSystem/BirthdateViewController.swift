//
//  BirthdateViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/24/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class BirthdateViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

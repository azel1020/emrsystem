//
//  AddAppointmentViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/30/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class AddAppointmentViewController: UITableViewController, RMDateSelectionViewControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, KSTokenViewDelegate {
    @IBOutlet weak var viewMoreDetailsCell: UITableViewCell!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var complaintTextField: UITextView!
    @IBOutlet weak var diagnosisTextView: UITextView!
    @IBOutlet weak var treatmentTextView: UITextView!
    @IBOutlet weak var diagnosticsTextView: UITextView!
    @IBOutlet var textViewArray: [UITextView]!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var locationTokenView: KSTokenView!

    var photoArray: NSMutableArray = []
    var photoToBeDeletedArray: NSMutableArray = []
   
    var info : PatientInfo!
    let base : BaseViewController   = BaseViewController()
    
    var selectedIndex = NSIndexPath(forRow: 0, inSection: 0)
    var locationValues: Array<String> = List.sharedInstance.locationList
    var locationArray : [String] = []
    var selectedLocation = ""
    
    //Photos
    let maxPhoto = 1
    var picker:UIImagePickerController?=UIImagePickerController()
    let pathForPhoto = NSIndexPath(forRow: 6, inSection: 0)
    let cameraIndex = 1
    let galleryIndex = 2
    var photoBrowser = IDMPhotoBrowser()
    
    //for viewing appointment
    var appointmentDetails : Appointment?
    var isViewAppointment: Bool = false
    var userCanEdit : Bool = true
    var isFromAppointmentViewController = false
    
    //MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.setHUD()
        
        nameLabel.text = "\(info.lastName), \(info.firstName) \(info.middleName)     \(info.age) \(info.gender)"
        
        if isViewAppointment {
            self.setAppointmentDetails()
            if appointmentDetails?.status == true {
                self.viewMoreDetailsCell.hidden = false
            } else {
                self.viewMoreDetailsCell.hidden = true
            }
        } else {
            self.showHUD("", show: false)
            self.viewMoreDetailsCell.hidden = true
        }
        
        //customize TextViews & date button
        dateButton.layer.borderColor  = UIColor.lightGrayColor().CGColor
        dateButton.layer.borderWidth  = 1.0
        dateButton.layer.cornerRadius = 5.0
        
        locationTokenView.delegate    = self
        locationTokenView.promptText  = ""
        locationTokenView.style       = .Squared
        locationTokenView.font        = UIFont(name: "STHeitiTC-Medium", size: 14)!
        locationTokenView.shouldDisplayAlreadyTokenized = false
        locationTokenView.maxTokenLimit = 1
        
        locationTokenView.layer.borderColor  = UIColor.lightGrayColor().CGColor
        locationTokenView.layer.borderWidth  = 1.0
        locationTokenView.layer.cornerRadius = 5.0
        locationTokenView.contentMode        = UIViewContentMode.TopLeft
    
        for field in self.textViewArray {
            var textView = field as UITextView
                textView.layer.borderColor = UIColor.lightGrayColor().CGColor
                textView.layer.borderWidth = 1.0
                textView.layer.cornerRadius = 5.0
                textView.editable = self.userCanEdit
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateArray:", name: self.base.drawingListener, object: nil)
    }
    
    func updateArray(noti:NSNotification) {
        var imageDict = self.photoArray.objectAtIndex(selectedIndex.row) as NSMutableDictionary
        imageDict.setValue(noti.object as UIImage, forKey: "image")
        self.photoCollectionView.reloadData()
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        base.HUD.mode = MBProgressHUDMode.AnnularDeterminate
    }
    
    func setUpNavigationBar() {
        var saveButton : UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: "saveAppointment")
        self.navigationItem.rightBarButtonItem = saveButton
        if isViewAppointment {
            self.navigationItem.title = "Appointment"
        } else {
            self.navigationItem.title = "Add Appointment"
        }
    }
    
    func setAppointmentDetails() {
        self.dateButton.setTitle(self.appointmentDetails!.date, forState: UIControlState.Normal)
        self.complaintTextField.text       = self.appointmentDetails!.complaint
        self.diagnosisTextView.text        = self.appointmentDetails!.diagnosis
        self.treatmentTextView.text        = self.appointmentDetails!.treatment
        self.diagnosticsTextView.text      = self.appointmentDetails!.diagnostics
        self.locationTokenView.placeholder = self.appointmentDetails!.location
        
        if self.appointmentDetails?.photo.count > 0 {
            var id = self.appointmentDetails?.photo.objectAtIndex(0) as NSString
            if id.isEqualToString(self.base.appointments.emptyFileId) {
                self.showHUD("", show: false)
            } else {
                self.retrievePhoto()
            }
        } else {
            self.showHUD("", show: false)
        }
    }
    
    func retrievePhoto() {
        if base.isConnectedToNetwork() {
            var id = self.appointmentDetails!.photo.objectAtIndex(0) as NSString
            self.base.appointments.retrievePhoto(id, inBlock: { (succeeded :Bool?, object:AnyObject?) -> Void in
                if succeeded == true {
                    var imageDict = NSMutableDictionary()
                    imageDict.setObject(id, forKey: "fileId")
                    imageDict.setObject(object as UIImage, forKey: "image")
                    self.photoArray.addObject(imageDict)
                    self.showHUD("", show: false)
                    self.photoCollectionView.reloadData()
                } else {
                    self.base.showAlertError(object)
                }
            })
        } else {
            self.base.showAlert(base.noNetworkConnectionMessage)
        }
    }
    
    //MARK: MBProgressHUD
    func setHUD() {
        base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.showHUD("Retrieving Photo...", show: true)
    }
    
    func showHUD(title: NSString, show: Bool) {
        base.HUD.labelText = title
        if show == true {
            base.HUD.show(true)
        } else {
            base.HUD.hide(true)
        }
        self.tableView.scrollEnabled = !show
    }
    
    //MARK: Date Selection
    @IBAction func selectAppointmentDate(sender: AnyObject) {
        var dateSelectionViewController : RMDateSelectionViewController = RMDateSelectionViewController()
            dateSelectionViewController.hideNowButton = true
            dateSelectionViewController.delegate = self
            dateSelectionViewController.datePicker.minimumDate = NSDate()
            dateSelectionViewController.show()
    }
    
    func dateSelectionViewController(vc: RMDateSelectionViewController!, didSelectDate aDate: NSDate!) {
        self.dateButton.setTitle(base.formatDate(aDate, withTime: true), forState: UIControlState.Normal)
    }
    
    //MARK: Appointment 
    func saveAppointment() {
        if !self.checkDuplicateDate(){
            if !isFormValid() {
                return
            }
            
            base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            if self.photoToBeDeletedArray.count > 0 {
                self.base.HUD.labelText = "Deleting Image..."
                let imageDict = self.photoToBeDeletedArray.objectAtIndex(0) as NSDictionary
                let id = imageDict.objectForKey("fileId") as NSString
                
                self.base.appointments.deletePhoto(id, inBlock: { (succeed:Bool?, object:AnyObject?) -> Void in
                    if succeed == true {
                        self.checkDetails()
                    } else {
                        self.base.showAlert("Error: Deleting Photo Fail. Please Try again.")
                    }
                })
            } else {
                self.checkDetails()
            }
        } else {
            self.base.showAlert("Appointment date conflicts with other appointment")
        }
    }
    
    func checkDuplicateDate() -> Bool {
        var appointmentDate  = self.dateButton.titleLabel?.text
        var appointmentArray = AuthService.sharedInstance.allAppointmentArray
        var isDuplicate      = false
        
        if appointmentDate != appointmentDetails?.date {
            for a in appointmentArray {
                var appointmentD = a as Appointment
                
                if appointmentDate == appointmentD.date {
                    isDuplicate = true
                    break
                }
            }
        }
 
        return isDuplicate
    }
    
    func checkDetails() {
        if self.photoArray.count > 0 {
            self.saveAppointmentPhoto()
        } else {
            self.saveAppointmentDetail(self.base.appointments.emptyFileId)
        }
    }
    
    func saveAppointmentPhoto () {
        if base.isConnectedToNetwork() {
            base.HUD.labelText = "Uploading Image. Please Wait..."
            let imageDict = self.photoArray.objectAtIndex(0) as NSDictionary
            let image = imageDict.objectForKey("image") as UIImage
            let data = UIImageJPEGRepresentation(image, 0.5) as NSData
            base.appointments.savePhoto(data, inBlock: { (succeeded :Bool?, object:AnyObject?) -> Void in
                self.base.HUD.hide(true)
                if succeeded == true {
                    let fileId = object as NSDictionary
                    self.saveAppointmentDetail(fileId.objectForKey(self.base.appointments.filesId)! as NSString)
                } else {
                    self.base.showAlertError(object)
                }
            })
        } else {
            self.base.showAlert(base.noNetworkConnectionMessage)
        }
    }
    
    func saveAppointmentDetail (fileId : NSString) {
        if base.isConnectedToNetwork() {
            base.HUD.labelText = "Saving..."
            var appointmentDate = self.dateButton.titleLabel?.text
            var id = isViewAppointment ? appointmentDetails?.entryId : ""
            var dict = self.base.getDateDictionary(self.base.convertDateFromString(appointmentDate!, withTime: true)!)
            
            base.appointments.saveAppointment(info, date: appointmentDate!,location: selectedLocation , treatment: self.treatmentTextView.text, complaint: self.complaintTextField.text, diagnosis: diagnosisTextView.text, diagnostics: self.diagnosticsTextView.text , fileId: fileId, entryID: id ,isFromEditForm: isViewAppointment, dict: dict) { (isSuccess: Bool?, error: AnyObject?) -> Void in
                self.base.HUD.hide(true)
                
                if isSuccess != false {
                    if self.isViewAppointment {
                        self.base.showAlert("Appointment Updated!")
                    } else {
                        self.base.showAlert("Appointment Saved!")
                    }

                    NSNotificationCenter.defaultCenter().postNotificationName(self.base.fetchAppointmentListener, object: nil)
                    if self.isFromAppointmentViewController == true {
                        self.navigationController?.popToRootViewControllerAnimated(true)
                    } else {
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                } else {
                    self.base.showAlertError(error!)
                }
            }
        } else {
            self.base.showAlert(base.noNetworkConnectionMessage)
        }
    }
    
    //MARK: KSTokenView Delegate
    func tokenView(token: KSTokenView, performSearchWithString string: String, completion: ((results: Array<AnyObject>) -> Void)?) {
        var data: Array<String> = []
        for value: String in locationValues {
            if value.lowercaseString.rangeOfString(string.lowercaseString) != nil {
                data.append(value)
            }
        }
        completion!(results: data)
    }
    
    func tokenView(tokenView: KSTokenView, didAddToken token: KSToken) {
        selectedLocation = token.title
        println("VV: \(selectedLocation)")
    }
    
    func tokenView(tokenView: KSTokenView, didDeleteToken token: KSToken) {
        locationArray = base.deleteTokenFromArray(token.title, tokenArray: locationArray)
    }
    
    func tokenView(token: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        return object as String
    }
    
    func tokenView(tokenView: KSTokenView, shouldAddToken token: KSToken) -> Bool {
        let list: List = List()
        list.addToObjectList(token.title, type: ListType.Location)
        locationValues = List.sharedInstance.locationList
        
        println("V: \(locationValues)")
        return true
    }
    
    //MARK: Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.row == 7 {
            var madvc = self.storyboard?.instantiateViewControllerWithIdentifier("MoreAppointmentDetailsViewController") as MoreAppointmentDetailsViewController
            madvc.isViewMoreDetails = true
            madvc.info = info
            madvc.appointmentDetails = appointmentDetails
            self.navigationController?.pushViewController(madvc, animated: true)
        }
    }
    
    //MARK: Form Validation
    func isFormValid() -> Bool {
        self.view.endEditing(true)
        var appointmentDate = self.dateButton.titleLabel?.text
        var defaultText = "Select Date"
        if appointmentDate == defaultText {
            self.base.showAlert("Please select Appointment date")
            return false
        } else if self.complaintTextField.text == "" {
            self.base.showAlert("Please fill up complaint.")
            return false
        }
        return true
    }
    
    func isAppointmentDateTimeValid() -> Bool {
    
        return true
    }
    
    //MARK: Action sheet
    func showActionSheet() {
        if self.photoArray.count < self.maxPhoto {
            let actionSheet = UIActionSheet(title: "Choose", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera","Gallery")
            actionSheet.showInView(self.view)
        } else {
            var alertMessage = "Maximum of \(self.maxPhoto) photo only."
            if self.maxPhoto > 1 {
                alertMessage = "Maximum of \(self.maxPhoto) photos only."
            }
            
            self.base.showAlert(alertMessage)
        }
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if (buttonIndex == cameraIndex) { //show Camera Image Picker
            self.showCamera(.ipadCamera)
        } else if (buttonIndex == galleryIndex) { //show Gallery Image Picker
            self.showCamera(.ipadCameraRoll)
        }
    }
    
    //MARK: Camera
    func showCamera(camera : CameraSettings) {
        self.picker?.delegate = self
        self.picker?.allowsEditing = true
        picker = UIImagePickerController()
        picker?.delegate = self
        switch (camera) {
        case .ipadCamera :
            if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
                //Camera Available
                self.picker!.sourceType = UIImagePickerControllerSourceType.Camera
                self.showOperatedImagePicker()
            } else {
                //No Camera Available, open gallery instead
                self.picker!.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
                self.showOperatedImagePicker()
            }
            break
        case .ipadCameraRoll :
            self.picker!.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
            self.showOperatedImagePicker()
            break
        default :
            break
        }
    }
    
    func showOperatedImagePicker () {
        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
            self.showPicker()
        })
    }
    
    func showPicker () {
        if picker?.sourceType == UIImagePickerControllerSourceType.SavedPhotosAlbum {
            picker?.modalPresentationStyle = UIModalPresentationStyle.FormSheet
            self.base.setLandscapeLeftOrientation()
        } else {
            self.base.setCameraOrientation()
        }
        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
            self.picker?.allowsEditing = true
            self.presentViewController(self.picker!, animated: true, completion: { () -> Void in
            })
        })
    }
    
    //MARK: UIImage Picker Controller
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        var chosenImage = info[UIImagePickerControllerEditedImage] as UIImage
        var imageDict = NSMutableDictionary()
        imageDict.setObject(self.base.appointments.emptyFileId, forKey: "fileId")
        imageDict.setObject(chosenImage, forKey: "image")
        self.photoArray.addObject(imageDict)
        self.photoCollectionView.reloadData()
        self.dismissPicker()
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissPicker()
    }
    
    func dismissPicker () {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.base.setLandscapeLeftOrientation()
            self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 4, inSection: 0), atScrollPosition: UITableViewScrollPosition.Top, animated: true)
        })
    }
    
    //MARK: Collection View
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photoArray.count+1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var addCell = self.photoArray.count
        
        var cellIndetifier = "PhotoCell"
        if indexPath.row == addCell {
            cellIndetifier = "AddCell"
        }
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIndetifier, forIndexPath: indexPath) as UICollectionViewCell
        
        if indexPath.row != addCell {
            let imageView = cell.viewWithTag(10) as UIImageView
            let imageDict = self.photoArray.objectAtIndex(indexPath.row) as NSDictionary
            let image = imageDict.objectForKey("image") as UIImage
            imageView.image = image

            let deleteButton = cell.viewWithTag(11) as PhotoCloseButton
            deleteButton.index = indexPath
            
            deleteButton.addTarget(self, action: "deletePhoto:", forControlEvents: UIControlEvents.TouchUpInside)
            deleteButton.hidden = !self.userCanEdit
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        var addCell = self.photoArray.count
        self.selectedIndex = indexPath
        
        if indexPath.row == addCell {
            self.showActionSheet()
        } else {
            let imageDict = self.photoArray.objectAtIndex(indexPath.row) as NSDictionary
            let image = imageDict.objectForKey("image") as UIImage
            
            var arrayPhoto = NSMutableArray()
            arrayPhoto.addObject(IDMPhoto(image: image))
            let cell = collectionView.cellForItemAtIndexPath(indexPath)
            
            let imageView = cell?.viewWithTag(10) as UIImageView
            
            if self.userCanEdit == true {
                
                var dnvc = self.storyboard?.instantiateViewControllerWithIdentifier("DrawViewNavigationController") as UINavigationController
                var dvc = dnvc.viewControllers[0] as DrawViewController
                dvc.imageToBeEdit = image
                self.presentViewController(dnvc, animated: true, completion: nil)
            } else {
                self.photoBrowser = IDMPhotoBrowser(photos: arrayPhoto)//,animatedFromView: imageView)
                self.photoBrowser.displayActionButton = false
                self.presentViewController(self.photoBrowser, animated: true, completion:nil)
            }
            
        }
    }
    
    func deletePhoto(button: PhotoCloseButton) {
        let imageDict = self.photoArray.objectAtIndex(button.index.row) as NSDictionary
        var id = imageDict.objectForKey("fileId") as NSString
        if !id.isEqualToString(self.base.appointments.emptyFileId) {
            self.photoToBeDeletedArray.addObject(imageDict)
        }
        self.photoArray.removeObjectAtIndex(button.index.row)
        self.photoCollectionView.reloadData()
    }

}

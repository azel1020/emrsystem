//
//  AppointmentViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/24/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class PatientAppointmentViewController: BaseViewController , UITableViewDelegate , UITableViewDataSource, UIAlertViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var base : BaseViewController = BaseViewController()
    var info : PatientInfo!
    var appointmentArray : NSMutableArray = []
    var timeLabel : UILabel!
    
    var selectedIndex: NSIndexPath!
    
    //MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.fetchAppointmentsByPatient()
        
        var nameLabel = tableView.viewWithTag(300) as UILabel
            nameLabel.text = "\(info.lastName), \(info.firstName) \(info.middleName)     \(info.age)  \(info.gender)"
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "fetchAppointmentsByPatient", name: self.base.fetchAppointmentListener, object: nil)
    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "\(info.firstName)'s Appointments"
        var addButton : UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "gotoAddAppointment")
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    func gotoAddAppointment() {
        var aavc : AddAppointmentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddAppointmentViewController") as AddAppointmentViewController
            aavc.info = info
            aavc.isViewAppointment = false
            aavc.userCanEdit = true
        self.navigationController?.pushViewController(aavc, animated: true)
    }
    
    //MARK: Appointment
    func fetchAppointmentsByPatient() {
        self.HUD.show(true)
        self.HUD.labelText = "Retrieving Appointments..."
        appointments.fetchAllAppointmentsWithPatientId(info.entryId, withBlock: { (result: [AnyObject]?, error: AnyObject?) -> Void in
            self.HUD.hide(true)
            if error == nil {
                self.appointmentArray.removeAllObjects()
                self.appointmentArray     = self.convertToDisplayGroupings(result as [Appointment])
                self.tableView.delegate   = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
            } else {
                self.showAlertError(error!)
            }
        })
    }
    
    //MARK: UITableView Delegate
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: MGSwipeTableCell!
        var dict = self.appointmentArray.objectAtIndex(indexPath.section) as NSMutableDictionary
        var title = dict.objectForKey("title") as NSString
        
        if title.isEqualToString(PAST) {
            cell = tableView.dequeueReusableCellWithIdentifier("HistoryCell", forIndexPath: indexPath) as MGSwipeTableCell
            var date                = cell.viewWithTag(500) as UILabel
            var historyView         = cell.viewWithTag(505) as UIView!
            var complaintTextView   = cell.viewWithTag(700) as SummaryTextView
            var diagnosticTextView  = cell.viewWithTag(701) as SummaryTextView
            var diagnosisTextView   = cell.viewWithTag(702) as SummaryTextView
            var treatmentTextView   = cell.viewWithTag(703) as SummaryTextView
            var noHistoryLabel      = cell.viewWithTag(504) as UILabel
            var timeLabel           = cell.viewWithTag(600) as UILabel

            var appointments = dict.objectForKey("appointment") as NSArray
            if appointments.count == 0 {
                historyView.hidden          = true
                noHistoryLabel.hidden       = false
                
            } else {
                historyView.hidden         = false
                noHistoryLabel.hidden      = true
                var appointment: Appointment = appointments.objectAtIndex(indexPath.row) as Appointment
                date.text           = self.getDateOnly(appointment.date)
                timeLabel.text      = self.getTime(appointment.date)
                
                complaintTextView.text  = appointment.complaint
                diagnosisTextView.text  = appointment.diagnosis
                diagnosticTextView.text = appointment.diagnostics
                treatmentTextView.text  = appointment.treatment
                
                complaintTextView.textColor = UIColor.darkGrayColor()
                diagnosisTextView.textColor = UIColor.darkGrayColor()
                diagnosticTextView.textColor = UIColor.darkGrayColor()
                treatmentTextView.textColor = UIColor.darkGrayColor()
            }
        } else if title.isEqualToString(TODAY) {
            cell = tableView.dequeueReusableCellWithIdentifier("TodayCell", forIndexPath: indexPath) as MGSwipeTableCell
            
            var noAppointmentLabel = cell.viewWithTag(400) as UILabel
            var complaintLabel     = cell.viewWithTag(401) as UILabel
            var complaintTextView  = cell.viewWithTag(402) as SummaryTextView
            var timeLabel          = cell.viewWithTag(403) as UILabel
            
            var appointments = dict.objectForKey("appointment") as NSArray
            if appointments.count == 0 {
                noAppointmentLabel.hidden = false
                complaintLabel.hidden     = true
                complaintTextView.hidden  = true
                timeLabel.hidden          = true
                cell.accessoryType = UITableViewCellAccessoryType.None
            } else {
                noAppointmentLabel.hidden = true
                complaintLabel.hidden     = false
                complaintTextView.hidden  = false
                timeLabel.hidden          = false
                var appointment: Appointment = appointments.objectAtIndex(indexPath.row) as Appointment
                timeLabel.text               = self.getTime(appointment.date)
                complaintTextView.text          = appointment.complaint
                complaintTextView.textColor  = UIColor.darkGrayColor()
                cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
                cell.rightButtons = [self.cellDeleteButton(indexPath, appointment: appointment) as MGSwipeButton]
                cell.rightSwipeSettings.transition = MGSwipeTransition.Border

            }
        } else if title.isEqualToString(UPCOMING) {
            cell = tableView.dequeueReusableCellWithIdentifier("UpcommingCell", forIndexPath: indexPath) as MGSwipeTableCell
            var date               = cell.viewWithTag(500) as UILabel
            var complaintLabel     = cell.viewWithTag(401) as UILabel
            var complaintTextView  = cell.viewWithTag(402) as SummaryTextView
            var timeLabel          = cell.viewWithTag(403) as UILabel
            
            var appointments = dict.objectForKey("appointment") as NSArray
            var appointment: Appointment = appointments.objectAtIndex(indexPath.row) as Appointment
            timeLabel.text               = self.getTime(appointment.date)
            complaintTextView.text       = appointment.complaint
            complaintTextView.textColor  = UIColor.darkGrayColor()
            date.text                    = self.getDateOnly(appointment.date)
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            
            cell.rightButtons = [self.cellDeleteButton(indexPath, appointment: appointment) as MGSwipeButton]
            cell.rightSwipeSettings.transition = MGSwipeTransition.Border
        }
        
        cell.clipsToBounds = true
        cell.layer.cornerRadius = 5.0
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor(red: (209/255.0), green: (210/255.0), blue: (206/255.0), alpha: 1.0).CGColor
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var dict = self.appointmentArray.objectAtIndex(indexPath.section) as NSMutableDictionary
        var appointment = dict.objectForKey("appointment") as NSArray
        var title = dict.objectForKey("title") as NSString
        
        if appointment.count == 0 {
            return
        }
        
        var aavc : AddAppointmentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddAppointmentViewController") as AddAppointmentViewController
        aavc.info = info
        aavc.appointmentDetails = appointment[indexPath.row] as? Appointment
        aavc.isViewAppointment  = true
        aavc.userCanEdit        = false
        
        if title.isEqualToString(PAST) {
            aavc.isViewAppointment  = true
            aavc.userCanEdit        = false
        } else {
            aavc.isViewAppointment  = true
            aavc.userCanEdit        = true
        } 
        self.navigationController?.pushViewController(aavc, animated: true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var dict = self.appointmentArray.objectAtIndex(section) as NSMutableDictionary
        var appointment = dict.objectForKey("appointment") as NSArray
        var title = dict.objectForKey("title") as NSString
        if title.isEqualToString(UPCOMING) {
        } else {
            if appointment.count == 0 {
                return 1
            }
        }
        return appointment.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.appointmentArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var dict = self.appointmentArray.objectAtIndex(indexPath.section) as NSMutableDictionary
        var title = dict.objectForKey("title") as NSString
        
        if title.isEqualToString(PAST) {
            return 128
        } else {
            return 85
        }
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var dict = self.appointmentArray.objectAtIndex(section) as NSMutableDictionary
        return dict.objectForKey("title") as? String
    }
    
    func cellDeleteButton(indexPath: NSIndexPath, appointment: Appointment) -> MGSwipeButton {
        var button: MGSwipeButton = MGSwipeButton(title: "     ", backgroundColor: self.eRedColor()) { (sender:MGSwipeTableCell!) -> Bool in
            let alert = UIAlertView()
            alert.title = "EMR"
            alert.message = "Are you sure you want to cancel appointment with \(appointment.firstName) \(appointment.lastName)?"
            alert.addButtonWithTitle("No")
            alert.addButtonWithTitle("Yes")
            alert.delegate = self
            alert.show()
            self.selectedIndex = indexPath
            return false
        }
        button.titleLabel?.font = UIFont(name: "fontawesome", size: 30)
        return button
    }
    
    //MARK: AlertView
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            self.HUD.show(true)
            self.HUD.labelText = "Cancelling Appointment..."
            let dict = self.appointmentArray.objectAtIndex(selectedIndex.section) as NSMutableDictionary
            let appointments = dict.objectForKey(self.keyAppointment) as NSArray
            let appointment = appointments[selectedIndex.row] as Appointment
            self.appointments.deleteAppointmentWithEntryId(appointment.entryId!, inBlock: { (succeeded:Bool?, error:AnyObject?) -> Void in
                if succeeded == true {
                    self.showAlert("Successfully Cancelled appointment!")
                    NSNotificationCenter.defaultCenter().postNotificationName(self.fetchAppointmentListener, object: nil)
                    self.fetchAppointmentsByPatient()
                } else {
                    self.HUD.hide(false)
                    self.showAlert("Oops! something went wrong. Please try again.")
                }
            })
        }
    }
    
}

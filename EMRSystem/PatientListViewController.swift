//
//  PatientListViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/24/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class PatientListViewController: BaseViewController , UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate , UISearchControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var patientArray = [PatientInfo]()
    var searchResult: [PatientInfo] = []
    
    var isList = true
    
    //MARK: Initialization
    override func viewDidLoad() {
        self.setListener()
        self.setUpNavigationBar()
        self.searchDisplayController?.searchResultsTableView.tableFooterView = UIView(frame: CGRectZero)
        self.searchBar.barTintColor = eRedColor()
        self.searchBar.translucent = false
        var textFieldInsideSearchBar = searchBar.valueForKey("searchField") as? UITextField
            textFieldInsideSearchBar?.textColor = self.eRedColor()
        self.setUpPatientTable()
    }
    
    func setListener() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setUpPatientTable", name: self.patientListener, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "showHUD", name: self.patientHUDListener, object: nil)
    }
    
    func showHUD() {
        self.HUD.show(true)
    }
    
    func setUpNavigationBar() {
        if isList == true {
            self.navigationItem.title = "Patients"
            var addButton : UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "gotoAddPatientInfo")
            self.navigationItem.rightBarButtonItem = addButton
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.whiteColor()
        } else {
            self.navigationItem.title = "Select Patient"
        }
    }
    
    func setUpPatientTable() {
        self.HUD.hide(true)
        self.patientArray = AuthService.sharedInstance.allPatientsArray
        if self.patientArray.count > 0 {
            self.tableView.tableFooterView?.hidden = true
        } else {
            self.tableView.tableFooterView?.hidden = false
        }
        self.tableView.reloadData()
    }
    
    func gotoAddPatientInfo() {
        var apvc : AddPatientViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddPatientViewController") as AddPatientViewController
        self.navigationController?.pushViewController(apvc, animated: true)
    }
    
    //MARK: UITableViewDelegate
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "patientCell"
        var cell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as MGSwipeTableCell!
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier) as MGSwipeTableCell
        }
        
        var photoImageView = cell.viewWithTag(100) as UIImageView
        var genderAgeLabel = cell.viewWithTag(101) as UILabel
        var fullNameLabel  = cell.viewWithTag(102) as UILabel
        var patientView    = cell.viewWithTag(103) as UIView!
        var addressLabel   = cell.viewWithTag(104) as UILabel
        var emailLabel     = cell.viewWithTag(105) as UILabel
        var contactNumberLabel  = cell.viewWithTag(106) as UILabel
        
        var patientDetail : PatientInfo!
        
        if tableView == self.searchDisplayController?.searchResultsTableView {
            patientDetail = self.searchResult[indexPath.row] as PatientInfo
        } else {
            patientDetail = self.patientArray[indexPath.row] as PatientInfo
            if self.patientArray.count != 0 {
                patientView.hidden = false
            } else {
                patientView.hidden = true
            }
        }
        
        fullNameLabel.text  = patientDetail.lastName + ", " + patientDetail.firstName + " " + patientDetail.middleName
        genderAgeLabel.text = String(patientDetail.age) + " " + patientDetail.gender
        
        
        if patientDetail.address != "" {
            addressLabel.text = "\(patientDetail.address), \(patientDetail.city)"
        } else {
            addressLabel.text = "Not Available"
        }
        
        if patientDetail.email != "" {
            emailLabel.text = patientDetail.email
        } else {
            emailLabel.text = "Not Available"
        }
        
        if patientDetail.contactNo != "" {
            contactNumberLabel.text = patientDetail.contactNo
        } else {
            contactNumberLabel.text = "Not Available"
        }
        
        photoImageView.layer.masksToBounds = true
        photoImageView.layer.cornerRadius = photoImageView.frame.size.height/2
        photoImageView.layer.masksToBounds = true
        
        if let photoFileId = patientDetail.photo {
            if photoFileId != "" {
                self.userAccount.getImageWithBlock(photoFileId, withBlock: { (succeeded:Bool?, object:AnyObject?) -> Void in
                    if succeeded == true {
                        let image = object as UIImage
                        photoImageView.image = image
                    } else {
                        photoImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.getBackgroundColor(indexPath.row), circular: true)
                    }
                })
            } else {
                photoImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: getBackgroundColor(indexPath.row), circular: true)
            }
        } else {
            photoImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: getBackgroundColor(indexPath.row), circular: true)

        }
        
        if isList == true {
            var buttonTag = 200 + indexPath.row
            var moreButton : UIButton       = UIButton(frame: CGRectMake(0, 0, 80, 50))
                moreButton.tag              = buttonTag
                moreButton.backgroundColor  = self.eRedColor()
                moreButton.titleLabel?.font = UIFont(name: "fontawesome", size: 30)
                moreButton.setTitle("     ", forState: UIControlState.Normal)
                moreButton.addTarget(self, action: "gotoAppointments:", forControlEvents: UIControlEvents.TouchUpInside)
            cell.rightButtons = [moreButton]
            cell.rightSwipeSettings.transition = MGSwipeTransition.Border
        }

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var patientInfo = PatientInfo()
        
        if tableView == self.searchDisplayController?.searchResultsTableView {
            patientInfo = searchResult[indexPath.row] as PatientInfo
        } else {
            patientInfo = self.patientArray[indexPath.row]
        }
        
        if isList == true {
            var ppvc = self.storyboard?.instantiateViewControllerWithIdentifier("PatientProfileViewController") as PatientProfileViewController
            ppvc.patientDetails = patientInfo
            self.navigationController?.pushViewController(ppvc, animated: true)
        } else {
            var aavc : AddAppointmentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddAppointmentViewController") as AddAppointmentViewController
            aavc.info = patientInfo
            aavc.isViewAppointment = false
            aavc.userCanEdit = true
            aavc.isFromAppointmentViewController = true
            self.navigationController?.pushViewController(aavc, animated: true)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController?.searchResultsTableView {
            return self.searchResult.count
        } else {
            return self.patientArray.count
        }
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95
    }
    
    //MARK: UISearchBar Delegate
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String!) -> Bool {
        searchResult = []
        for info in patientArray {
            if containsString(info.firstName, keyword: searchString)   ||
                containsString(info.middleName, keyword: searchString) ||
                containsString(info.lastName, keyword: searchString)   {
                    searchResult.append(info)
            }
        }
        return true
    }
    
    //MARK: Swipe Options
    func gotoAppointments(sender: UIButton) {
        self.hideSwipe(sender.tag - 200)
        var avc = self.storyboard?.instantiateViewControllerWithIdentifier("PatientAppointmentViewController") as PatientAppointmentViewController
            avc.info = self.patientArray[sender.tag - 200]
        self.navigationController?.pushViewController(avc, animated: true)
    }
    
    func hideSwipe(row:Int) {
        let indexPath = NSIndexPath(forRow: row, inSection: 0)
        println(indexPath)
        if tableView == self.searchDisplayController?.searchResultsTableView {
        
        } else {
            let cell = self.tableView.cellForRowAtIndexPath(indexPath) as MGSwipeTableCell
                cell.hideSwipeAnimated(true)
        }

    }
    
    func getBackgroundColor(row:Int) -> UIColor {
        if row%2==0 {
            return eRedColor()
        } else if row%3==0 {
            return eLightRedColor()
        } else {
            return eDarkRedColor()
        }
    }
}

//
//  LocalNotification.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 5/7/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class LocalNotification: NSObject {
    
    //Delay
    let alertAdvanceMinute = 30
    
    //Application
    var application:UIApplication = UIApplication.sharedApplication()
    
    //Local Notification
    let notificationId = "id"
    
    //MARK: Local Notification
    func setLocalNotification(patientName: NSString, atTimeOf time: NSString, id: NSString) {
        var firstNotification = UILocalNotification()
        
        var date = self.convertDateFromString(time)! as NSDate
        
        var calendar = NSCalendar(identifier: NSGregorianCalendar)
        var components = self.dateComponents(date) as NSDateComponents
        components.minute -= self.alertAdvanceMinute
        
        firstNotification.fireDate = calendar?.dateFromComponents(components)
        firstNotification.alertBody = "Next appointment with \(patientName) at \(self.getTime(time))"
        firstNotification.timeZone = NSTimeZone.defaultTimeZone()
        firstNotification.applicationIconBadgeNumber = self.application.applicationIconBadgeNumber + 1
        
        var firstNotificationDict = [NSString: NSString]()
        firstNotificationDict[self.notificationId] = "first\(id)"
        firstNotification.userInfo = firstNotificationDict
        
        self.application.scheduleLocalNotification(firstNotification)
        UIApplication.sharedApplication().scheduleLocalNotification(firstNotification)
        
        var secondNotification = UILocalNotification()
        
        secondNotification.fireDate = date
        secondNotification.alertBody = "Appointment with \(patientName) started."
        secondNotification.timeZone = NSTimeZone.defaultTimeZone()
        secondNotification.applicationIconBadgeNumber = self.application.applicationIconBadgeNumber + 1
        
        var secondNotificationDict = [NSString: NSString]()
        secondNotificationDict[self.notificationId] = "second\(id)"
        secondNotification.userInfo = secondNotificationDict
        
        self.application.scheduleLocalNotification(secondNotification)
        
    }
    
    func clearAll() {
        self.application.cancelAllLocalNotifications()
    }
    
    func cancel(#entryId: NSString) {
        let schedules = self.application.scheduledLocalNotifications as NSArray
        var ctr = 0 as Int
        for schedule in schedules {
            var notification: UILocalNotification = schedule as UILocalNotification
            if let id:NSString = notification.userInfo?[self.notificationId] as? NSString {
                if id.isEqualToString("first\(entryId)") || id.isEqualToString("second\(entryId)") {
                    self.application.cancelLocalNotification(notification)
                    ctr++
                }
                
                if ctr == 2 {
                    break
                }
            }
        }
    }
    
    
    func getTime(date: String?) -> String {
        let formatterDate = NSDateFormatter()
        formatterDate.dateFormat = "hh:mm a"
        
        return "\(formatterDate.stringFromDate(convertDateFromString(date!)!))"
    }
    func convertDateFromString(date: String) -> NSDate? {
        let formatterDate = NSDateFormatter()
        formatterDate.timeZone = NSCalendar.currentCalendar().timeZone
        formatterDate.dateFormat = "MMM dd, yyyy hh:mm a"
        return formatterDate.dateFromString(date)!
    }

    func dateComponents(date :NSDate) -> NSDateComponents {
        let flags: NSCalendarUnit = .DayCalendarUnit | .MonthCalendarUnit | .YearCalendarUnit | .HourCalendarUnit | .MinuteCalendarUnit | .SecondCalendarUnit
        let components = NSCalendar.currentCalendar().components(flags, fromDate:date)
        return components
    }
    
    func isDate(date:NSDate, isEqualTo comparedDate: NSDate) -> Bool {
        var dateCompare = date.compare(comparedDate)
        if dateCompare == NSComparisonResult.OrderedSame {
            return true
        }
        return false
    }
    
    func isDate(date:NSDate, isGreaterThan comparedDate: NSDate) -> Bool {
        var dateCompare = date.compare(comparedDate)
        if dateCompare == NSComparisonResult.OrderedAscending {
            return true
        }
        return false
    }
    
    func isDate(date:NSDate, isLesserThan comparedDate: NSDate) -> Bool {
        var dateCompare = date.compare(comparedDate)
        if dateCompare == NSComparisonResult.OrderedDescending {
            return true
        }
        return false
    }

    
}

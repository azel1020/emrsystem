//
//  AppointmentListViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 4/1/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class AppointmentListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var moreButton : UIButton!
    var appointmentArray: NSMutableArray = []
    var isShowAll      = false
    var pastLimitCount = 5
    var selectedIndex : NSIndexPath!
    var archiveButton : UIBarButtonItem = UIBarButtonItem()
    var isShowArchived = false
    
    //MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showHUD(true)
        self.HUD.labelText = "Retrieving Appointments..."
        self.setUpNavigation()
        self.updateAppointmentTable()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateAppointmentTable", name: updateAppointmentList, object: nil)
    }
    
    func setUpNavigation() {
        var addButton : UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "gotoPatientList")
        self.navigationItem.rightBarButtonItem = addButton
        self.navigationItem.title = "Appointments"
        
        self.archiveButton = UIBarButtonItem(title: "Archives", style: UIBarButtonItemStyle.Plain, target: self, action: "reloadTableForPastAppointment")
        self.navigationItem.leftBarButtonItem = self.archiveButton
    }
    
    func reloadTableForPastAppointment() {
        isShowArchived = !isShowArchived
        
        if isShowArchived == true {
            self.archiveButton.title = "Back"
            self.navigationItem.title = "Archived Appointments"
        } else {
            self.archiveButton.title = "Archives"
            self.navigationItem.title = "Appointments"
        }
    
        self.tableView.reloadData()
    }
    
    func updateAppointmentTable() {
        if let allAppointments = AuthService.sharedInstance.allAppointmentArray {
            self.appointmentArray = self.convertToDisplayGroupings(allAppointments as [Appointment])
            self.tableView.reloadData()
            self.showHUD(false)
        }
    }
    
    func gotoPatientList() {
        var plvc : PatientListViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PatientListViewController") as PatientListViewController
        plvc.isList = false
        self.navigationController?.pushViewController(plvc, animated: true)
    }
    
    //MARK: UITableview Delegate
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : MGSwipeTableCell!
        var dict = self.appointmentArray.objectAtIndex(indexPath.section) as NSMutableDictionary
        
        if isShowArchived == true {
            dict = appointmentArray.lastObject as NSMutableDictionary
        }
        
        var appointments = dict.objectForKey(self.keyAppointment) as NSArray
        var title        = dict.objectForKey(self.keyTitle) as NSString
        
        if appointments.count == 0 {
            cell = self.tableView.dequeueReusableCellWithIdentifier("EmptyStateCell", forIndexPath: indexPath) as MGSwipeTableCell
            var emptyTitle         = cell.viewWithTag(2) as UILabel
            var emptySubtitle      = cell.viewWithTag(3) as UILabel
                emptyTitle.text    = "\(dict.objectForKey(self.keyEmptyTitle)!)"
                emptySubtitle.text = "\(dict.objectForKey(self.keyEmptySubtitle)!)"
            return cell
        }
        
        var appointment  = appointments.objectAtIndex(indexPath.row) as Appointment
        if title.isEqualToString(TODAY) {
            cell = self.tableView.dequeueReusableCellWithIdentifier("TodayCell", forIndexPath: indexPath) as MGSwipeTableCell
            var weekDay         = cell.viewWithTag(self.tagDay) as UILabel
            var aheadLabel      = cell.viewWithTag(self.tagAheadTime) as UILabel
            var statusLabel     = cell.viewWithTag(20) as UILabel
            var date            = self.convertDateFromString(appointment.date, withTime:true) as NSDate!
                weekDay.text    = self.getWeekDay(self.convertDateFromString(appointment.date, withTime:true)!, isComplete: false)
                aheadLabel.text = self.timeAhead(date)
            
            if isAppointmentFinished(appointment.status!) {
                statusLabel.text = ""
            } else if isAppointmentMissed(appointment.status!, date: appointment.date) {
                statusLabel.text = ""
            } else {
                statusLabel.text = ""
            }
        } else if title.isEqualToString(UPCOMING) {
            cell = self.tableView.dequeueReusableCellWithIdentifier("UpcommingCell", forIndexPath: indexPath) as MGSwipeTableCell
            var dateLabel: UILabel = cell.viewWithTag(self.tagFullDate) as UILabel
                dateLabel.text     = self.getDateOnly(appointment.date)
            var dayLabel: UILabel  = cell.viewWithTag(self.tagDay) as UILabel
                dayLabel.text      = self.getWeekDay(self.convertDateFromString(appointment.date, withTime:true)!, isComplete: false)
        } else {
            cell = self.tableView.dequeueReusableCellWithIdentifier("PastCell", forIndexPath: indexPath) as MGSwipeTableCell
            var dateLabel: UILabel = cell.viewWithTag(self.tagFullDate) as UILabel
                dateLabel.text = self.getDateOnly(appointment.date)
            var agoLabel: UILabel  = cell.viewWithTag(self.tagAgoTime) as UILabel
            var date = self.convertDateFromString(appointment.date, withTime:true) as NSDate!
                agoLabel.text = date.timeAgo()
        }
        

        var photoImageView: UIImageView       = cell.viewWithTag(self.tagPhotoImage) as UIImageView
        var timeLabel: UILabel                = cell.viewWithTag(self.tagTime) as UILabel
        var nameLabel: UILabel                = cell.viewWithTag(self.tagName) as UILabel
        var complaintLabel: SummaryTextView   = cell.viewWithTag(self.tagComplaint) as SummaryTextView
        
        if self.hasCacheFor(appointment.patientId) == true {
            let patientDetail = self.patientInfo.getDictionary(self.getCacheFor(appointment.patientId) as NSDictionary)
            
            if let photoFileId = patientDetail.photo {
                if photoFileId != "" {
                    self.userAccount.getImageWithBlock(photoFileId, withBlock: { (succeeded:Bool?, object:AnyObject?) -> Void in
                        
                        if succeeded == true {
                            let image = object as UIImage
                            photoImageView.image = image
                        } else {
                            photoImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.getAppointmentColor(title), circular: true)
                        }
                    })
                } else {
                    photoImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.getAppointmentColor(title), circular: true)
                }
            } else {
                photoImageView.setImageWithString("\(patientDetail.firstName) \(self.getInitialLetter(patientDetail.lastName))", color: self.getAppointmentColor(title), circular: true)
            }
        }
        
        nameLabel.text      = "\(appointment.lastName) \(appointment.firstName), \(appointment.middleName)"
        complaintLabel.text = appointment.complaint
        complaintLabel.textColor = UIColor.darkGrayColor()
        timeLabel.text      = self.getTime(appointment.date)
        
        var deleteButton: MGSwipeButton = MGSwipeButton(title: "     ", backgroundColor: self.eRedColor()) { (sender:MGSwipeTableCell!) -> Bool in
            let patientDetail = self.patientInfo.getDictionary(self.getCacheFor(appointment.patientId) as NSDictionary)
            let alert = UIAlertView()
            alert.title   = "EMR"
            alert.message = "Are you sure you want to cancel appointment with \(patientDetail.firstName) \(patientDetail.lastName)?"
            alert.addButtonWithTitle("No")
            alert.addButtonWithTitle("Yes")
            alert.delegate = self
            alert.show()
            self.selectedIndex = indexPath
            return false
        }
        deleteButton.titleLabel?.font = UIFont(name: "fontawesome", size: 30)          
        cell.rightButtons = [deleteButton]
        cell.rightSwipeSettings.transition = MGSwipeTransition.Border
        
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var dict = self.appointmentArray.objectAtIndex(indexPath.section) as NSMutableDictionary
        if isShowArchived == true {
            dict = self.appointmentArray.lastObject as NSMutableDictionary
        }

        let appointments = dict.objectForKey(self.keyAppointment) as [Appointment]
        let title = dict.objectForKey(self.keyTitle) as NSString

        var aavc = self.storyboard?.instantiateViewControllerWithIdentifier("AddAppointmentViewController") as AddAppointmentViewController
        var appointmentDetail  = appointments[indexPath.row]
        var info : PatientInfo = PatientInfo()
            info.firstName  = appointmentDetail.firstName
            info.lastName   = appointmentDetail.lastName
            info.middleName = appointmentDetail.middleName
            info.age        = (appointmentDetail.patientAge).toInt()
            info.gender     = appointmentDetail.patientGender
        
            aavc.info               = info
            aavc.appointmentDetails = appointmentDetail
            aavc.isViewAppointment  = true
            aavc.userCanEdit        = true

        self.navigationController?.pushViewController(aavc, animated: true)
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let dict = self.appointmentArray.objectAtIndex(indexPath.section) as NSMutableDictionary
        let appointment = dict.objectForKey(self.keyAppointment) as NSArray
        if appointment.count == 0 {
            return 115
        }
        return 100
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var dict : NSMutableDictionary = NSMutableDictionary()
        if isShowArchived == true {
            dict = self.appointmentArray.lastObject as NSMutableDictionary
        } else {
            dict = self.appointmentArray.objectAtIndex(section) as NSMutableDictionary
        }
        
        let appointment = dict.objectForKey(self.keyAppointment) as NSArray
        let title       = dict.objectForKey(self.keyTitle) as NSString

        return appointment.count == 0 ? 1 : appointment.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var result = isShowArchived == true ? 1 : isUpcomingExists() == true ? 2 : 1
        return result
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var dict = self.appointmentArray.objectAtIndex(section) as NSMutableDictionary
        if isShowArchived {
            dict = self.appointmentArray.lastObject as NSMutableDictionary
        }
    
        let appointment = dict.objectForKey(self.keyAppointment) as NSArray
        let title       = dict.objectForKey(self.keyTitle) as NSString
        return "\(title) (\(appointment.count))"
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    //MARK: Helper
    func isUpcomingExists() -> Bool {
        if self.appointmentArray.count == 3 {
            return true
        }
        return false
    }
    
    func getAppointmentColor(title: String) -> UIColor {
        switch title {
        case TODAY    : return eDarkRedColor()
        case PAST     : return eLightRedColor()
        case UPCOMING : return eRedColor()
        default: return eRedColor()
        }
    }
    
    //MARK: AlertView
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            self.showHUD(true)
            self.HUD.labelText = "Cancelling Appointment..."
            let dict = self.appointmentArray.objectAtIndex(selectedIndex.section) as NSMutableDictionary
            let appointments = dict.objectForKey(self.keyAppointment) as NSArray
            let appointment = appointments[selectedIndex.row] as Appointment
            self.appointments.deleteAppointmentWithEntryId(appointment.entryId!, inBlock: { (succeeded:Bool?, error:AnyObject?) -> Void in
                if succeeded == true {
                    self.showHUD(false)
                    self.showHUD(true)
                    self.HUD.labelText = "Retrieving Appointments..."
                    self.showAlert("Successfully Cancelled appointment!")
                    NSNotificationCenter.defaultCenter().postNotificationName(self.fetchAppointmentListener, object: nil)
                } else {
                    self.showHUD(false)
                    self.showAlert("Oops! something went wrong. Please try again.")
                }
            })
        }
    }
    
}

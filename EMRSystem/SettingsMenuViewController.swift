//
//  SettingsMenuViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 6/1/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class SettingsMenuViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    
    let list: List    = List()
    var selectedTab   = 0
    var selectedIndex : NSIndexPath!
    var selectedArray = []
    let base: BaseViewController = BaseViewController()
    let grant:GrantAndRevokeAccessService = GrantAndRevokeAccessService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        selectedArray = List.sharedInstance.medicalList
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.grant.getAppointmentOf(dateCode: self.base.getDateDatabaseFormat(NSDate()), query: GrantAndRevokeAccessService.appointmentQuery.match) { (object:AnyObject?, error:AnyObject?) -> Void in
        }

    }
    
    func setUpNavigationBar() {
        self.navigationItem.title = "Manage Lists"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: UITableView Delegate and DataSource
extension SettingsMenuViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : MGSwipeTableCell!
        
        if selectedArray.count == 0 {
            cell  = tableView.dequeueReusableCellWithIdentifier("EmptyStateCell", forIndexPath: indexPath) as MGSwipeTableCell
            var message   = cell.viewWithTag(2) as UILabel
            var descLabel = cell.viewWithTag(3) as UILabel
            var msgString = selectedTab == 0 ? "Medical" : "Location"
            
                message.text   = "No \(msgString) List Found."
                descLabel.text = "Added \(msgString) List can be found here."
            return cell
        }
        
        cell  = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as MGSwipeTableCell
        let title = selectedArray[indexPath.row] as String
        var titleLabel      = cell.viewWithTag(100) as UILabel
            titleLabel.text = title
        
        var deleteButton: MGSwipeButton = MGSwipeButton(title: "     ", backgroundColor: eRedColor()) { (sender:MGSwipeTableCell!) -> Bool in
            let alert = UIAlertView()
            alert.title = "EMR"
            alert.message = "Are you sure you want to delete \"\(title)\"?"
            alert.addButtonWithTitle("No")
            alert.addButtonWithTitle("Yes")
            alert.delegate = self
            alert.show()
            self.selectedIndex = indexPath
            return false
        }
        deleteButton.titleLabel?.font = UIFont(name: "fontawesome", size: 30)
        cell.rightButtons = [deleteButton]
        cell.rightSwipeSettings.transition = MGSwipeTransition.Border
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.whiteColor()
        } else {
            cell.backgroundColor = UIColor.groupTableViewBackgroundColor()
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedArray.count == 0 ? 1 : selectedArray.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return selectedTab == 0 ? "Medical List (\(selectedArray.count))" : "Location List (\(selectedArray.count))"
    }
    
}

//MARK: TabBar Delegate
extension SettingsMenuViewController : UITabBarDelegate, UITabBarControllerDelegate {
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem!) {
        selectedTab = item.tag
        selectedArray = selectedTab == 0 ? List.sharedInstance.medicalList : List.sharedInstance.locationList
        self.tableView.reloadData()
        println("Item : \(item.tag)")
    }
    
}

//MARK: AlertView Delegate 
extension SettingsMenuViewController : UIAlertViewDelegate {
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        let title = selectedArray[self.selectedIndex.row] as String
        var key   = selectedTab == 0 ? ListType.Medical : ListType.Location
        
        self.showHUD(true)
        self.HUD.labelText = "Deleting item..."
        self.list.deleteToObjectList(title, type: key)
        self.showHUD(false)
        
        selectedArray = selectedTab == 0 ? List.sharedInstance.medicalList : List.sharedInstance.locationList
        self.tableView.reloadData()
        println("Tokens List : \( List.sharedInstance.medicalList) \n \(List.sharedInstance.locationList)")
        
        
    }
    
    override func showHUD(show: Bool) {
        if show == true {
            HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        } else {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
        }
    }
}


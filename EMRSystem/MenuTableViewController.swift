//
//  MenuTableViewController.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 4/17/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {

    var base: BaseViewController = BaseViewController()
    let localNotification: LocalNotification = LocalNotification()
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var patientCountLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var patientActivityIndicator: UIActivityIndicatorView!
    
    let menu = [
        ["","DashboardViewController"],
        ["AppointmentListViewController","PatientListViewController","StaffListViewController"],
        ["","SettingsMenuViewController","AboutUsViewController","Logout"],
        "UserProfileViewController"
    ] as NSArray
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        EGOCache.globalCache().clearCache()
//        self.fetchAllPatients()
        self.setListener()
        
        self.photoImageView.layer.borderWidth   = 2.0
        self.photoImageView.layer.borderColor   = UIColor.whiteColor().CGColor
        self.photoImageView.layer.cornerRadius  = self.photoImageView.frame.height / 2
        self.photoImageView.layer.masksToBounds = true
    }
    
    func setListener() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setUserDetail", name: self.base.menuUserListener, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "fetchAllPatients", name: self.base.fetchPatientListener, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "fetchAllAppointments", name: self.base.fetchAppointmentListener, object: nil)
    }
    
    func setUserDetail() {
        self.fetchAllPatients()
        self.fetchAllAppointments()
        
        self.fullnameLabel.text   = "\(AuthService.sharedInstance.userCredentials.name.firstName) \(AuthService.sharedInstance.userCredentials.name.lastName)"
        if let department: String = AuthService.sharedInstance.userCredentials.extraForKey(self.base.userAccount.department) as? String {
            self.departmentLabel.text = department
        }
        
        if let position: String = AuthService.sharedInstance.userCredentials.extraForKey(self.base.userAccount.position) as? String {
            self.positionLabel.text = position
        }
        
        if let photoFileId = AuthService.sharedInstance.userCredentials.profilePhoto {
            if photoFileId != "" {
                if self.base.userAccount.hasCacheImageFor(photoFileId) {
                    self.photoImageView.image = self.base.userAccount.getCacheImageFor(photoFileId)
                } else {
                    self.base.userAccount.retrieveProfilePhoto(photoFileId, withBlock: { (succeeded:Bool?, result:AnyObject?) -> Void in
                        if succeeded == true {
                            self.photoImageView.image = result as? UIImage
                            NSNotificationCenter.defaultCenter().postNotificationName(self.base.userProfileListener, object: nil)
                        } else {
                            self.base.showAlertError(result!)
                        }
                    })
                }
            } else {
                self.photoImageView.image = UIImage(named: "profile")
            }
        } else {
            self.photoImageView.image = UIImage(named: "profile")
        }
    }
    
    //MARK: Fetch Patient Infos and Appointments
    func fetchAllPatients() {
        self.patientCountLabel.hidden = true
        self.patientActivityIndicator.hidden = false
        base.patientInfo.fetchAllPatient { (object: [AnyObject]?, error: AnyObject?) -> Void in
            if error == nil {
                var patientArray = object! as [PatientInfo]
                self.patientCountLabel.text = "\(patientArray.count)"
                self.patientCountLabel.hidden = false
                self.patientActivityIndicator.hidden = true
                NSNotificationCenter.defaultCenter().postNotificationName(self.base.patientListener, object:nil)
                NSNotificationCenter.defaultCenter().postNotificationName(self.base.updateAppointmentList, object:nil)
            } else {
                self.base.showAlertError(error!)
            }
        }
    }
    
    func fetchAllAppointments() {
        base.appointments.fetchAllAppointments { (object: [AnyObject]?, error: AnyObject?) -> Void in
            if error == nil {
            var appointmentArray = object! as [Appointment]
                NSNotificationCenter.defaultCenter().postNotificationName(self.base.updateAppointmentList, object:nil)
            } else {
                self.base.showAlertError(error!)
            }
        }
    }
    
    //MARK: TableView Delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("Sending...")
        var section = self.menu.objectAtIndex(indexPath.section) as NSArray
        var identifier = section.objectAtIndex(indexPath.row) as NSString
        
        if identifier.isEqualToString("") {
            return
        } else if identifier.isEqualToString("Logout") {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.localNotification.clearAll()
            NSNotificationCenter.defaultCenter().postNotificationName(self.base.splitviewListener, object:false)
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName(self.base.navigationListener, object:identifier)
        }
    }
    
    //MARK: User Profile
    @IBAction func editUserProfile(sender: AnyObject) {
        var identifier = menu[3] as NSString
        if let indexpath = self.tableView.indexPathForSelectedRow() {
            self.tableView.deselectRowAtIndexPath(indexpath, animated: true)
        }
        NSNotificationCenter.defaultCenter().postNotificationName(self.base.navigationListener, object: identifier)
    }
    
    //MARK: Others
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

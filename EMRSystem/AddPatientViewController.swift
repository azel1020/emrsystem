//
//  AddPatientViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/23/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class AddPatientViewController: UITableViewController, UIPopoverControllerDelegate , UIImagePickerControllerDelegate, UIActionSheetDelegate , UINavigationControllerDelegate, RMDateSelectionViewControllerDelegate , KSTokenViewDelegate {
    @IBOutlet var patientTableView: UITableView!
    @IBOutlet weak var dateofFirstDateButon: UIButton!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var middleNameTextField: UITextField!
    @IBOutlet weak var dateofBirthButton: UIButton!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var citytextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var contactNoTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var refPhysicianTextField: UITextField!
    @IBOutlet weak var attPhysicianTextField: UITextField!
    @IBOutlet weak var allergiesTokenField: KSTokenView!
    @IBOutlet weak var famHistoryTokenField: KSTokenView!
    @IBOutlet weak var othersTextField: UITextField!
    @IBOutlet weak var profilePhoto: UIImageView!
    
    let patientInfo : PatientInfo = PatientInfo()
    let base : BaseViewController = BaseViewController()
    var selectedBdate : NSDate!
    
    var dateSelector = RMDateSelectionViewController.dateSelectionController()
    var popoverController: UIPopoverController!
    var popOverView: UIPopoverController!
    var datePicker: UIDatePicker!
    var doneButton: UIButton!

    var photoImage: NSMutableDictionary = NSMutableDictionary()
    var photoToBeDeleted: NSMutableDictionary = NSMutableDictionary()
    
    //KSTokenView 
    var names: Array<String> = List.sharedInstance.medicalList
    var selectedAllergies : [String] = []
    var selectedFamHistory: [String] = []
    
    //Profile Photo
    var photoPicker : UIImagePickerController? = UIImagePickerController()
    
    //for viewing PatientInfo
    var info: PatientInfo!
    var isViewPatientInfo : Bool = false
    
    //MARK: Initialization
    override func viewDidLoad() {
        self.setUpTokenViews()
        
        self.patientTableView.layer.cornerRadius = 6.0
        self.patientTableView.tableFooterView = UIView(frame: CGRectZero)
        
        self.dateofBirthButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.dateofBirthButton.layer.borderWidth = 1.0
        self.dateofBirthButton.layer.cornerRadius = 6.0
        
        self.genderButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.genderButton.layer.borderWidth = 1.0
        self.genderButton.layer.cornerRadius = 5.0
        
        self.allergiesTokenField.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.allergiesTokenField.layer.borderWidth = 1.0
        self.allergiesTokenField.layer.cornerRadius = 6.0
        
        self.famHistoryTokenField.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.famHistoryTokenField.layer.borderWidth = 1.0
        self.famHistoryTokenField.layer.cornerRadius = 6.0
        
        self.dateSelector.delegate = self
        self.dateSelector.hideNowButton = true
        
        self.profilePhoto.layer.borderWidth   = 3.0
        self.profilePhoto.layer.borderColor   = base.eRedColor().CGColor
        self.profilePhoto.layer.cornerRadius  = self.profilePhoto.frame.height / 2
        self.profilePhoto.layer.masksToBounds = true
        
        var tap : UITapGestureRecognizer      =  UITapGestureRecognizer(target: self, action: "showTakePhotoOptions")
        self.profilePhoto.addGestureRecognizer(tap)

        if isViewPatientInfo {
            self.setPatientInfo()
        } else {
            self.setImageDictionary(UIImage(), hasImage: false, fileId: "", isNew: false)
            self.dateofFirstDateButon.setTitle(self.base.formatDate(NSDate(), withTime: false), forState: UIControlState.Normal)
            self.setUpNavigationBar()
        }
    }
    
    func showTakePhotoOptions() {
        var actionSheet = UIActionSheet()
        if profilePhoto.image != UIImage(named: "profile") {
            actionSheet = UIActionSheet(title: "Choose", delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery", "Remove Photo")
        } else {
            actionSheet = UIActionSheet(title: "Choose", delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        }
        actionSheet.tag = 1
        actionSheet.showInView(self.view)
    }
    
    func setUpNavigationBar() {
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    func setUpTokenViews() {
        allergiesTokenField.delegate    = self
        allergiesTokenField.promptText  = ""
        allergiesTokenField.style       = .Squared
        allergiesTokenField.direction   = .Horizontal
        allergiesTokenField.tag         = 1
        allergiesTokenField.font        = UIFont(name: "STHeitiTC-Medium", size: 14)!
        
        famHistoryTokenField.delegate    = self
        famHistoryTokenField.promptText  = ""
        famHistoryTokenField.placeholder = "Add Family History"
        famHistoryTokenField.style       = .Squared
        famHistoryTokenField.direction   = .Horizontal
        famHistoryTokenField.tag         = 2
        famHistoryTokenField.font        = UIFont(name: "STHeitiTC-Medium", size: 14)!
        
        if isViewPatientInfo {
            allergiesTokenField.placeholder  = base.convertToStringSet(info.allergies)
            famHistoryTokenField.placeholder = base.convertToStringSet(info.familyHistory)
        }
    }
    
    //MARK: KSTokenView
    func tokenView(token: KSTokenView, performSearchWithString string: String, completion: ((results: Array<AnyObject>) -> Void)?) {
        var data: Array<String> = []
        for value: String in names {
            if value.lowercaseString.rangeOfString(string.lowercaseString) != nil {
                data.append(value)
            }
        }
        completion!(results: data)
    }
    
    func tokenView(tokenView: KSTokenView, didAddToken token: KSToken) {
        if tokenView.tag == 1 {
            self.selectedAllergies.append(token.title)
        } else {
            self.selectedFamHistory.append(token.title)
        }
    }
    
    func tokenView(tokenView: KSTokenView, didDeleteToken token: KSToken) {
        if tokenView.tag == 1 {
            self.selectedAllergies  = base.deleteTokenFromArray(token.title, tokenArray: selectedAllergies)
        } else {
            self.selectedFamHistory = base.deleteTokenFromArray(token.title, tokenArray: selectedFamHistory)
        }
    }
    
    func tokenView(token: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        return object as String
    }
    
    func tokenView(tokenView: KSTokenView, shouldAddToken token: KSToken) -> Bool {
        let list: List = List()
        list.addToObjectList(token.title, type: ListType.Medical)
        self.names = List.sharedInstance.medicalList
        return true
    }
    
    //MARK: RMDateSelection
    @IBAction func showDatePicker(sender: AnyObject) {
        self.dateSelector.show()
    }
    
    func dateSelectionViewController(vc: RMDateSelectionViewController!, didSelectDate aDate: NSDate!) {
        self.dateofFirstDateButon.setTitle(self.base.formatDate(aDate, withTime: false), forState: UIControlState.Normal)
    }
    
    //MARK: Date Selection and Age Update
    @IBAction func selectDateOfBirth(sender: AnyObject) {
        self.view.endEditing(true)
        
        var dobRect = self.tableView.rectForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0))
        var bvc : BirthdateViewController = BirthdateViewController(nibName: "BirthdateViewController", bundle: nil)
        
        self.popOverView  = UIPopoverController(contentViewController: bvc)
        self.popOverView.presentPopoverFromRect(CGRectMake(-200, dobRect.origin.y, dobRect.width, dobRect.height), inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Up, animated: true)
        self.popOverView.passthroughViews = nil
        self.popOverView.delegate = self
        self.popOverView.setPopoverContentSize(CGSizeMake(400, 260), animated: true)
        
        self.datePicker = bvc.datePicker as UIDatePicker
        if isViewPatientInfo {
            self.datePicker.setDate(selectedBdate, animated: true)
        }
        
        self.doneButton = bvc.doneButton as UIButton
        self.doneButton.addTarget(self, action: "setDateAndAge", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    func setDateAndAge() {
        if self.base.getYear(self.datePicker.date) <=  self.base.getYear(NSDate()) {
            self.dateofBirthButton.setTitle(self.base.formatDate(self.datePicker.date, withTime: false), forState: UIControlState.Normal)
            self.popOverView.dismissPopoverAnimated(true)
            self.selectedBdate     = self.datePicker.date
            self.ageTextField.text = self.base.getAge(self.datePicker.date)
        } else {
            self.base.showAlert("Invalid Date of Birth")
        }
    }
    
    @IBAction func setGender(sender: AnyObject) {
        var actionSheet : UIActionSheet = UIActionSheet(title: "Select Gender", delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil)
            actionSheet.addButtonWithTitle("Female")
            actionSheet.addButtonWithTitle("Male")
            actionSheet.showInView(self.view)
    }
    
    //MARK: ActionSheetDelegate    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if actionSheet.tag == 1{
            self.showCamera(buttonIndex)
        } else {
            if buttonIndex == 0 {
                self.genderButton.setTitle("F", forState: UIControlState.Normal)
            } else if buttonIndex == 1 {
                self.genderButton.setTitle("M", forState: UIControlState.Normal)
            }
        }
    }
    
    func showCamera(camera : Int) {
        self.photoPicker = UIImagePickerController()
        self.photoPicker?.delegate      = self
        self.photoPicker?.allowsEditing = true
        
        switch camera {
        case 0:
            if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                self.photoPicker?.sourceType = .Camera
            } else {
                self.photoPicker?.sourceType = .SavedPhotosAlbum
            }
            self.showOperatedImagePicker()
        case 1:
            self.photoPicker?.sourceType = .SavedPhotosAlbum
            self.showOperatedImagePicker()
        case 2:
            var hasImage = self.photoImage.objectForKey(self.base.keyHasImage)?.boolValue
            var isNew = self.photoImage.objectForKey(self.base.keyIsNew)?.boolValue
            if hasImage == true && isNew == false {
                self.photoToBeDeleted.setObject(self.photoImage.objectForKey(self.base.keyImage)!, forKey: self.base.keyImage)
                self.photoToBeDeleted.setObject(self.photoImage.objectForKey(self.base.keyFileId)!, forKey: self.base.keyFileId)
                self.photoToBeDeleted.setObject(NSNumber(bool: true), forKey: self.base.keyToBeDeleted)
            }
            self.setImageDictionary(UIImage(), hasImage: false, fileId: "", isNew: false)
            self.profilePhoto.image = UIImage(named: "profile") //Remove profile photo
        default:
            break
        }
    }
    
    func showOperatedImagePicker() {
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            self.showPicker()
        }
    }
    
    func showPicker() {
        if photoPicker?.sourceType == UIImagePickerControllerSourceType.SavedPhotosAlbum {
            photoPicker?.modalPresentationStyle = .FormSheet
            self.base.setLandscapeLeftOrientation()
        } else {
            self.base.setCameraOrientation()
        }
        
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            self.photoPicker?.allowsEditing = true
            self.presentViewController(self.photoPicker!, animated: true, completion: { () -> Void in })
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        var chosenImage = info[UIImagePickerControllerEditedImage] as UIImage
        var hasImage = self.photoImage.objectForKey(self.base.keyHasImage)?.boolValue
        var isNew = self.photoImage.objectForKey(self.base.keyIsNew)?.boolValue
        if hasImage == true && isNew == false {
            self.photoToBeDeleted.setObject(self.photoImage.objectForKey(self.base.keyImage)!, forKey: self.base.keyImage)
            self.photoToBeDeleted.setObject(self.photoImage.objectForKey(self.base.keyFileId)!, forKey: self.base.keyFileId)
            self.photoToBeDeleted.setObject(NSNumber(bool: true), forKey: self.base.keyToBeDeleted)
        }
        
        self.profilePhoto.image = chosenImage
        self.setImageDictionary(chosenImage, hasImage: true, fileId: "", isNew: true)
        
        self.dismissPicker()
    }
    
    func dismissPicker() {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.base.setLandscapeLeftOrientation()
        })
    }
    
    func setImageDictionary(image:UIImage, hasImage: Bool, fileId: NSString, isNew: Bool) {
        if hasImage {
            self.photoImage.setObject(image, forKey: self.base.keyImage)
            self.photoImage.setObject(NSNumber(bool: true), forKey: self.base.keyHasImage)
            self.photoImage.setObject(fileId, forKey: self.base.keyFileId)
        } else {
            self.photoImage.setObject("", forKey: self.base.keyFileId)
            self.photoImage.setObject(NSNumber(bool: false), forKey: self.base.keyHasImage)
        }
        
        self.photoImage.setObject(NSNumber(bool: isNew), forKey: self.base.keyIsNew)
    }
    
    //MARK: UITableViewDelegate
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
            case 0 : return "Patient Information"
            case 3 : return "Medical History"
            default: return ""
        }
    }

    //MARK: Validations
    func getValidValue(string: String) -> String {
        if string.isEmpty || string == "" {
            
            return "None"
        }
        return string
    }

    //MARK: Set PatientInfo (Edit View)
    func setPatientInfo() {
        self.lastNameTextField.text     = info.lastName
        self.firstNameTextField.text    = info.firstName
        self.middleNameTextField.text   = info.middleName
        self.ageTextField.text          = String(info.age)
        self.addressTextField.text      = info.address
        self.citytextField.text         = info.city
        self.postalCodeTextField.text   = info.postalCode
        self.stateTextField.text        = info.state
        self.contactNoTextField.text    = info.contactNo
        self.emailTextField.text        = info.email
        self.refPhysicianTextField.text = info.referringPhysician
        self.attPhysicianTextField.text = info.attendingPhysician
        self.othersTextField.text       = info.others
        
        if info.allergies.count != 0 {
            for a in info.allergies {
                allergiesTokenField.addToken(KSToken(title: a))
            }
        }
        
        if info.familyHistory.count != 0 {
            for f in info.familyHistory {
                famHistoryTokenField.addToken(KSToken(title: f))
            }
        }
        
        self.selectedBdate = self.base.convertDateFromString(info.dateOfBirth, withTime: false)
        self.genderButton.setTitle(info.gender, forState: UIControlState.Normal)
        self.dateofBirthButton.setTitle(info.dateOfBirth, forState: UIControlState.Normal)
        self.dateofFirstDateButon.setTitle(info.dateofFirstVisit, forState: UIControlState.Normal)
        self.getPatientPhoto()
    }
    
    func getPatientPhoto() {
        self.base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.base.HUD.labelText = "Retrieving Photo..."
        if let photoFileId = info.photo {
            if photoFileId != "" {
                if self.base.userAccount.hasCacheImageFor(photoFileId) {
                    println("has image cache")
                    var image = self.base.userAccount.getCacheImageFor(photoFileId)
                    self.setImageDictionary(image, hasImage: true, fileId: photoFileId, isNew: false)
                    
                    self.photoToBeDeleted.setObject(NSNumber(bool: false), forKey: self.base.keyToBeDeleted)
                    self.profilePhoto.image = image
                    self.base.HUD.hide(true)
                    self.setUpNavigationBar()
                } else {
                    self.base.userAccount.getImageWithBlock(photoFileId, withBlock: { (succeeded:Bool?, object:AnyObject?) -> Void in
                        if succeeded == true {
                            var image = object as UIImage
                            self.setImageDictionary(image, hasImage: true, fileId: photoFileId, isNew: false)
                            self.photoToBeDeleted.setObject(NSNumber(bool: false), forKey: self.base.keyToBeDeleted)
                            self.profilePhoto.image = image
                            self.base.HUD.hide(true)
                            self.setUpNavigationBar()
                        } else {
                            self.base.showAlert("Oops! Something went wrong. Please try again.")
                        }
                    })
                }
            } else {
                self.setImageDictionary(UIImage(), hasImage: false, fileId: "", isNew: false)
                self.base.HUD.hide(true)
                self.setUpNavigationBar()
            }
        } else {
            self.setImageDictionary(UIImage(), hasImage: false, fileId: "", isNew: false)
            self.base.HUD.hide(true)
            self.setUpNavigationBar()
        }

    }
    
    func deletePhoto(#hasNewImage: Bool, fileId: String) {
        self.base.appointments.deletePhoto(self.photoToBeDeleted.objectForKey(self.base.keyFileId) as NSString, inBlock: { (succeeded:Bool?, object: AnyObject?) -> Void in
            if succeeded == true {
                if hasNewImage == true { //If User Changed Existing image
                    println("with saving new image")
                    self.savePhoto(hasNewImage: hasNewImage,fileId:fileId)
                } else {
                    self.base.HUD.hide(true)
                    self.savePatientDetails("")
                    
                }
            } else {
                self.base.showAlert("Oops. Something wrong. Please try again.")
                self.base.HUD.hide(true)
                self.saveButton(isEnable:true)
            }
        })
    }
    
    //MARK: Saving Patient Info
    @IBAction func savePatientInfo(sender: AnyObject) {
        var dob        = self.dateofBirthButton.titleLabel?.text
        var gender     = self.genderButton.titleLabel?.text
        var firstVisit = self.dateofFirstDateButon.titleLabel?.text

        if dob != nil {
            if gender != nil {
                if base.isConnectedToNetwork() {
                    self.base.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                    self.base.HUD.labelText = "Saving Photo..."
                    self.view.endEditing(true)
                    self.saveButton(isEnable: false)
                    var hasImage = self.photoImage.objectForKey(self.base.keyHasImage)?.boolValue
                    var hasDeletePhoto = self.photoToBeDeleted.objectForKey(self.base.keyToBeDeleted)?.boolValue
                    var isNewImage  = self.photoImage.objectForKey(self.base.keyIsNew)?.boolValue
                    var fileId = self.photoImage.objectForKey(self.base.keyFileId) as NSString
                    
                    if hasDeletePhoto == true {
                        println("---- Save to Delete previous photo ----")
                        self.deletePhoto(hasNewImage: isNewImage!, fileId: fileId)
                    } else {
                        println("---- Just save photo ----")
                        self.savePhoto(hasNewImage: isNewImage!, fileId: fileId)
                    }
                    
                } else {
                    self.base.showAlert(base.noNetworkConnectionMessage)
                }
            } else {
                self.base.showAlert("Please fill Gender")
            }
        } else {
            self.base.showAlert("Please fill Date of Birth")
        }
    }
    
    func savePhoto(#hasNewImage: Bool, fileId: String) {
        
        if hasNewImage == true {
            let data = UIImageJPEGRepresentation(profilePhoto.image, 0.5) as NSData
            
            self.base.userAccount.saveProfilePhoto(data, withBlock: { (succeeded:Bool?, result:AnyObject?) -> Void in
                if succeeded == true {
                     println("saving user details")
                    let fileDictionary = result as NSDictionary
                    let newFileId = fileDictionary.objectForKey(self.base.appointments.filesId)! as NSString
                    self.setImageDictionary(self.photoImage.objectForKey(self.base.keyImage)! as UIImage, hasImage: true, fileId: newFileId, isNew: false)
                    self.photoToBeDeleted.removeAllObjects()
                    self.savePatientDetails(newFileId)
                } else {
                    self.base.showAlert("Oops. Something wrong. Please try again.")
                    self.base.HUD.hide(true)
                    self.saveButton(isEnable:true)
                }
            })
        } else {
            println("without saving new image")
            self.savePatientDetails(fileId)
         }
        
    }
    
    func savePatientDetails(fileId: NSString) {
        self.base.HUD.labelText = "Saving Details..."
        var id = isViewPatientInfo ? info.entryId : ""
        
        patientInfo.savePatientInfo(self.dateofFirstDateButon.titleLabel?.text, fName: self.firstNameTextField.text, mName: self.middleNameTextField.text, lName: self.lastNameTextField.text, dob: self.dateofBirthButton.titleLabel?.text, age: (self.ageTextField.text).toInt()!, gender: self.genderButton.titleLabel?.text, address: self.addressTextField.text, city: self.citytextField.text, state: self.stateTextField.text, postalCode: self.postalCodeTextField.text, contactno: self.contactNoTextField.text, email: self.emailTextField.text, refPhysician: self.refPhysicianTextField.text, attPhysician: self.attPhysicianTextField.text, allergies: selectedAllergies, familyHistory: selectedFamHistory, others: getValidValue(othersTextField.text), entryId: id, photo:fileId, isFromEditView: isViewPatientInfo, withBlock: { (isSuccess: Bool?, result: AnyObject?) -> Void in
            self.base.HUD.hide(true)
            self.saveButton(isEnable:true)
            if isSuccess != false {
                if self.isViewPatientInfo {
                    self.base.showAlert("Patient Information Updated")
                } else {
                    self.base.showAlert("Patient Information Saved")
                }
                NSNotificationCenter.defaultCenter().postNotificationName(self.base.fetchPatientListener, object:nil)
                NSNotificationCenter.defaultCenter().postNotificationName(self.base.patientHUDListener, object:nil)
                NSNotificationCenter.defaultCenter().postNotificationName(self.base.updatePatientProfile, object: result)
                self.navigationController?.popViewControllerAnimated(true)
            } else {
                self.base.showAlertError(result)
            }
        })
    }
    
    //MARK: Save Button
    func saveButton(#isEnable: Bool) {
        self.navigationItem.rightBarButtonItem?.enabled = isEnable
    }
    
}

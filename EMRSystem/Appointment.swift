//
//  Appointment.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/24/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class Appointment: NSObject {
    //MARK: Appointment Container Value
    var entryId          : String?
    var patientId        : String!
    var firstName        : String!
    var lastName         : String!
    var middleName       : String!
    var patientAge       : String!
    var patientGender    : String!
    var date             : String!
    var location         : String!
    var treatment        : String!
    var complaint        : String!
    var diagnosis        : String!
    var diagnostics      : String!
    var photo            : NSMutableArray!
    
    var day              : String!
    var year             : String!
    var month            : String!
    var dateCode         : Int!
    
    var vitals           : String?
    var notes            : String?
    var medication       : String?
    var allergy          : [String]?
    var plan             : [String]?
    var observation      : [String]?
    var assessment       : [String]?
    var status           : Bool?

    
    //MARK: Appointment Custom Class Properties
    let a_entryId       = "entryId"
    
    let a_patientId     = "patientId"
    let a_fname         = "firstName"
    let a_lname         = "lastName"
    let a_mname         = "middleName"
    let a_patientAge    = "patientAge"
    let a_patientGender = "patientGender"
    let a_date          = "date"
    let a_location      = "location"
    let a_treatment     = "treatment"
    let a_complaint     = "complaint"
    let a_diagnosis     = "diagnosis"
    let a_diagnostics   = "diagnostics"
    let a_photo         = "photo"
    
    var a_day           = "day"
    var a_year          = "year"
    var a_month         = "month"
    var a_dateCode      = "dateCode"

    let a_vitals        = "vitals"
    let a_notes         = "notes"
    let a_medication    = "medication"
    let a_allergy       = "allergy"
    let a_plan          = "plan"
    let a_observation   = "observation"
    let a_assessment    = "assessment"
    let a_status        = "isAppointmentDone"
    
    let filesId         = "filesId"
    let emptyFileId     = "-"
    
    let a_content         = "content"
    
    let APPOINTMENT = "Appointment"
    let PATIENT_INFO_CLASS = "PatientInfo"
    let ENTRY_ID = "entryId"
    let patientInfo : PatientInfo = PatientInfo()
    let networkActivity: NetworkActivityIndicator = NetworkActivityIndicator()
    let localNotification: LocalNotification = LocalNotification()
    
    var retrievedInfo : PatientInfo!
    
    typealias RetrieveBlock = (AnyObject?, AnyObject?) -> Void
    typealias SaveBlock     = (Bool?, AnyObject?) -> Void
    typealias ResultBlock   = ([AnyObject]?, AnyObject?) -> Void
    typealias CountBlock    = (Int? , AnyObject?) -> Void
    
    //MARK: Fetch Appointments
    func fetchAllAppointmentsWithPatientId(entryID: String, withBlock: ResultBlock) {
        var query : CatalyzeQuery = CatalyzeQuery(className: APPOINTMENT)
        query.queryField = a_patientId
        query.queryValue = entryID
        query.pageNumber = 1
        query.pageSize = 100 //may vary
        
        query.retrieveInBackgroundForUsersId(AuthService.sharedInstance.userCredentials.usersId, success: { (result: [AnyObject]!) -> Void in
            withBlock(self.convertAppointmentArray(result as [CatalyzeEntry]), nil)
            }) { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
            withBlock(nil, result)
        }
        
    }
    
    func fetchAllAppointments(withBlock: ResultBlock) {
        var query : CatalyzeQuery = CatalyzeQuery(className: APPOINTMENT)
        query.pageNumber = 1
        query.pageSize = 100 //may vary
        
        query.retrieveInBackgroundForUsersId(AuthService.sharedInstance.userCredentials.usersId, success: { (result: [AnyObject]!) -> Void in
          
            var appointmentArray : [Appointment] = self.convertAppointmentArray(result as [CatalyzeEntry])
                AuthService.sharedInstance.allAppointmentArray = appointmentArray
            withBlock (appointmentArray, nil)
            }) { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
            withBlock(nil, result)
        }
    }
    
    
    func convertDictionaryToAppointmentArray(dict: [NSDictionary]) -> [Appointment] {
        var appointments: Array = [Appointment]()
        self.localNotification.clearAll()
        for info in dict {
            appointments.append(self.convertDictionaryToAppointments(info as NSDictionary))
        }
        return appointments
    }
    
    func convertDictionaryToAppointments(infoDict: NSDictionary) -> Appointment {
        var appointment: Appointment  = Appointment()
        var info: NSDictionary = infoDict.objectForKey(a_content) as NSDictionary
        appointment.entryId       = info.objectForKey(a_entryId)       as? String
        appointment.date          = info.objectForKey(a_date)          as? String
        appointment.location      = info.objectForKey(a_location)      as? String
        appointment.patientId     = info.objectForKey(a_patientId)     as? String
        appointment.firstName     = info.objectForKey(a_fname)         as? String
        appointment.middleName    = info.objectForKey(a_mname)         as? String
        appointment.lastName      = info.objectForKey(a_lname)         as? String
        appointment.patientGender = info.objectForKey(a_patientGender) as? String
        appointment.patientAge    = info.objectForKey(a_patientAge)    as? String
        appointment.treatment     = info.objectForKey(a_treatment)     as? String
        appointment.complaint     = info.objectForKey(a_complaint)     as? String
        appointment.diagnosis     = info.objectForKey(a_diagnosis)     as? String
        appointment.diagnostics   = info.objectForKey(a_diagnostics)   as? String
        appointment.photo         = info.objectForKey(a_photo)         as? NSMutableArray
        //appointment day code
        appointment.day           = info.objectForKey(a_day)           as? String
        appointment.month         = info.objectForKey(a_month)         as? String
        appointment.year          = info.objectForKey(a_year)          as? String
        appointment.dateCode      = info.objectForKey(a_dateCode)?.integerValue
        //when appointment is done
        appointment.status        = info.objectForKey(a_status)        as? Bool
        appointment.vitals        = info.objectForKey(a_vitals)        as? String
        appointment.notes         = info.objectForKey(a_notes)         as? String
        appointment.medication    = info.objectForKey(a_medication)    as? String
        appointment.allergy       = info.objectForKey(a_allergy)       as? [String]
        appointment.plan          = info.objectForKey(a_plan)          as? [String]
        appointment.observation   = info.objectForKey(a_observation)   as? [String]
        appointment.assessment    = info.objectForKey(a_assessment)    as? [String]
        
        var date = self.localNotification.convertDateFromString(appointment.date) as NSDate!
        if self.localNotification.isDate(date, isEqualTo: NSDate()) == true || self.localNotification.isDate(NSDate(), isLesserThan: date){
            var name = "\(appointment.firstName) \(appointment.lastName)"
            self.localNotification.setLocalNotification(name, atTimeOf: appointment.date, id: appointment.entryId!)
        }
        return appointment
    }
    
    func convertAppointmentArray(result: [CatalyzeEntry]) -> [Appointment] {
        var appointments : Array = [Appointment]()
        self.localNotification.clearAll()
        for info in result {
            appointments.append(self.convertAppointments(info))
//            self.deleteAppointmentWithEntryId(info.entryId, inBlock: { (result: Bool?, error: AnyObject?) -> Void in })
        }
        return appointments
    }
    
    func convertAppointments(info: CatalyzeEntry) -> Appointment {
        var appointment: Appointment  = Appointment()
            appointment.entryId       = info.entryId as String
            appointment.date          = info.content.objectForKey(a_date)          as String
            appointment.location      = info.content.objectForKey(a_location)      as String
            appointment.patientId     = info.content.objectForKey(a_patientId)     as String
            appointment.firstName     = info.content.objectForKey(a_fname)         as String
            appointment.middleName    = info.content.objectForKey(a_mname)         as String
            appointment.lastName      = info.content.objectForKey(a_lname)         as String
            appointment.patientGender = info.content.objectForKey(a_patientGender) as String
            appointment.patientAge    = info.content.objectForKey(a_patientAge)    as String
            appointment.treatment     = info.content.objectForKey(a_treatment)     as String
            appointment.complaint     = info.content.objectForKey(a_complaint)     as String
            appointment.diagnosis     = info.content.objectForKey(a_diagnosis)     as String
            appointment.diagnostics   = info.content.objectForKey(a_diagnostics)   as String
            appointment.photo         = info.content.objectForKey(a_photo)         as NSMutableArray
            //appointment day code
            appointment.day           = info.content.objectForKey(a_day)           as String
            appointment.month         = info.content.objectForKey(a_month)         as String
            appointment.year          = info.content.objectForKey(a_year)          as String
            appointment.dateCode      = info.content.objectForKey(a_dateCode)?.integerValue
            //when appointment is done
            appointment.status        = info.content.objectForKey(a_status)        as? Bool
            appointment.vitals        = info.content.objectForKey(a_vitals)        as? String
            appointment.notes         = info.content.objectForKey(a_notes)         as? String
            appointment.medication    = info.content.objectForKey(a_medication)    as? String
            appointment.allergy       = info.content.objectForKey(a_allergy)       as? [String]
            appointment.plan          = info.content.objectForKey(a_plan)          as? [String]
            appointment.observation   = info.content.objectForKey(a_observation)   as? [String]
            appointment.assessment    = info.content.objectForKey(a_assessment)    as? [String]
        
            println("\(appointment.dateCode)")
            println("\(appointment.day)")
            println("\(appointment.month)")
            println("\(appointment.year)")
        
        var date = self.localNotification.convertDateFromString(appointment.date) as NSDate!
        if self.localNotification.isDate(date, isEqualTo: NSDate()) == true || self.localNotification.isDate(NSDate(), isLesserThan: date){
            var name = "\(appointment.firstName) \(appointment.lastName)"
            self.localNotification.setLocalNotification(name, atTimeOf: appointment.date, id: appointment.entryId!)
        }
        return appointment
    }
    
    //MARK: Save Appointment
    func saveAppointment(patientInfo : PatientInfo, date: String, location: String, treatment: String, complaint: String, diagnosis: String, diagnostics: String, fileId : String, entryID: String?, isFromEditForm: Bool, dict: NSDictionary, withBlock: SaveBlock) {
        
        self.networkActivity.show(true)
        var appointmentEntry : CatalyzeEntry = CatalyzeEntry(className: APPOINTMENT)
            appointmentEntry.content.setObject(date, forKey: a_date)
            appointmentEntry.content.setObject(location, forKey: a_location)
            appointmentEntry.content.setObject(treatment, forKey: a_treatment)
            appointmentEntry.content.setObject(complaint, forKey: a_complaint)
            appointmentEntry.content.setObject(diagnosis, forKey: a_diagnosis)
            appointmentEntry.content.setObject(diagnostics, forKey: a_diagnostics)
            appointmentEntry.content.setObject([fileId], forKey: a_photo)
            appointmentEntry.content.setObject(dict.objectForKey(a_day)!, forKey: a_day)
            appointmentEntry.content.setObject(dict.objectForKey(a_month)!, forKey: a_month)
            appointmentEntry.content.setObject(dict.objectForKey(a_year)!, forKey: a_year)
            appointmentEntry.content.setObject(dict.objectForKey(a_dateCode)!.integerValue, forKey: a_dateCode)
        
        if isFromEditForm != true {
            appointmentEntry.content.setObject(location, forKey: a_location)
            appointmentEntry.content.setObject(patientInfo.entryId, forKey: a_patientId)
            appointmentEntry.content.setObject(patientInfo.lastName, forKey: a_lname)
            appointmentEntry.content.setObject(patientInfo.firstName, forKey: a_fname)
            appointmentEntry.content.setObject(patientInfo.middleName, forKey: a_mname)
            appointmentEntry.content.setObject(String(patientInfo.age), forKey: a_patientAge)
            appointmentEntry.content.setObject(patientInfo.gender, forKey: a_patientGender)
            appointmentEntry.content.setObject(false, forKey: a_status)
            
            appointmentEntry.createInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
                self.networkActivity.show(false)
                withBlock(true, nil)
                }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                    self.networkActivity.show(false)
                    withBlock(false, result)
            })
        } else {
            appointmentEntry.entryId = entryID!
            appointmentEntry.saveInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
                self.networkActivity.show(false)
                withBlock(true, nil)
                }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                    self.networkActivity.show(false)
                    withBlock(false, result)
            })
        }
    }
    
    func saveStartedAppointmentDetails(entryID: String, vitals : String, notes: String, medication: String, allergy: [String], observation: [String], assessment: [String], plan: [String], isAppointmentDone: Bool, inBlock withBlock: SaveBlock) {
        self.networkActivity.show(true)
        var detailsEntry: CatalyzeEntry = CatalyzeEntry(className: APPOINTMENT)
            detailsEntry.content.setObject(vitals, forKey: a_vitals)
            detailsEntry.content.setObject(notes, forKey: a_notes)
            detailsEntry.content.setObject(medication, forKey: a_medication)
            detailsEntry.content.setObject(allergy, forKey: a_allergy)
            detailsEntry.content.setObject(observation, forKey: a_observation)
            detailsEntry.content.setObject(assessment, forKey: a_assessment)
            detailsEntry.content.setObject(plan, forKey: a_plan)
            detailsEntry.content.setObject(isAppointmentDone, forKey: a_status)
        
            detailsEntry.entryId = entryID
            detailsEntry.saveInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
                self.networkActivity.show(false)
                withBlock(true, nil)
            }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                self.networkActivity.show(false)
                withBlock(false, result)
        })
    }
    
    //MARK: PHOTO
    func savePhoto(data: NSData, inBlock block : SaveBlock) {
        self.networkActivity.show(true)
        CatalyzeFileManager.uploadFileToUser(data, phi: false, mimeType: "image/jpg", success: { (result:[NSObject : AnyObject]!) -> Void in
                println("success : \(result)")
                self.networkActivity.show(false)
                block(true, result)
            }) { (result:[NSObject : AnyObject]!, status :Int32, error:NSError!) -> Void in
                println("error : \(result)")
                self.networkActivity.show(false)
                block(false, error)
        }
    }
    
    func retrievePhoto(id:NSString, inBlock block : SaveBlock) {
        self.networkActivity.show(true)
        CatalyzeFileManager.retrieveFile(id, success: { (data:NSData!) -> Void in
                self.networkActivity.show(false)
                block(true, UIImage(data: data))
            }) { (result:[NSObject : AnyObject]!, status:Int32, error:NSError!) -> Void in
                println("error : \(result)")
                self.networkActivity.show(false)
                block(false, error)
        }
    }
    
    func deletePhoto(id:NSString, inBlock block : SaveBlock) {
        self.networkActivity.show(true)
        CatalyzeFileManager.deleteFile(id, success: { (result : AnyObject!) -> Void in
                EGOCache.globalCache().removeCacheForKey(id)
                sleep(1)
                self.networkActivity.show(false)
                block(true, result)
            }) { (object:[NSObject : AnyObject]!, result:Int32, error:NSError!) -> Void in
                println("error : \(result)")
                self.networkActivity.show(false)
                block(false, error)
        }
    }
    
    //MARK: Delete Entry 
    func deleteAppointmentWithEntryId(entryID : String, inBlock block : SaveBlock) {
        var appointmentEntry: CatalyzeEntry = CatalyzeEntry(className: APPOINTMENT)
        appointmentEntry.entryId = entryID
        
        appointmentEntry.deleteInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
            println("Appointment Deleted")
            self.localNotification.cancel(entryId: entryID)
            block(true, nil)
            }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                
            println(error)
            block(false, error)
        })
    }
    
}

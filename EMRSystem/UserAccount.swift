//
//  UserAccount.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 4/20/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class UserAccount: NSObject {
    
    let filesId       = "filesId"
    let userName      = "username"
    let emailAdd      = "email"
    let fullname      = "name"
    let birthdate     = "dob"
    let uAge          = "age"
    let ugender       = "gender"
    let maritalStatus = "maritalStatus"
    let addresses     = "addresses"
    let phoneNo       = "phoneNumber"
    let photo         = "profilePhoto"
    let department    = "department"
    let position      = "position"
    
    let cacheDay      = ((60 * 60 * 24) * 2)

    let networkActivity: NetworkActivityIndicator = NetworkActivityIndicator()
    typealias SaveBlock = (Bool?, AnyObject?) -> Void
    
    
    func editUserAccount(uname: String , email: Email, name: Name, dob: NSDate, age: NSNumber, gender: String, status: String, address: NSMutableArray, contactNo: PhoneNumber , dept: String, pos: String , profilePhoto: String, withBlock: SaveBlock) {
        
        CatalyzeUser.currentUser().setValue(uname, forKey: userName)
        CatalyzeUser.currentUser().setValue(email, forKey: emailAdd)
        CatalyzeUser.currentUser().setValue(name, forKey: fullname)
        CatalyzeUser.currentUser().setValue(dob, forKey: birthdate)
        CatalyzeUser.currentUser().setValue(age, forKey: uAge)
        CatalyzeUser.currentUser().setValue(gender, forKey: ugender)
        CatalyzeUser.currentUser().setValue(status, forKey: maritalStatus)
        CatalyzeUser.currentUser().setValue(address, forKey: addresses)
        CatalyzeUser.currentUser().setValue(contactNo, forKey: phoneNo)
        CatalyzeUser.currentUser().setValue(profilePhoto, forKey: photo)
        
        CatalyzeUser.currentUser().setExtra(dept, forKey: department)
        CatalyzeUser.currentUser().setExtra(pos, forKey: position)
        
        CatalyzeUser.currentUser().saveInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
            withBlock(true, nil)
            }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
                withBlock(false, result)
        })
    }
    
    func editUserAccountPhotoOnly(fileId: NSString, withBlock: SaveBlock) {
        CatalyzeUser.currentUser().setValue(fileId, forKey: photo)
        
        CatalyzeUser.currentUser().saveInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
            withBlock(true, nil)
            }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
            withBlock(false, result)
        })
    }
    
    //Photo
    func saveProfilePhoto(data: NSData, withBlock: SaveBlock) {
        self.networkActivity.show(true)
        CatalyzeFileManager.uploadFileToUser(data, phi: false, mimeType: "image/jpg", success: { (result: [NSObject : AnyObject]!) -> Void in
            let fileDictionary = result as NSDictionary
            let fileId = fileDictionary.objectForKey("filesId") as NSString
            let image = UIImage(data: data)
            self.setCacheImageFor(fileId, image: image!)
            sleep(1)
            self.networkActivity.show(false)
            withBlock(true , result)
            }) { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
            self.networkActivity.show(false)
            withBlock(false , result)
        }
    }
    
    func retrieveProfilePhoto(fileId : NSString, withBlock: SaveBlock) {
        self.networkActivity.show(true)
        CatalyzeFileManager.retrieveFile(fileId, success: { (data: NSData!) -> Void in
            self.networkActivity.show(false)
            var image = UIImage(data: data)
            self.setCacheImageFor(fileId, image: image!)
            sleep(1)
            withBlock(true, image)
            }) { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
            withBlock(false, result)
            self.networkActivity.show(false)
        }
    }
    
    func hasCacheImageFor(fileId: NSString) -> Bool {
        if EGOCache.globalCache().hasCacheForKey(fileId) {
            return true
        } else {
            return false
        }
    }
    
    func getCacheImageFor(fileId: NSString) -> UIImage {
        var image = EGOCache.globalCache().imageForKey(fileId)
        return image
    }
    
    func setCacheImageFor(fileId:NSString, image:UIImage) {
        println("setting image file id for : \(fileId)")
        EGOCache.globalCache().setImage(image, forKey: fileId, withTimeoutInterval: NSTimeInterval( self.cacheDay))
    }
    
    func deleteCacheImageFor(fileId:NSString) {
        EGOCache.globalCache().removeCacheForKey(fileId)
    }
    
    func getImageWithBlock(fileId: NSString, withBlock block: SaveBlock) {
        if self.hasCacheImageFor(fileId) {
            block(true, self.getCacheImageFor(fileId))
        } else {
            self.retrieveProfilePhoto(fileId, withBlock: { (succeeded:Bool?, result:AnyObject?) -> Void in
                if succeeded == true {
                    block(true, result)
                } else {
                    block(false, result)
                }
            })
        }
    }
    
}


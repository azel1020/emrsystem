//
//  PatientNotesTableView.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 5/11/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class PatientNotesTableView: UITableViewController, UIAlertViewDelegate {

    var patientNotes : [PatientsNote] = []
    var base: BaseViewController = BaseViewController()
    
    var patientDetails: PatientInfo = PatientInfo()
    var pNotes : PatientsNote = PatientsNote()
    var selectedIndex: NSIndexPath!
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.patientNotes.count == 0 {
            return 1
        }
        return self.patientNotes.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell!
    
        if self.patientNotes.count == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("EmptyStateCell", forIndexPath: indexPath) as UITableViewCell
        } else {
            let patientNote = self.patientNotes[indexPath.row] as PatientsNote
            
            cell = tableView.dequeueReusableCellWithIdentifier("NoteCell", forIndexPath: indexPath) as UITableViewCell
            
            var imageView = cell.viewWithTag(1) as UIImageView
            
            imageView.setImageWithString("\(self.base.getInitialLetter(patientNote.firstName)) \(self.base.getInitialLetter(patientNote.lastName))", color: self.getBackgroundColor(indexPath.row), circular: true)
            
            var nameLabel = cell.viewWithTag(2) as UILabel
            nameLabel.text = "\(patientNote.firstName) \(patientNote.lastName)"
            
            var notes = cell.viewWithTag(3) as SummaryTextView
            notes.text = patientNote.note
            
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        NSNotificationCenter.defaultCenter().postNotificationName(self.base.viewPatientNoteListener, object: indexPath)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if self.patientNotes.count == 0 {
            return 115
        }
        return 81
    }
    
    
    func getBackgroundColor(row:Int) -> UIColor {
        if row%2==0 {
            return self.base.eRedColor()
        } else if row%3==0 {
            return self.base.eLightRedColor()
        } else {
            return self.base.eDarkRedColor()
        }
    }
    
      
}

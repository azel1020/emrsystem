//
//  SummaryTextView.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 4/9/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class SummaryTextView: UITextView, UITextViewDelegate {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        self.delegate = self
        self.setElipsis()
    }
    
    func setElipsis() {
        self.textContainer.maximumNumberOfLines = 0
        self.textContainer.lineBreakMode = NSLineBreakMode.ByTruncatingTail
    }
    
    func textViewDidChange(textView: UITextView) {
        self.setElipsis()
    }
    
}

//
//  PatientProfileViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 4/28/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class PatientProfileViewController: UITableViewController {
    var patientDetails : PatientInfo!
    var base : BaseViewController = BaseViewController()
    var patientAppointments : [Appointment] = []
    var patientNotesTableView: PatientNotesTableView = PatientNotesTableView()
    var patientNotes : PatientsNote = PatientsNote()
    var pNote : [PatientsNote] = []
    let heightOfEmptyNoteTable = 211 as CGFloat//163.0 as CGFloat
    var heightOfDoctorsNoteTableView = 211 as CGFloat//163.0 as CGFloat
    
    @IBOutlet weak var doctorsNoteTableView: UITableView!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var complaintTextView: SummaryTextView!
    @IBOutlet weak var firstDateLabel: UILabel!
    @IBOutlet weak var recentAppointmentLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var contactNoLabel: UILabel!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var genderAgeLabel: UILabel!
    @IBOutlet weak var allergiesLabel: UILabel!
    @IBOutlet weak var famHistoryLabel: UILabel!
    @IBOutlet weak var othersLabel: UILabel!
    @IBOutlet weak var addNotesButton: UIButton!
    
    //MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar(patientDetails.firstName)
        self.setPatientDetails(patientDetails)
        self.setDoctorsNote()
        self.getRecentAppoitmentDate()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePatientProfile:", name: base.updatePatientProfile, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getPatientNotes", name: base.patientNoteListener, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "showNote:", name: base.viewPatientNoteListener, object: nil)
    }
    
    func showNote(noti: NSNotification) {
        var indexPath = noti.object as NSIndexPath
        let patientNote = self.pNote[indexPath.row] as PatientsNote
        var nvc = self.storyboard?.instantiateViewControllerWithIdentifier("NotesViewController") as NotesViewController
        nvc.patientInfo = patientDetails
        nvc.patientNote = patientNote
        nvc.isAdd = false
        self.navigationController?.pushViewController(nvc, animated: true)
    }
    
    func setPatientDetails(info: PatientInfo) {
        fullNameLabel.text     = "\(info.lastName), \(info.firstName) \(info.middleName)"
        complaintTextView.text = "Complaint Here"
        firstDateLabel.text    = info.dateofFirstVisit
        addressLabel.text      = isNotAvailable(info.address)
        cityLabel.text         = isNotAvailable(info.city)
        stateLabel.text        = isNotAvailable(info.state)
        zipLabel.text          = isNotAvailable(info.postalCode)
        contactNoLabel.text    = isNotAvailable(info.contactNo)
        emailAddressLabel.text = isNotAvailable(info.email)
        allergiesLabel.text    = base.convertToStringSet(info.allergies)
        famHistoryLabel.text   = base.convertToStringSet(info.familyHistory)
        othersLabel.text       = info.others
        genderAgeLabel.text    = "\(base.getAge(base.convertDateFromString(info.dateOfBirth, withTime: false)!)) \(info.gender)"
        
        self.profilePhoto.layer.borderWidth   = 3.0
        self.profilePhoto.layer.borderColor   = self.base.eRedColor().CGColor
        self.profilePhoto.layer.masksToBounds = true
        self.profilePhoto.layer.cornerRadius  = profilePhoto.frame.size.height/2
        self.profilePhoto.layer.masksToBounds = true

        if let photoFileId = info.photo {
            if photoFileId != "" {
                self.base.userAccount.getImageWithBlock(photoFileId, withBlock: { (succeeded:Bool?, object:AnyObject?) -> Void in
                    if succeeded == true {
                        let image = object as UIImage
                        self.profilePhoto.image = image
                    } else {
                        self.profilePhoto.setImageWithString("\(info.firstName) \(self.base.getInitialLetter(info.lastName))", color: self.base.eRedColor(), circular: true)
                    }
                })
            } else {
                self.profilePhoto.setImageWithString("\(info.firstName) \(self.base.getInitialLetter(info.lastName))", color: self.base.eRedColor(), circular: true)
            }
        } else {
            self.profilePhoto.setImageWithString("\(info.firstName) \(self.base.getInitialLetter(info.lastName))", color: self.base.eRedColor(), circular: true)
        }
    }
    
    func setDoctorsNote() {
        self.addNotesButton.addTarget(self, action: "addNewNote", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.base.HUD = MBProgressHUD.showHUDAddedTo(self.doctorsNoteTableView, animated: true)
        patientNotesTableView.patientDetails = self.patientDetails
        self.doctorsNoteTableView.delegate = patientNotesTableView
        self.doctorsNoteTableView.dataSource = patientNotesTableView
        
        self.getPatientNotes()
    }
    
    func addNewNote() {
        var nvc = self.storyboard?.instantiateViewControllerWithIdentifier("NotesViewController") as NotesViewController
        nvc.patientInfo = patientDetails
        self.navigationController?.pushViewController(nvc, animated: true)
    }
    
    func getPatientNotes() {
        self.base.HUD.labelText = "Loading patient notes..."
        self.base.HUD.show(true)
        self.patientNotes.getNotesOf(id: self.patientDetails.entryId) { (object:AnyObject?, error:AnyObject?) -> Void in
            if error == nil {
                self.base.HUD.hide(false)
                
                var note : [PatientsNote] = []
                note = object as [PatientsNote]
                self.pNote = note
                self.patientNotesTableView.patientNotes = note
                if note.count == 0 {
                    self.heightOfDoctorsNoteTableView = self.heightOfEmptyNoteTable as CGFloat
                } else {
                    self.heightOfDoctorsNoteTableView = 48.0+48.0+(81.0*CGFloat(note.count))
                }
                self.doctorsNoteTableView.reloadData()
                self.tableView.reloadData()
            } else {
                self.base.HUD.hide(false)
                self.base.showAlert("Oops! Something went wrong. Please try again.")
            }
        }
    }
    
    func setUpNavigationBar(firstName: String) {
        self.navigationItem.title = "\(firstName)'s Profile"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Edit,
                                                                              target: self,
                                                                              action: "editPatientProfile")
    }
    
    func getRecentAppoitmentDate() {
        patientAppointments = []
        var allAppointments = AuthService.sharedInstance.allAppointmentArray
        for appointment in allAppointments {
            if patientDetails.entryId == appointment.patientId {
                if appointment.status == true { //check if appointment has been done
                    patientAppointments.append(appointment)
                }
            }
        }
        
        if patientAppointments.count != 0 {
            var sortedList = base.sortAppointmenByDate(patientAppointments, isAscending: true)
            self.recentAppointmentLabel.text = sortedList[0].date
        } else {
            self.recentAppointmentLabel.text = "NONE"
        }
    }
    
    //MARK: UITableView Delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath == NSIndexPath(forRow: 1, inSection: 0) {
            var pavc = self.storyboard?.instantiateViewControllerWithIdentifier("PatientAppointmentViewController") as PatientAppointmentViewController
                pavc.info = patientDetails
            self.navigationController?.pushViewController(pavc, animated: true)
        }
    }
    
    //MARK: Edit Patient
    func editPatientProfile() {
        var apvc = self.storyboard?.instantiateViewControllerWithIdentifier("AddPatientViewController") as AddPatientViewController
            apvc.info = patientDetails
            apvc.isViewPatientInfo = true
        self.navigationController?.pushViewController(apvc, animated: true)
    }
    
    func updatePatientProfile(result: NSNotification) {
        var info = result.object! as PatientInfo
        self.patientDetails = info
        self.setPatientDetails(info)
        self.setUpNavigationBar(info.firstName)
    }
    
    //MARK: Helper
    func isNotAvailable(string: String) -> String {
        if string == "" {
            return "Not Available"
        }
        return string
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Tableview
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath == NSIndexPath(forRow: 0, inSection: 0) {
            return 130
        } else if indexPath == NSIndexPath(forRow: 1, inSection: 0) {
            return 40
        } else if indexPath == NSIndexPath(forRow: 0, inSection: 4) {
            return self.heightOfDoctorsNoteTableView
        } else {
            return 44
        }
    }
}

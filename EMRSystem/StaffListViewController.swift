//
//  StaffListViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 5/20/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class StaffListViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
    }

    func setUpNavigationBar() {
        self.navigationItem.title = "Staff"
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add,
                                                                              target: self,
                                                                              action: "gotoAddStaff")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gotoAddStaff() {
        var asvc = self.storyboard?.instantiateViewControllerWithIdentifier("AddStaffViewController") as AddStaffViewController
        self.navigationController?.pushViewController(asvc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

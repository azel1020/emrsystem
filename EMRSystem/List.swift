//
//  List.swift
//  EMRSystem
//
//  Created by Allan Gonzales on 5/20/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class List: NSObject {
    let fileName        = "List"
    let fileExtension   = "plist"
    let fileManager     = NSFileManager.defaultManager()
    
    let keyMedical      = "Medical"
    let keyLocation     = "Location"
    
    var medicalList : [String] = []
    var locationList: [String] = []
    
    class var sharedInstance : List {
        struct Singleton {
            static let instance = List()
        }
        return Singleton.instance
    }
    
    func awakeTypeAhead() {
        var fileList: NSDictionary = NSDictionary(contentsOfFile: self.pathFile())!
        var medicalList = fileList.objectForKey(self.keyMedical) as [String]
        List.sharedInstance.medicalList = medicalList
        var locationList = fileList.objectForKey(self.keyLocation) as [String]
        List.sharedInstance.locationList = locationList
    }
    
    func pathFile() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        let documentsDirectory = paths.objectAtIndex(0)as NSString
        let path = documentsDirectory.stringByAppendingPathComponent("\(fileName).\(fileExtension)")
        println(path)
        // Check if file exists
        if(!fileManager.fileExistsAtPath(path)) {
            // If it doesn't, copy it from the default file in the Resources folder
            let bundle = NSBundle.mainBundle().pathForResource(fileName, ofType: fileExtension)
            fileManager.copyItemAtPath(bundle!, toPath: path, error:nil)
        }
        return path
    }
    
    //MARK: Medical
    func addToObjectList(title:String, type: ListType) {
        var list    = type == ListType.Medical ? List.sharedInstance.medicalList : List.sharedInstance.locationList
        var keyList = type == ListType.Medical ? self.keyMedical : self.keyLocation
        
        if !contains(list, title) {
            println("Doesn't Contains.")
            var fileList: NSMutableDictionary = NSMutableDictionary(contentsOfFile: self.pathFile())!
            var objectList = fileList.objectForKey(keyList) as NSMutableArray
                objectList.addObject(title)
                fileList.writeToFile(self.pathFile(), atomically: true)
            self.awakeTypeAhead()
        } else {
            println("Contains.")
        }
    }
    
    func deleteToObjectList(title:String,  type: ListType) {
        var list    = type == ListType.Medical ? List.sharedInstance.medicalList : List.sharedInstance.locationList
        var keyList = type == ListType.Medical ? self.keyMedical : self.keyLocation
        
        if contains(list, title) {
            println("Contains.")
            var fileList: NSMutableDictionary = NSMutableDictionary(contentsOfFile: self.pathFile())!
            var objectList = fileList.objectForKey(keyList) as NSMutableArray
                objectList.removeObject(title)
                fileList.writeToFile(self.pathFile(), atomically: true)
            self.awakeTypeAhead()
        } else {
            println("Doesn't Contains.")
        }
    }
}

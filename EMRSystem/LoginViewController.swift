//
//  LoginViewController.swift
//  EMRSystem
//
//  Created by Glazel Magpayo on 3/25/15.
//  Copyright (c) 2015 glazel.co. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var grantAndAccess = GrantAndRevokeAccessService()

    let list : List = List()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.usernameTextField.text = "teamdyno"
        self.passwordTextField.text = "dyno"
        
        self.usernameTextField.layer.borderColor       = UIColor.whiteColor().CGColor
        self.passwordTextField.layer.borderColor       = UIColor.whiteColor().CGColor
        self.usernameTextField.layer.sublayerTransform = CATransform3DMakeTranslation(8, 0, 0)
        self.passwordTextField.layer.sublayerTransform = CATransform3DMakeTranslation(8, 0, 0)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    @IBAction func loginUser(sender: AnyObject) {
        
        if isConnectedToNetwork() {
            self.HUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            authService.loginuser(self.usernameTextField.text, password: self.passwordTextField.text) { (result: Bool?, error: AnyObject?) -> Void in 
                self.HUD.hide(true)
                self.view.endEditing(true)
                if result != false {
//                    self.grantAndAccess.postGrantUser(userId: "a242d5a1-f8c5-4ec0-b59a-5498d9c4d918", customClass: "AssistantInfo", withBlock: { (isSuccess: Bool?, error: AnyObject?) -> Void in
//                        if isSuccess == true {
//                            println("YEHEY !")
//                        } else {
//                            println("ERROR :( WHATTHE")
//                        }
//                    })
                    

                
//                    var user = CatalyzeUser()
//                        user.usersId = "a242d5a1-f8c5-4ec0-b59a-5498d9c4d918"
//                    
//                    user.deleteInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
//                        println("Suckcess :P")
//                        }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
//                        println("Error")
//                    })
                    
//                        user.retrieveInBackgroundWithSuccess({ (result: AnyObject!) -> Void in
//                            println("Success Result: \(result)")
//                        
//                        var assistant = result as CatalyzeUser
//                        
//                        println(assistant.username)
//                        println(assistant.name.firstName)
//                        println(assistant.name.lastName)
//                    
//                        }, failure: { (result: [NSObject : AnyObject]!, status: Int32, error: NSError!) -> Void in
//                            println("Failed Rsutl: \(result)")
//                    })

                    NSNotificationCenter.defaultCenter().postNotificationName(self.splitviewListener, object:true)
                } else {
                    println(error!)
                    self.showAlertError(error!)
                }
            }
        } else {
            self.showAlert(noNetworkConnectionMessage)
        }
    }
}
